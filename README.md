# Graph Algorithm Visualiser
The Algorithm Visualizer is a web-based tool developed by Luke Roberts and Sam Wilson for the University of Leeds, and extended to fully support graph algorithm by George Wright. This project will aim to make a tool to easily create graphs in a web browser then implement algorithms to manipulate them. This will have the aim of making graph theory and graphical algorithms more accessible to those without a programming background. All code is free and open-source.

# Building and Running
The program is [live](https://wrightg42.gitlab.io/graph-visualiser/) using GitLab Pages.
To download and run locally the code and linked submodules need to be downloaded and can be done by cloning the repo with submodules:
```
git clone --recursive -j8 git@gitlab.com:wrightg42/graph-visualiser.git
```
Or by loading the submodules after cloning separately:
```
git clone git@gitlab.com:wrightg42/graph-visualiser.git
git submodule update --init --recursive -j8
```
From here the program can be ran locally by loading `index.html`. This can be ran in uncompressed mode or compressed mode using a URL query string parameter `compressed=`. This will default to true if unprovided or erroneous. So to load compressed and uncompressed versions, respectively, use the following URL's.
```
C:/path/to/repo/index.html
C:/path/to/repo/index.html?compressed=false
```
If you are running locally using a chromium based browser there is an issue which will block the request to load the toolbox and start-blocks xml files due to CORS policies (Issues [41024](https://bugs.chromium.org/p/chromium/issues/detail?id=41024) and [47416](https://bugs.chromium.org/p/chromium/issues/detail?id=47416)). If this is the case please either use a less strict browser such as Safari and Firefox, or copy the contents of these files into the correct `div` elements on the page on the page. If you wish to compress the code run `npm run build` to compress all files into the compressed folder for code. If modules need installing run:
```
npm install
npm --prefix ./libraries/google/blockly install
npm run build
```

## Testing
Blockly has [testing](https://developers.google.com/blockly/guides/modify/web/unit-testing) built in to allow developers to easily test their code. JS tests and Generator tests have been implemented in order to verify the Extensions/Mutators created, and the outputted code respectively. Building the project will copy over testing scripts to test code created in this repository. Tests for this project require additional files to be loaded so can be accessed in the `graph-index.html` variant of the testing pages, rather than the default `index.html` files in the testing folders.

The internal graph searching method has also been tested to be accurate and we can generate box plots of timings.

More details on both these testing, as well as user testing, can be found in the [Wiki](https://gitlab.com/wrightg42/graph-visualiser/-/wikis/Testing).

# Libraries
This tool is build with numerous libraries. These are detailed below:
- [Google Blockly] - Block based programming UI (linked in submodules)
- [JS-Interpreter] - Interpret and run compiled blockly code (linked in submodules)
- [vis.js] - Render and manage graph
- [jQWidgets] - 3-way splitter functionality (linked in submodules)
- [node.js] - Building and compressing of JS scripts and Blockly
- [Bootstrap] - Page styling and layout
- [jQuery] - Handle HTML page interactions
- [lodash] - Checks deep equality and other useful uncommon functions

# License
The use and modification of this tool is controlled by the terms of the Apache 2 licence http://www.apache.org/licenses/LICENSE-2.0

All enquires regarding the use of this tool beyond the boundaries of the Apache 2 licence should be directed to The University of Leeds https://www.leeds.ac.uk/

[Google Blockly]: <https://github.com/google/blockly/>
[JS-Interpreter]: <https://github.com/NeilFraser/JS-Interpreter>
[vis.js]: <https://github.com/visjs/vis-network>
[jQWidgets]: <https://github.com/jqwidgets/jQWidgets>
[node.js]: <http://nodejs.org>
[Bootstrap]: <https://github.com/twbs/bootstrap>
[jQuery]: <http://jquery.com>
[lodash]: <https://github.com/lodash/lodash>
