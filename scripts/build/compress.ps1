Set-PSDebug -Trace 1

# move to root of project
cd ../../

# make compressed build directory
New-Item -ItemType Directory -Force -Path ".\compressed\"

# compress javascript then css
npm run compress-js
npm run compress-css
