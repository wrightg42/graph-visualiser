#!/bin/bash
set -x

# move to root of project
cd ../../

# make compressed build directory
mkdir -p ./compressed/

# compress javascript then css
npm run compress-js
npm run compress-css
