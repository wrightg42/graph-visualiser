#!/bin/bash
set -x

# move to root of project
cd ../../

# make compressed build directory
mkdir -p ./compressed/blockly/msg/

# copy the generator tests index page add line for graph tests
cp ./libraries/google/blockly/tests/generators/index.html ./scripts/blockly/tests/generators/graph-index.html
sed -i 's|</body>|<script src="graph-tests.js"></script>\n</body>|g' ./scripts/blockly/tests/generators/graph-index.html

# copy the mocha tests index page add line for graph tests and import the blocks
cp ./libraries/google/blockly/tests/mocha/index.html ./scripts/blockly/tests/mocha/graph-index.html
sed -i 's|<div id="blocklyDiv">|<script src="graphs_test.js"></script>\n<div id="blocklyDiv">|g' ./scripts/blockly/tests/mocha/graph-index.html
sed -i 's|</head>|<script src="../../blocks/object.js"></script>\n<script src="../../blocks/z_graph.js"></script>\n</head>|g' ./scripts/blockly/tests/mocha/graph-index.html

# copy toolbox for testing
mkdir -p ./scripts/blockly/tests/generators/toolbox
cp ./xml/toolbox.xml ./scripts/blockly/tests/generators/toolbox

# rename graph files to make sure it is compiled last
mv ./scripts/blockly/blocks/graph.js ./scripts/blockly/blocks/z_graph.js
mv ./scripts/blockly/generators/javascript/graph.js ./scripts/blockly/generators/javascript/z_graph.js

# copy blockly extensions to blockly source
cp -RT ./scripts/blockly/ ./libraries/google/blockly

# revert graph files names to normal name
mv ./scripts/blockly/blocks/z_graph.js ./scripts/blockly/blocks/graph.js
mv ./scripts/blockly/generators/javascript/z_graph.js ./scripts/blockly/generators/javascript/graph.js

# build blockly
cd ./libraries/google/blockly/
npm run build:compressed
npm run build:blocks
npm run build:generators
npm run build:langfiles

# copy build blockly to its correct directory
cp ./*_compressed.js ../../../compressed/blockly/
cp -RT ./msg/ ../../../compressed/blockly/msg

# cleanup testing folder
cd ../../../scripts/blockly/tests/
rm ./generators/graph-index.html
rm -r ./generators/toolbox
rm ./mocha/graph-index.html
