Set-PSDebug -Trace 1

# move to root of project
cd ../../

# make compressed build directory
New-Item -ItemType Directory -Force -Path ".\compressed\"
New-Item -ItemType Directory -Force -Path ".\compressed\blockly"
New-Item -ItemType Directory -Force -Path ".\compressed\blockly\msg"

# copy the generator tests index page add line for graph tests
Copy-Item ".\libraries\google\blockly\tests\generators\index.html" -Destination ".\scripts\blockly\tests\generators\graph-index.html"
(Get-Content .\scripts\blockly\tests\generators\graph-index.html) `
    -replace '</body>', '<script src="graph-tests.js"></script></body>' |
  Set-Content .\scripts\blockly\tests\generators\graph-index.html

# copy the mocha tests index page add line for graph tests and import the blocks
Copy-Item ".\libraries\google\blockly\tests\mocha\index.html" -Destination ".\scripts\blockly\tests\mocha\graph-index.html"
(Get-Content .\scripts\blockly\tests\mocha\graph-index.html) `
    -replace '<div id="blocklyDiv">', '<script src="graphs_test.js"></script><div id="blocklyDiv">' `
    -replace '</head>', '<script src="../../blocks/object.js"></script><script src="../../blocks/z_graph.js"></script></head>' |
  Set-Content .\scripts\blockly\tests\mocha\graph-index.html

# copy toolbox for testing
New-Item -ItemType Directory -Force -Path ".\scripts\blockly\tests\generators\toolbox"
Copy-Item ".\xml\toolbox.xml" -Destination ".\scripts\blockly\tests\generators\toolbox\"

# rename graph files to make sure it is compiled last
Rename-Item -Path ".\scripts\blockly\blocks\graph.js" -NewName "z_graph.js"
Rename-Item -Path ".\scripts\blockly\generators\javascript\graph.js" -NewName "z_graph.js"

# copy blockly extensions to blockly source
Copy-Item -Path ".\scripts\blockly\*" -Destination ".\libraries\google\blockly" -Recurse -force

# revert graph files names to normal name
Rename-Item -Path ".\scripts\blockly\blocks\z_graph.js" -NewName "graph.js"
Rename-Item -Path ".\scripts\blockly\generators\javascript\z_graph.js" -NewName "graph.js"

# build blockly
cd ./libraries/google/blockly/
npm run build:compressed
npm run build:blocks
npm run build:generators
npm run build:langfiles

# copy build blockly to its correct directory
Copy-Item ".\*_compressed.js" "..\..\..\compressed\blockly\" -force
Copy-Item ".\msg\*" -Destination "..\..\..\compressed\blockly\msg" -Recurse -force

# cleanup testing folder
cd ../../../scripts/blockly/tests
Remove-Item -Path ".\generators\graph-index.html"
Remove-Item -LiteralPath ".\generators\toolbox\" -Force -Recurse
Remove-Item -Path ".\mocha\graph-index.html"
