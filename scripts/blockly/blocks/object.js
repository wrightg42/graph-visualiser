/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Object blocks for Blockly.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

goog.provide("Blockly.Blocks.object"); // depricated
goog.provide("Blockly.Constants.Object");

goog.require("Blockly");
goog.require("Blockly.Blocks");
goog.require("Blockly.FieldTextInput");
goog.require("Blockly.FieldVariable");
goog.require("Blockly.Mutator");

// define the constants object for objects in blockly
Blockly.Constants.Object = {
  HUE: 50,
  OUTPUT: "Object",
  NEW: { Events: { } }, // helper namespace for common new object functions
};

Blockly.defineBlocksWithJsonArray([
  {
    "type": "object_new",
    "inputsInline": true,
    "output": Blockly.Constants.Object.OUTPUT,
    "style": "object_blocks",
    "tooltip": "Creates a new object with data.",
    "mutator": "object_new_mutator",
  },
  {
    "type": "object_new_container",
    "message0": "Object %2 %1",
    "args0": [
      {	"type": "input_statement", "name": "STACK",	},
      { "type": "input_dummy", "name": "NEWLINE", },
    ],
    "style": "object_blocks",
    "tooltip": "The object structure to create.",
    "contextMenu": false,
  },
  {
    "type": "object_copy_data",
    "message0": "copy %2 to %1",
    "args0": [
      { "type": "field_variable", "name": "VAR", },
      { "type": "input_value", "name": "OBJECT", "check": Blockly.Constants.Object.OUTPUT, },
    ],
    "inputsInline": true,
    "previousStatement": null,
    "nextStatement": null,
    "style": "object_blocks",
    "tooltip": "Access a value held in an object.",
  },
	{
		"type": "object_get",
		"message0": "%1 . %2",
		"args0": [
			{	"type": "input_value", "name": "VAR", "check": "Object",	},
			{	"type": "input_value", "name": "FIELD", "check": "String", },
		],
		"inputsInline": true,
    "output": null,
    "style": "object_blocks",
		"tooltip": "Access a value held in an object.",
	},
  {
    "type": "object_set",
    "message0": "%1 . %2 = %3",
    "args0": [
      {	"type": "input_value", "name": "VAR", "check": "Object",	},
			{ "type": "input_value", "name": "FIELD", "check": "String", },
      { "type": "input_value", "name": "VALUE", },
    ],
    "inputsInline": true,
    "previousStatement": null,
    "nextStatement": null,
    "style": "object_blocks",
    "tooltip": "Changes the values inside an object.",
  },
]);

/**
 * Generates the block structure of a object data property for a mutator UI.
 * NOTE: will also call functions to update the flyout. The block supplied in "block"
 * must have an "updateFlyoutBlockCreated" and "updateFlyoutBlockDisposed" function.
 * @param block the block to generate on. Needed since 'this' is changed by returning function.
 * @param property the property name of the new mutator item. If undefined will be editable by user.
 * @param style the style to set the mutator block to.
 * @return a function used to init the mutator block.
 **/
Blockly.Constants.Object.NEW.MUTATOR_GEN_INIT = function(block, property, style) {
  // add default value to colour
  if (style === undefined) {
    style = "object_blocks";
  }

  return function() {
    // generate the name of the property field
    if (property === undefined) {
      block.nameField_ = new Blockly.FieldTextInput("", block.validator_);
    } else {
      block.nameField_ = new Blockly.FieldLabelSerializable(property);
    }

    // generate this mutator block with property then name field
    block.appendDummyInput()
        .appendField("Property:")
        .appendField(block.nameField_, "NAME");
    block.setPreviousStatement(true);
    block.setNextStatement(true);
    block.setStyle(style);
    block.setTooltip(function() {
      return "Adds an additional property, " + block.nameField_.getValue() + ", to object definition.";
    });
    block.contextMenu = false;
  };
}

// Mutator blocks - new property field
Blockly.Blocks["object_new_prop"] = {
  init: function() {
    // generate mutator block structure
    Blockly.Constants.Object.NEW.MUTATOR_GEN_INIT(this)();

    // correctly label the field label
    this.nameField_.setValue(this.genNextLabel_());
  },

  /**
   * Generates and sets a valid non-conflicting label for a property name.
   * @return the next label used for a property name.
   **/
  genNextLabel_: function() {
    var defaultName = "property", i = 0;
    while (this.nameField_.validator_(defaultName + i) === null) {
      ++i;
    }
    return defaultName + i;
  },

  /**
   * Validates a property name choice.
   * @param propname the property name given to the field.
   * @return the property name if valid, otherwise null.
   **/
  validator_: function(propname) {
    // remove repeated whitespace, and leading and trailing whitespace from property name
    propname = propname.replace(/[\s\xa0]+/g, " ").replace(/^ | $/g, "");
    if (!propname) {
      return null;
    }

    // prevent duplicate property names
    var sourceBlock = this.getSourceBlock();
    var workspace = sourceBlock.workspace.targetWorkspace ||
                    sourceBlock.workspace;
    var caselessName = propname.toLowerCase();

    // iterate all blocks in mutator workspace
    var blocks = workspace.getAllBlocks(false);
    for (var i = 0; i < blocks.length; i++) {
      if (blocks[i].id == sourceBlock.id) {
        // skip container block
        continue;
      }

      // check for same value
      var otherVar = blocks[i].getFieldValue("NAME");
      if (otherVar && otherVar.toLowerCase() == caselessName) {
        return null;
      }
    }

    // valid, return value
    return propname;
  },
}

// based heavily on the procedure mutator code
Blockly.Constants.Object.NEW.MUTATOR_MIXIN = {
  // data to define in this mutator
  data_: {
    properties: [],  // array of property names
  },

  text_: {
    PREFIX: "{",
    POSTFIX: "}",
  },

  /**
   * Create XML to represent the properties stored in the new object.
   * @return {Element} XML storage element.
   * @this {Blockly.Block}
   */
  mutationToDom: function() {
    var container = Blockly.utils.xml.createElement("mutation");
    container.setAttribute("data", JSON.stringify(this.data_).replaceAll("\"", "\\'"));
    return container;
  },

  /**
   * Parse XML to restore properties for new object.
   * @param {!Element} xmlElement XML storage element.
   * @this {Blockly.Block}
   */
  domToMutation: function(xmlElement) {
    this.data_ = JSON.parse(xmlElement.getAttribute("data").replaceAll("\\'", "\""));
    if (!this.data_.properties) {
      this.data_.properties = [];
    }
    this.data_.properties = this.processProperties(this.data_.properties);
    this.updateShape_();
  },

  /**
   * Populate the mutator's dialog with this block's components.
   * @param {!Blockly.Workspace} workspace Mutator's workspace.
   * @return {!Blockly.Block} Root block in mutator.
   * @this {Blockly.Block}
   */
  decompose: function(workspace) {
    var containerBlock = this.decomposeContainer(workspace);
    var initSvg = true;
    try {
      containerBlock.initSvg();
    } catch (e) {
      initSvg = false;
      containerBlock.init();
    }

    // add a property for each defined property
    var connection = containerBlock.getInput("STACK").connection;
    for (var i in this.data_.properties) {
      var itemBlock = this.decomposeBlock(workspace, this.data_.properties[i]);
      initSvg ? itemBlock.initSvg() : itemBlock.init();
      connection.connect(itemBlock.previousConnection);
      connection = itemBlock.nextConnection;
    }

    return containerBlock;
  },

  /**
   * Reconfigure this block based on the mutator dialog's components.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this {Blockly.Block}
   */
  compose: function(containerBlock) {
    var itemBlock = containerBlock.getInputTargetBlock("STACK");

    // count properties supplied
    var properties = [];
    var connections = [];
    while (itemBlock && !itemBlock.isInsertionMarker()) {
      var property = itemBlock.nameField_.getValue();
      properties.push(property);
      connections.push(containerBlock.valueConnections_[property]);
      itemBlock = itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
    }

    // run additional processing on properties
    properties = this.processProperties(properties, containerBlock);

    // remove all removed item
    var diff = this.data_.properties.filter(function(x) { return !properties.includes(x) });
    for (var i in diff) {
      // removed, disconnect
      try {
        var connection = this.getInput("K" + diff[i]).connection.targetConnection;
        if (connection && connections.indexOf(connection) == -1) {
          connection.disconnect();
        }
      } catch (error) { }
    }
    this.data_.properties = properties;
    this.updateShape_();

    // reconnect any child blocks
    for (var i in this.data_.properties) {
      Blockly.Mutator.reconnect(connections[i], this, "K" + this.data_.properties[i]);
    }
  },

  /**
   * Store pointers to any connected child blocks.
   * @param {!Blockly.Block} containerBlock Root block in mutator.
   * @this {Blockly.Block}
   */
  saveConnections: function(containerBlock) {
    if (containerBlock.valueConnections_ === undefined) {
      containerBlock.valueConnections_ = { };
      var itemBlock = containerBlock.getInputTargetBlock("STACK");
      for (var i in this.data_.properties) {
        if (!itemBlock) {
          break;
        }
        var input = this.getInput("K" + this.data_.properties[i]);
        containerBlock.valueConnections_[this.data_.properties[i]] = input && input.connection && input.connection.targetConnection;
        itemBlock = itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
      }
    }
  },

  /**
   * Modify this block to have the correct number of inputs.
   * @this {Blockly.Block}
   * @private
   */
  updateShape_: function() {
    // add open brace
    var addedOpen = false;
    if (!this.getInput("PREFIX")) {
      addedOpen = true;
      this.appendDummyInput("PREFIX")
          .appendField(this.text_.PREFIX, "PREFIX-TEXT");
      this.setInputsInline(true);
    } else {
      this.setFieldValue(this.text_.PREFIX, "PREFIX-TEXT");
      this.removeInput("POSTFIX");
    }

    // remove deleted inputs
    var rmList = [];
    for (var i in this.inputList) {
      if (this.inputList[i].name[0] === "K" && !this.data_.properties.includes(this.inputList[i].name.substring(1))) {
        rmList.push(this.inputList[i].name);
      }
    }
    rmList.forEach(function(item) {
      this.removeInput(item);
    }.bind(this));

    // add new inputs
    for (var i in this.data_.properties) {
      var property = this.data_.properties[i];
      var inp = this.getInput("K" + property);
      if (!inp) {
        this.generateInput(property, i == 0 && this.inputList.length == 1);
      } else {
        // remove the leading comma if this is the new first input
        if (this.inputList[1].name === inp.name && inp.fieldRow[0].value_ === ", ") {
          inp.removeField("K" + property + "COMMA");
        }
      }
    }

    // add closing brace text
    this.appendDummyInput("POSTFIX")
        .appendField(this.text_.POSTFIX, "POSTFIX-TEXT");

    // run cleanup function
    this.cleanup();
  },

  /**
   * Generates the initial container block for the mutation UI.
   * @param workspace the mutator workspace.
   * @return the mutator container block.
   **/
  decomposeContainer: function(workspace) {
    return workspace.newBlock("object_new_container");
  },

  /**
   * Generates a mutator block to place in the container during decomposition.
   * @param workspace the mutator workspace.
   * @param property the property to generate a mutator block for.
   * @return the mutator block for this property.
   **/
  decomposeBlock: function(workspace, property) {
    var itemBlock = workspace.newBlock("object_new_prop");
    itemBlock.nameField_.setValue(property);
    return itemBlock;
  },

  /**
   * Process the given properties from the compose function.
   * @param containerBlock the container block storing the mutator data.
   * @param properties the properties to process and update.
   * @return the filtered and modified properties.
   **/
  processProperties: function(properties, containerBlock) {
    return properties; // do nothing
  },

  /**
   * Generates an input and label for main block using a property definition. Called by updateShape_.
   * @param property the property to generate an input for.
   * @param first if this is the first input to be generated. Defaults to false.
   **/
  generateInput: function(property, first) {
    if (first === undefined) {
      first = false;
    }

    // generate uneditable property name
    var propname = new Blockly.FieldLabelSerializable(property);

    // create input for property
    var input = this.appendValueInput("K" + property);
    if (!first) {
      input.appendField(", ", "K" + property + "COMMA");
    }
    input.appendField(propname)
         .appendField("=")
         .setAlign(Blockly.ALIGN_RIGHT);
  },

  /**
   * A general cleanup function ran after updateShape_.
   **/
  cleanup: function() { }
};

// register mutator
Blockly.Extensions.registerMutator("object_new_mutator",
  Blockly.Constants.Object.NEW.MUTATOR_MIXIN, null,
  ["object_new_prop"]
);

/**
 * Listens for change in a graph data mutator.
 * @param e the event triggering the listener.
 **/
Blockly.Constants.Object.NEW.Events.mutatorChangeListener_ = function(e) {
  if ([Blockly.Events.BLOCK_CREATE, Blockly.Events.BLOCK_DELETE, Blockly.Events.BLOCK_CHANGE].includes(e.type)) {
    Blockly.Constants.Object.NEW.Events.updateMutatorFlyout_(Blockly.Workspace.getById(e.workspaceId));
  }
}

/**
 * Generates a function to handle events in a object mutator and keep it up to date.
 * @param mutatorWS the workspace of the mutator.
 **/
Blockly.Constants.Object.NEW.Events.updateMutatorFlyout_ = function(mutatorWS) {
  // get the mutator flyout block
  var flyoutBlock = mutatorWS.flyout_.workspace_.getAllBlocks()[0];

  // get blocks in mutator and which block to generate a label with
  var mutatorBlocks = mutatorWS.getBlocksByType("object_new_prop");

  // generate a new label and apply
  var defaultName = "property", i = 0;
  while (mutatorBlocks.length && (mutatorBlocks[0].nameField_.getValue() === defaultName + i ||
          mutatorBlocks[0].nameField_.validator_(defaultName + i) === null)) {
    ++i;
  }
  flyoutBlock.nameField_.setValue(defaultName + i);
}
