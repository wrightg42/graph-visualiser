/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Helper blocks that are not included, such as comments.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

goog.provide("Blockly.Blocks.helpers"); // depricated

goog.require("Blockly");
goog.require("Blockly.Blocks");
goog.require("Blockly.FieldTextInput");
goog.require("Blockly.FieldVariable");

Blockly.defineBlocksWithJsonArray([
  {
    "type": "helpers_main",
    "message0": "Start by,",
    "style": "loop_blocks",
    "nextStatement": null,
    "tooltip": "Program starts here."
  },
	{
		"type": "helpers_comment",
		"message0": "// %1",
		"args0": [
      { "type": "field_input", "name": "COMMENT",	"text": "", },
    ],
		"style": "text_blocks",
		"previousStatement": null,
		"nextStatement": null,
		"tooltip": "A comment to explain code or blocks.",
	},
	{
		"type": "helpers_variable_swap",
		"message0": "swap %1 and %2",
		"args0": [
      { "type": "field_variable", "name": "VAR1", },
      { "type": "field_variable", "name": "VAR2", },
		],
		"inputsInline": true,
    "previousStatement": null,
    "nextStatement": null,
		"style": "variable_blocks",
		"tooltip": "Swaps the values of two variables.",
	},
  {
    "type": "helpers_typeof",
    "message0": "type of %1",
    "args0": [
      { "type": "field_variable", "name": "VAR", },
    ],
    "output": "String",
    "style": "variable_blocks",
    "tooltip": "Gets the type of the variable.",
  },
  {
    "type": "helpers_lists_is_in",
    "message0": "is %1 in %2",
    "args0": [
      { "type": "input_value", "name": "ITEM", },
      { "type": "input_value", "name": "LIST", "check": "Array", },
    ],
    "inputsInline": true,
    "output": "Boolean",
    "style": "list_blocks",
    "tooltip": "Checks if a value is in a list.",
  },
  {
    "type": "helpers_lists_set_funcs",
    "message0": "%1 %3 %2",
    "args0": [
      { "type": "input_value", "name": "LIST1", "check": "Array", },
      { "type": "input_value", "name": "LIST2", "check": "Array", },
      { "type": "field_dropdown", "name": "OPERATION", "options": [
        [ "∪", "UNION" ],
        [ "∩", "INTERSECTION"],
        [ "-", "DIFFERENCE"],
        [ "△", "SYMETRIC_DIFFERENCE"],
      ],},
    ],
    "inputsInline": true,
    "output": "Array",
    "style": "list_blocks",
    "tooltip": function() {
      return "Applys the set %1 between two input lists. Using arrays so duplicates will still occure!"
          .replace("%1", thisBlock.getFieldValue("OPERATION").toLowerCase().replace("_", " "));
    },
    "extensions": ["function_tooltip"],
  },
]);
