/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Blocks that manipulate the graph.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

goog.provide("Blockly.Blocks.graph"); // depricated
goog.provide("Blockly.Constants.Graph");

goog.require("Blockly");
goog.require("Blockly.Blocks");
goog.require("Blockly.FieldCheckbox");
goog.require("Blockly.FieldDropdown");
goog.require("Blockly.FieldLabel");
//goog.require("Blockly.FieldLabelSerializable"); // causes errors? not sure why, exists in blockly
goog.require("Blockly.FieldTextInput");
goog.require("Blockly.FieldVariable");
goog.require("Blockly.Mutator");

goog.require("Blockly.Constants.Object");

// define the constants object for graphs in blockly
Blockly.Constants.Graph = {
  HUE: 180,
  DATA: { Events: { } },
};

// general graph blocks
Blockly.defineBlocksWithJsonArray([
  //////////////////////
  // Graph Data       //
  //////////////////////
  {
    "type": "graph_select",
    "message0": "Graph: %1",
    "args0": [
      { "type": "input_dummy", "name": "DUMMY" },
    ],
    "output": "String",
    "style": "graph_blocks",
    "tooltip": "Generates a string corrosponding to a specified graph. Used to get data from the correct graph.",
    "extensions": ["graph_select_dropdown_extension"],
  },
  {
    "type": "graph_data",
    "output": Blockly.Constants.Object.OUTPUT,
    "style": "graph_blocks",
    "tooltip": "A dictionary of graph data.",
    "mutator": "graph_data_mutator",
  },
  {
    "type": "graph_data_container",
    "message0": "%1 data%3 %2",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["Node", "NODE"], ["Edge", "EDGE"]], },
      {	"type": "input_statement", "name": "STACK",	},
      { "type": "input_dummy", "name": "NEWLINE", },
    ],
    "style": "graph_blocks",
    "tooltip": "The graph item data structure to create.",
    "contextMenu": false,
  },
  {
    "type": "graph_options",
    "message0": "edge weights: %1, node weights: %2,%6 directed: %3,%7 negative weights: %4, multigraph %5",
    "args0": [
      { "type": "field_checkbox", "name": "EVAL", "checked": true },
      { "type": "field_checkbox", "name": "NVAL", "checked": false },
      { "type": "field_checkbox", "name": "DIR", "checked": true },
      { "type": "field_checkbox", "name": "NEG", "checked": false },
      { "type": "field_checkbox", "name": "MULTI", "checked": false },
      { "type": "input_dummy", "name": "newline1", },
      { "type": "input_dummy", "name": "newline2", },
    ],
    "inputsInline": false,
    "output": Blockly.Constants.Object.OUTPUT,
    "style": "graph_blocks",
    "tooltip": "Generates graph options.",
  },
  ////////////////////////
  // Graph Manipulation //
  ////////////////////////
  {
    "type": "graph_data_add",
    "message0": "add %1 %4%2 to %3",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["node", "NODE"], ["edge", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": function() {
        return "Adds a " + this.getFieldValue("TYPE").toLowerCase() + " to a specified graph.";
      },
    "extensions": ["graph_data_receive_type_extension", "function_tooltip"],
  },
  {
    "type": "graph_data_update",
    "message0": "update %1 %5%2 with new data %3 to %4",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["node", "NODE"], ["edge", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "UPDATE", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": function() {
        return "Updates some " + this.getFieldValue("TYPE").toLowerCase() + "'s data in a specified graph.";
      },
    "extensions": ["graph_data_receive_type_extension", "graph_data_update_extension", "function_tooltip"],
  },
  {
    "type": "graph_data_remove",
    "message0": "remove all %1 where %4%2 from %3",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["nodes", "NODE"], ["edges", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": function() {
      return "Remove all " + this.getFieldValue("TYPE").toLowerCase() + "s from a graph that meet specified criteria.";
    },
    "extensions": ["graph_data_receive_type_extension", "function_tooltip"],
  },
  {
    "type": "graph_data_highlight",
    "message0": "highlight %1 with %4%2 in %3",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["node", "NODE"], ["edge", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": function() {
        return "Highlights a " + this.getFieldValue("TYPE").toLowerCase() + " in a specified graph.";
      },
    "extensions": ["graph_data_receive_type_extension", "function_tooltip"],
  },
  {
    "type": "graph_data_copy",
    "message0": "copy %1 with %6%2(including style %3) from %4 to %5",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["node", "NODE"], ["edge", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "field_checkbox", "name": "STYLE", "checked": false, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "input_value", "name": "TARGET", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": function() {
        var type = this.getFieldValue("TYPE").toLowerCase();
        return "Copies a " + type + " from one graph to another. If multiple data items received will copy them all. If the item exists in the second graph will overwrite the data.";
      },
    "extensions": ["graph_data_receive_type_extension", "function_tooltip"],
  },
  //////////////////////
  // Graph Getters    //
  //////////////////////
  {
    "type": "graph_data_get_single",
    "message0": "get %1 %2 with %5%3 from %4",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["node", "NODE"], ["edge", "EDGE"]], },
      { "type": "input_dummy", "name": "RETURN_DUMMY" },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
    ],
    "inputsInline": false,
    "output": null,
    "style": "graph_blocks",
    "tooltip": function() {
        var type = this.getFieldValue("TYPE").toLowerCase();
        var ret = this.getFieldValue("RETURN").toLowerCase();
        return "Gets a single " + type + "'s " + ret + " from a graph. If multiple " + type + "s meet the criteria, will return the first.";
      },
    "extensions": ["graph_data_receive_type_extension", "graph_return_dropdown_extension", "function_tooltip"],
  },
  {
    "type": "graph_data_get_multi",
    "message0": "%4%7%6 %1%8 (ID's) where %5%2 from %3",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["nodes", "NODE"], ["edges", "EDGE"]], },
      { "type": "input_value", "name": "DATA", "check": Blockly.Constants.Object.OUTPUT, },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_dropdown", "name": "FILTER", "options": [
        ["-", "UNDEFINED"], ["get the cheapest", "MIN"], ["get the most expensive", "MAX"], ["sort (weight, ascending)", "SORTASC"], ["sort (weight, descending)", "SORTDSC"]
      ], },
      { "type": "field_label", "name": "WITHLABEL", "text": "", },
      { "type": "field_label", "name": "INSTR", "text": "in", },
      { "type": "input_dummy", "name": "newline1", },
      { "type": "input_dummy", "name": "newline2", },
    ],
    "inputsInline": false,
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
        var type = this.getFieldValue("TYPE").toLowerCase();
        var filter = this.getFieldValue("FILTER").toLowerCase();
        if (filter === "min" || filter === "max") {
          return "Gets the ID of the " + filter + "imum weighted " + type + " from a graph, out of all " + type + "s that meet specified criteria.";
        } else {
          return "Gets a list of " + type + "s' IDs, sorted by weight, from a graph that meet specified criteria.";
        }
      },
    "extensions": ["graph_data_receive_type_extension", "function_tooltip", "graph_many_filter_sort_extension",],
  },
  {
    "type": "graph_data_get_all",
    "message0": "%3%5%4 %1 (ID's) from %2",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["nodes", "NODE"], ["edges", "EDGE"]], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_dropdown", "name": "FILTER", "options": [
        ["-", "UNDEFINED"], ["get the cheapest", "MIN"], ["get the most expensive", "MAX"], ["sort (weight, ascending)", "SORTASC"], ["sort (weight, descending)", "SORTDSC"]
      ], },
      { "type": "field_label", "name": "INSTR", "text": "in", },
      { "type": "input_dummy", "name": "newline1", },
    ],
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
      var type = this.getFieldValue("TYPE").toLowerCase();
      var filter = this.getFieldValue("FILTER").toLowerCase();
      if (filter === "min" || filter === "max") {
        return "Gets the ID of the " + filter + "imum weighted " + type + " from a graph, out of all " + type + "s from a graph.";
      } else {
        return "Gets a list of " + type + "s' IDs, sorted by weight, from a graph.";
      }
    },
    "extensions": ["function_tooltip", "graph_many_filter_sort_extension",],
  },
  {
    "type": "graph_data_get_by_degree",
    "message0": "%3%5%4 nodes (ID's) with degree %1 from  %2",
    "args0": [
      { "type": "input_value", "name": "DEGREE", "check": ["Number", "Array"], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_dropdown", "name": "FILTER", "options": [
        ["-", "UNDEFINED"], ["get the cheapest", "MIN"], ["get the most expensive", "MAX"], ["sort (weight, ascending)", "SORTASC"], ["sort (weight, descending)", "SORTDSC"]
      ], },
      { "type": "field_label", "name": "INSTR", "text": "in", },
      { "type": "input_dummy", "name": "newline1", },
    ],
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
      var type = this.getFieldValue("TYPE").toLowerCase();
      var filter = this.getFieldValue("FILTER").toLowerCase();
      if (filter === "min" || filter === "max") {
        return "Gets the ID of the " + filter + "imum weighted " + type + " from a graph, out of all " + type + "s with a given degree.";
      } else {
        return "Gets a list of " + type + "s' IDs, sorted by weight, with a given degree.";
      }
    },
    "extensions": ["graph_many_filter_sort_extension", "function_tooltip",],
  },
  {
    "type": "graph_data_ids_to_data",
    "message0": "convert %1 IDs list %2 for graph %3 to data: %4",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["nodes", "NODE"], ["edges", "EDGE"]], },
      { "type": "input_value", "name": "IDS", "check": "Array", },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "input_dummy", "name": "RETURN_DUMMY" },
    ],
    "inputsInline": false,
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": "Converts a list of IDs to another aspect of data for that item.",
    "extensions": ["graph_return_dropdown_extension",],
  },
  //////////////////////
  // Graph Queries    //
  //////////////////////
  {
    "type": "graph_query_connected",
    "message0": "is %1 connected",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": true,
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Checks if a graph is connected. Will return 'partial' for partially connected directed graphs.",
  },
  {
    "type": "graph_query_connected_node",
    "message0": "is %1 connected to %2 in %3",
    "args0": [
      { "type": "input_value", "name": "NODE1", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "NODE2", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Checks if a node is reachable from another specified node.",
  },
  {
    "type": "graph_query_is_node",
    "message0": "is %1 a node in %2",
    "args0": [
      { "type": "input_value", "name": "NODE", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Checks if a node exists a specified label or data exists. Will return the node ID or false, but can be interpreted as a boolean.",
  },
  {
    "type": "graph_query_is_edge",
    "message0": "is there an edge from %1 to %2 in %3",
    "args0": [
      { "type": "input_value", "name": "NODE1", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "NODE2", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Checks if a specific directed edge exists. Will return the edge ID, but can be interpreted as a boolean.",
  },
  {
    "type": "graph_query_is_adjacent",
    "message0": "is there an edge between %1 and %2 in %3",
    "args0": [
      { "type": "input_value", "name": "NODE1", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "NODE2", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Checks if a edge exists exists between two nodes. Will return the edge ID, but can be interpreted as a boolean.",
  },
  {
    "type": "graph_query_get_sourcesink",
    "message0": "%3%5%4 %1 nodes%6 (ID's) in %2",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["source", "SOURCE"], ["sink", "SINK"]], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_dropdown", "name": "FILTER", "options": [
        ["-", "UNDEFINED"], ["get the cheapest", "MIN"], ["get the most expensive", "MAX"], ["sort (weight, ascending)", "SORTASC"], ["sort (weight, descending)", "SORTDSC"]
      ], },
      { "type": "field_label", "name": "INSTR", "text": "in", },
      { "type": "input_dummy", "name": "newline1", },
      { "type": "input_dummy", "name": "newline2", },
    ],
    "inputsInline": false,
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
      var type = this.getFieldValue("TYPE").toLowerCase();
      var filter = this.getFieldValue("FILTER").toLowerCase();
      if (filter === "min" || filter === "max") {
        return "Gets the ID of the " + filter + "imum weighted " + type + " node from a graph.";
      } else {
        return "Gets a list of " + type + " nodes' IDs, sorted by weight.";
      }
    },
    "extensions": ["graph_many_filter_sort_extension", "function_tooltip",],
  },
  {
    "type": "graph_query_get_neighbours",
    "message0": "%5%7%6 %2 %1%8 (ID's) from %3 in %4",
    "args0": [
      { "type": "field_dropdown", "name": "TYPE", "options": [["neighbouring nodes", "NODE"], ["adjacent edges", "EDGE"]], },
      { "type": "field_dropdown", "name": "DIR", "options": [["-", "ALL"], ["incoming", "IN"], ["outgoing", "OUT"]], },
      { "type": "input_value", "name": "NODE", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "field_dropdown", "name": "FILTER", "options": [
        ["-", "UNDEFINED"], ["get the cheapest", "MIN"], ["get the most expensive", "MAX"], ["sort (cost, ascending)", "SORTASC"], ["sort (cost, descending)", "SORTDSC"]
      ], },
      { "type": "field_label", "name": "INSTR", "text": "in", },
      { "type": "input_dummy", "name": "newline1", },
      { "type": "input_dummy", "name": "newline2", },
    ],
    "inputsInline": false,
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
      var type = this.getFieldValue("TYPE") === "NODE" ? "neighbouring node" : "adjacent edge";
      var dir = this.getFieldValue("DIR").toLowerCase();
      var filter = this.getFieldValue("FILTER").toLowerCase();
      switch (dir) {
        case "all":
          dir = ""; break;
        case "in":
          dir = "incoming "; break;
        case "out":
          dir = "outgoing "; break;
      }

      if (filter === "min" || filter === "max") {
        return "Gets the ID of the " + filter + "imum weighted " + dir + type + " from a given node.";
      } else {
        return "Gets a list of " + dir + type + "s ID's from a given node, sorted by cost.";
      }
    },
    "extensions": ["function_tooltip", "graph_many_filter_sort_extension",],
  },
  {
    "type": "graph_query_get_shortest_paths",
    "message0": "get all shortests paths from %1 in %2",
    "args0": [
      { "type": "input_value", "name": "NODE", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": Blockly.Constants.Object.OUTPUT,
    "style": "graph_blocks",
    "tooltip": "Gets an array of the shortest path to any connected node, starting at the one specified. The final element of each path is the destination node.",
  },
  {
    "type": "graph_query_get_shortest_path_to",
    "message0": "get %1 from %2 to %3 in %4",
    "args0": [
      { "type": "field_dropdown", "name": "RETURN", "options": [["shortest path", "PATH"], ["smallest cost", "COST"]], },
      { "type": "input_value", "name": "NODE1", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "NODE2", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": function() {
      var coststr = this.getFieldValue("RETURN") === "PATH" ? "" : "cost of the";
      return "Gets the " + coststr + " shortest path between two nodes in a graph.";
    },
    "extensions": ["function_tooltip", "shortest_path_dropdown"],
  },
  {
    "type": "graph_query_get_cost_to",
    "message0": "get the cost from %1 to %2 in %3",
    "args0": [
      { "type": "input_value", "name": "NODE1", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "NODE2", "check": ["String", Blockly.Constants.Object.OUTPUT,], },
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "inputsInline": false,
    "output": "Number",
    "style": "graph_blocks",
    "tooltip": "Gets the cost between two nodes in a graph.",
  },
  //////////////////////
  // Graph Management //
  //////////////////////
  {
    "type": "graph_manage_create",
    "message0": "create graph with name %1",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "output": "String",
    "style": "graph_blocks",
    "tooltip": "Creates a graph and return's its ID.",
  },
  {
    "type": "graph_manage_clear",
    "message0": "clear graph with name %1",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": "Clears all data in a graph.",
  },
  {
    "type": "graph_manage_set_options",
    "message0": "set options to %2 in %1",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "input_value", "name": "OPTS", "check": Blockly.Constants.Object.OUTPUT, },
    ],
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": "Sets the options in a graph.",
  },
  {
    "type": "graph_manage_destroy",
    "message0": "destroy graph with name %1",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": "Destroys a graph.",
  },
  {
    "type": "graph_manage_get_names",
    "message0": "get list of graph names",
    "output": "Array",
    "style": "graph_blocks",
    "tooltip": "Lists all current graph that exist.",
  },
  {
    "type": "graph_manage_set_view",
    "message0": "view graph with name %1",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
    ],
    "previousStatement": null,
    "nextStatement": null,
    "style": "graph_blocks",
    "tooltip": "Views a graph a graph.",
  },
  {
    "type": "graph_manage_copy",
    "message0": "copy graph %1 with new name %2",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "input_value", "name": "NAME", "check": "String", },
    ],
    "inputsInline": false,
    "output": "String",
    "style": "graph_blocks",
    "tooltip": "Copies a graph and returns the new ID of the graph.",
  },
  {
    "type": "graph_manage_rename",
    "message0": "rename graph with name %1 to %2",
    "args0": [
      { "type": "input_value", "name": "GRAPH", "check": "String", },
      { "type": "input_value", "name": "NAME", "check": "String", },
    ],
    "output": ["Boolean", "String",],
    "style": "graph_blocks",
    "tooltip": "Renames a graph and return false if failed, or the new graph ID.",
  },
]);

// extension method to allow functions as tooltips in JSON definition. Saves writing many extension methods for same functionality
Blockly.Extensions.register("function_tooltip",
  function() {
    if (typeof this.tooltip === "function") {
      this.setTooltip(this.tooltip.bind(this));
    }
  }
);

// extension method to generate a correct dropdown of graph names
Blockly.Extensions.register("graph_select_dropdown_extension",
  function() {
    this.getInput("DUMMY")
        .appendField(
          new Blockly.FieldDropdown(function() {
            var graphs = [ ];
            graphs.push(["Current", "CURRENT"]); // current graph, undefined as value

            // add each name and id
            var names = GRAPH.getGraphNames();
            for (var i in names) {
              graphs.push([ names[i], names[i] ]);
            }
            return graphs;
          }), "GRAPH");
  }
);

// extension method to update graph data type when type dropdown changes, and allow vice versa
Blockly.Extensions.register("graph_data_receive_type_extension",
  function() {
    // make sure the type of the graph data is same as the dropdown
    this.getField("TYPE").setValidator(function(t) {
      // get data input field
      var dataBlock = this.getSourceBlock().getInputTargetBlock("DATA");
      if (dataBlock !== null && dataBlock.type === "graph_data") {
        // set the type of the graph data block
        dataBlock.updateType(t);
      }

      this.getSourceBlock().updateGraphCheck(t);
    });

    // function to update the type, used with the close mutator code
    this.updateTypeField = function(t) {
      this.setFieldValue(t, "TYPE");
      this.updateGraphCheck(t);
    };

    // updates the check field on a graph data field. Accept string if in node mode
    this.updateGraphCheck = function(t) {
      var check = Blockly.Constants.Object.OUTPUT;
      if (t === "NODE") {
        check = [check, "String"];
      }
      this.getInput("DATA").setCheck(check);
      this.updateNodeAsString(t);
    };

    // updates the label to include "with label" when connecting a string to this block
    this.updateNodeAsString = function(t) {
      var string = true;
      if (!this.rendered) {
        string = false;
      }

      try {
        var child = this.getInput("DATA").connection.targetBlock();
        string = !child.outputConnection.check_.includes("Object");
      } catch (error) { }

      if ((t && t === "EDGE") || (!t && this.getFieldValue("TYPE") === "EDGE")) {
        string = false;
      }

      if (string) {
        this.setFieldValue("label =", "WITHLABEL");
      } else {
        this.setFieldValue("", "WITHLABEL");
      }
    }

    // render the 'label = ' correctly on first render
    this.oldRender = this.render;
    this.render = function(b) {
      this.oldRender(b);
      this.updateNodeAsString();
      this.render = this.oldRender;
      setTimeout(function() { delete this.oldRender }, 10);
    };
  }
);

// forces the update block to be dominated by the type of this update block
Blockly.Extensions.register("graph_data_update_extension",
  function() {
    // make sure the type of the update data is force to be same as block
    this.getField("TYPE").oldValidator_ = this.getField("TYPE").validator_;
    this.getField("TYPE").setValidator(function(t) {
      // get data input fields
      var updateBlock = this.getSourceBlock().getInputTargetBlock("UPDATE");
      if (updateBlock !== null && updateBlock.type === "graph_data") {
        // set the type of the graph data block, then update it's shape
        updateBlock.updateType(t, false);
      }

      this.oldValidator_(t);
    });
  }
)

// forces the return type based on the current return dropdown
Blockly.Extensions.register("graph_return_dropdown_extension",
  function() {
    // list of different values with the corrosponding type required and output type
    var items = [
      ["ID", "id", null, "String"], ["weight", "value", null, "Number"], ["degree", "degree", "NODE", ["Number", "Array"]],
      ["endpoints", "endpoints", "EDGE", "Array"], ["from", "from", "EDGE", "String"], ["to", "to", "EDGE", "String"],
      ["data (object)", "DATA", null, Blockly.Constants.Object.OUTPUT],
      ["colour", "color", null, "Colour"], ["shape", "shape", "NODE", "String"], ["label", "label", "EDGE", "String"],
      ["style (object)", "STYLE", null, Blockly.Constants.Object.OUTPUT],
      ["data + style (object)", "FULL", null, Blockly.Constants.Object.OUTPUT],
    ];

    this.getInput("RETURN_DUMMY")
        .appendField(
          new Blockly.FieldDropdown(function() {
            // generate a list of return values allowed given the current type
            var dropdown = [];
            var type = this.getSourceBlock() ? this.getSourceBlock().getFieldValue("TYPE") : null;
            var validValue = !this.getValue(); // assume invalid value
            for (var i in items) {
              if (i === 0 && this.getSourceBlock() && this.getSourceBlock.type === "graph_data_ids_to_data") {
                // do not convert IDs to IDs
                continue;
              }

              // get item and add if valid for the current type of block
              var item = items[i];
              if (type === null || item[2] === null || item[2] === type) {
                // check if we have saved valid value yet
                if (!validValue && this.getValue() === item[1]) {
                  validValue = true;
                }

                // push to dropdown
                dropdown.push(item.slice(0, 2));
              }
            }

            // reset the current value if we have an invalid value
            if (!validValue) {
              this.setValue(dropdown[0][1]);
            }
            return dropdown;
          }), "RETURN");

    // set validator to update the output for this block
    if (this.type !== "graph_data_ids_to_data") {
      this.getField("RETURN").setValidator(function(v) {
        // get which item we have
        var item = items[0];
        for (var i in items) {
          if (v === items[i][1]) {
            item = items[i];
            break;
          }
        }

        // set the output type
        this.getSourceBlock().setOutput(true, item[3]);
      });
    }

    // update the dropdown when type changes
    this.getField("TYPE").oldValidator_ = this.getField("TYPE").validator_;
    this.getField("TYPE").setValidator(function(t) {
      // run other validator if it exists
      if (this.oldValidator_) {
        this.oldValidator_(t);
      }

      // update the dropdown to have a valid value
      this.value_ = t;
      this.getSourceBlock().getField("RETURN").getOptions(false);
    });
  }
);

// sets the correct output type for a block getting many data which can be sorted or get the most extreme value
Blockly.Extensions.register("graph_many_filter_sort_extension",
  function() {
    // set validator to update the output for this block
    this.getField("FILTER").setValidator(function(v) {
      // set the output type
      if (v === "MIN" || v === "MAX") {
        this.getSourceBlock().setOutput(true, "String");
        this.getSourceBlock().setFieldValue("in", "INSTR");
      } else {
        this.getSourceBlock().setOutput(true, "Array");
        this.getSourceBlock().setFieldValue("all", "INSTR");
      }
    });
  }
);

// enforce the output of the block based on dropdown value
Blockly.Extensions.register("shortest_path_dropdown",
  function() {
    this.getField("RETURN").setValidator(function(val) {
      if (val === "PATH") {
        this.getSourceBlock().setOutput(true, "Array");
      } else {
        this.getSourceBlock().setOutput(true, "Number");
      }
    })
  }
);

/**
 * Corrects the type of a block with the "graph_data_receive_type_extension" on a move event.
 * @param event the blockly event to handle.
 **/
Blockly.Constants.Graph.DATA.Events.blockConnectListener = function(event) {
  if (event.type === Blockly.Events.MOVE) {
    // if block was graph_data, run the update parent method
    var block = Blockly.Workspace.getById(event.workspaceId).getBlockById(event.blockId);
    if (block) {
      if (block.type === "graph_data") {
        block.updateParentGraphDataType_();
      } else if (block.updateNodeAsString) {
        block.updateNodeAsString();
      } else {
        // update the label field if connected a random block to the extended block
        var parent = block.getParent();
        if (parent && parent.updateNodeAsString) {
          parent.updateNodeAsString();
        }
      }
    }
  }
}

/**
 * Generates a mutator setting item based off some config data.
 * @param name the name of the mutator item.
 * @param label the label of the mutator item.
 **/
Blockly.Constants.Graph.DATA.MUTATOR_GEN = function(name, label) {
  Blockly.Blocks[name] = {
    init: function() { Blockly.Constants.Object.NEW.MUTATOR_GEN_INIT(this, label, "graph_blocks")(); },
  };
}
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_label", "label");
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_colour", "colour"); // use internal american spelling for vis.js
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_weight", "weight"); // value used in vis.js, both fixed in process properties
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_shape", "shape");
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_to", "to");
Blockly.Constants.Graph.DATA.MUTATOR_GEN("graph_data_from", "from");

// Heavily based on the Blockly.Constants.Object.NEW.MUTATOR_MIXIN mutator
// has updated generator functions in order to work correctly in this use case
Blockly.Constants.Graph.DATA.MUTATOR_MIXIN = {
  data_: {
    type: "NODE",     // node or edge type graph data
    properties: [],   // properties defined for this graph setup
  },

  text_: {
    PREFIX: "Node {",
    POSTFIX: "}",
  },

  mutationToDom: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.mutationToDom,
  domToMutation: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.domToMutation,
  decompose: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.decompose,
  compose: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.compose,
  saveConnections: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.saveConnections,
  updateShape_: Blockly.Constants.Object.NEW.MUTATOR_MIXIN.updateShape_,

  decomposeContainer: function(workspace) {
    var containerBlock = workspace.newBlock("graph_data_container");
    containerBlock.setFieldValue(this.data_.type, "TYPE");
    return containerBlock;
  },

  decomposeBlock: function(workspace, property) {
    return workspace.newBlock("graph_data_" + property);
  },

  processProperties: function(properties, containerBlock) {
    if (containerBlock !== undefined) {
      // get type from the container block
      var t = containerBlock.getFieldValue("TYPE");
      if (t !== null) {
        if (this.data_.type !== t) {
          this.updateType(t);
        }
      }
    }
    this.text_.PREFIX = this.data_.type[0].toUpperCase() + this.data_.type.slice(1).toLowerCase() + " {";

    // update our properties if none supplied
    var updateSelf = false;
    if (properties === undefined) {
      properties = this.data_.properties;
      updateSelf = true;
    }

    // add in forced properties
    var forced = Blockly.Constants.Graph.DATA.GetForcedValue(this);
    if (forced) {
      properties = properties.concat(forced);
    }

    // remove duplicates
    var i;
    properties = properties.filter(function(i, p) { return properties.indexOf(i) === p; });

    // remove invalid items
    if (this.data_.type !== "NODE") {
      if ((i = properties.indexOf("shape")) !== -1) {
        properties.splice(i, 1);
      }
    } else if (this.data_.type !==  "EDGE"){
      if ((i = properties.indexOf("to")) !== -1) {
        properties.splice(i, 1);
      }
      if ((i = properties.indexOf("from")) !== -1) {
        properties.splice(i, 1);
      }
    }

    // if we are updating our self, update shape also
    if (updateSelf) {
      this.data_.properties = properties;
      this.updateShape_();
    }

    return properties;
  },

  generateInput: function(property, first) {
    if (first === undefined) {
      first = false;
    }

    // generate uneditable property name
    var fieldLabel = new Blockly.FieldLabelSerializable(property);

    // generate a check type for the property
    var check = property === "colour" ? "Colour" : property === "weight" ? "Number" : "";

    // create input for property
    var input;
    if (property === "shape") {
      input = this.appendDummyInput("K" + property)
    } else {
      input = this.appendValueInput("K" + property)
    }

    // add text as required
    if (!first) {
      input.appendField(", ", "K" + property + "COMMA");
    }
    input.appendField(fieldLabel)
         .appendField("=")
         .setAlign(Blockly.ALIGN_RIGHT);

    // add dropdown for shape, or type checking
    if (property === "shape") {
      // add shape dropdown
      input.appendField(new Blockly.FieldDropdown(
        [["Circle", "circle"], ["Ellipse", "ellipse"], ["Square", "box"],
        ["Triangle", "triangle"], ["Triangle Downwards", "triangleDown"],
        ["Hexagon", "hexagon"], ["Diamond", "diamond"], ["Star", "star"]]
      ), "K" + property);
    } else {
      input.setCheck(check);
    }
  },

  cleanup: function() {},

  /**
   * Updates the type and reprocesses properties.
   * @param type the type to update to.
   * @param updateParent if the block should update the parent. Default to true.
   **/
  updateType: function(type, updateParent) {
    if (this.data_.type !== type) {
      this.data_.type = type;
      this.processProperties();
      if (updateParent === undefined || updateParent) {
        this.updateParentGraphDataType_();
      }
    }
  },

  /**
   * Updates a parent block to the correct node / edge type if the parent
   * uses the "graph_data_receive_type_extension" extension.
   **/
  updateParentGraphDataType_: function() {
    var parent = this.getParent();
    if (parent !== null && parent.updateTypeField !== undefined) {
      // parent needs updating "graph_data_receive_type_extension"
      var data = parent.getInput("DATA") && parent.getInput("DATA").connection.targetBlock();
      var update = parent.getInput("UPDATE") && parent.getInput("UPDATE").connection.targetBlock();
      if (data && data.id === this.id) {
        // update parent as this is data
        parent.updateTypeField(this.data_.type);
        parent.updateNodeAsString(false);

        // reprocess properties
        this.processProperties();
      } else if (update && update.id === this.id) {
        // this is a update data block in the update block, force type to copy parent
        if (this.data_.type !== parent.getFieldValue("TYPE")) {
          // only force the rerun if needed to due to change in value
          this.updateType(parent.getFieldValue("TYPE"));
          if (this.mutator && this.mutator.workspace_) {
            // if the mutator is open, update the mutator to force back into correct mode
            this.mutator.workspace_.getBlocksByType("graph_data_container")[0].setFieldValue(this.data_.type, "TYPE");
          }
        }
      }
    }

    // update the mutator if its open
    if (this.mutator && this.mutator.workspace_) {
      // if mutator open, update the forced blocks in the mutator, could have changed in disconnect
      Blockly.Constants.Graph.DATA.Events.updateForcedBlocks_(this.mutator.workspace_);
    }
  },
};

// register mutator
Blockly.Extensions.registerMutator("graph_data_mutator",
  Blockly.Constants.Graph.DATA.MUTATOR_MIXIN, null,
  ["graph_data_label", "graph_data_colour", "graph_data_weight", "graph_data_shape", "graph_data_to", "graph_data_from"]
);

/**
 * Listens for change in a graph data mutator.
 * @param event the event triggering the listener.
 **/
Blockly.Constants.Graph.DATA.Events.mutatorChangeListener_ = function(event) {
  var eventWS = Blockly.Workspace.getById(event.workspaceId);
  Blockly.Constants.Graph.DATA.Events.updateMutatorFlyout_(eventWS);

  if (event.type === Blockly.Events.BLOCK_CHANGE || event.type === Blockly.Events.BLOCK_MOVE) {
    Blockly.Constants.Graph.DATA.Events.updateForcedBlocks_(eventWS);
  } else if (event.type === Blockly.Events.BLOCK_CREATE) {
    Blockly.Constants.Graph.DATA.Events.preventDuplicates_(eventWS, event);
  }
}

/**
 * Updates a graph data mutator workspace with correctly disabled fields.
 * @param mutatorWS the workspace of the mutator.
 **/
Blockly.Constants.Graph.DATA.Events.updateMutatorFlyout_ = function(mutatorWS) {
  // function to delete all blocks of a given type, or types if array supplied
  function disposeOfType(type) {
    if (Array.isArray(type)) {
      // dispose of each type in array
      for (var i in type) {
        disposeOfType(type[i]);
      }
    } else {
      // get all blocks, dispose each of them
      toDisable.push(type);
      mutatorWS.getBlocksByType(type).forEach(function(block) {
        try {
          block.unplug(true);
          block.dispose();
        } catch (e) { /* sometimes too quick in testing, needed to avoid errors */ }
      });
    }
  }

  // verify we need to run the update function
  var toDisable = mutatorWS.getAllBlocks().map(function(b) { return b.type; });

  // check if invalid properties need disabling and removing from workspace
  var container = mutatorWS.getBlocksByType("graph_data_container")[0];
  if (container) {
    if (container.getFieldValue("TYPE") !== "NODE") {
      // remove active node data blocks and add to disable
      disposeOfType("graph_data_shape");
    } else if (container.getFieldValue("TYPE") !== "EDGE") {
      // remove active edge data blocks and add to disable
      disposeOfType([ "graph_data_from", "graph_data_to" ]);
    }
  }

  toDisable.filter(function(v, i, s) { return s.indexOf(v) === i }); // remove duplicates
  toDisable.splice(toDisable.indexOf("graph_data_container"), 1); // remove container block

  // enable/disable toolbox blocks as required
  if (mutatorWS.flyout_) {
    mutatorWS.flyout_.workspace_.getAllBlocks().forEach(function(b) {
      var disabled = toDisable.includes(b.type);
      b.setEnabled(!disabled);
    });
  }
}


// dictionary of forced data based on data parents
Blockly.Constants.Graph.DATA.FORCED_DATA = {
  "graph_data_add": { "EDGE": ["to", "from"] },
}

/**
 * Gets an array of forced data values in a mutator given a parent and type.
 * @param block the graph data block that we want to check if there are forced data for.
 * @return an array of forced data items for the block.
 **/
Blockly.Constants.Graph.DATA.GetForcedValue = function(block) {
  var parent = block.getParent();
  var type = block.data_.type;
  if (parent &&
      Blockly.Constants.Graph.DATA.FORCED_DATA[parent.type] &&
      Blockly.Constants.Graph.DATA.FORCED_DATA[parent.type][type]) {
    return Blockly.Constants.Graph.DATA.FORCED_DATA[parent.type][type].slice();
  } else {
    return [];
  }
}

/**
 * Updates a graph data mutator to have any forced blocks depending on the parent block.
 * @param mutatorWS the workspace of the mutator.
 **/
Blockly.Constants.Graph.DATA.Events.updateForcedBlocks_ = function(mutatorWS) {
  /**
   * Appends a block to the end of the container stack.
   * @param block the block to add to the container stack.
   **/
  function appendToEndOfStack(block) {
    var stack = container.getInputTargetBlock("STACK");
    if (stack) {
      // something in stack, get last block to connect to
      while (true) {
        var next = stack.getNextBlock();
        if (next) {
          stack = next;
        } else {
          break;
        }
      }

      // connect the block to end of stack
      stack.nextConnection.connect(block.previousConnection);
    } else {
      // no stack currently, make one and connect
      container.getInput("STACK").connection.connect(block.previousConnection);
    }
  }

  // first make all blocks useable incase there was change
  mutatorWS.getAllBlocks().forEach(function(b) {
    if (b.type !== "graph_data_container") {
      b.setMovable(true);
      b.setDeletable(true);
    }
  });

  // get any forced data items
  var forced = Blockly.Constants.Graph.DATA.GetForcedValue(mutatorWS.mutatorOrigin);
  if (forced.length) {
    var forcedXml = ""; // xml of forced blocks to add
    var container = mutatorWS.getBlocksByType("graph_data_container")[0];

    // iterate over the forced blocks
    for (var i in forced) {
      // get blocks of this type
      var exist = false;
      mutatorWS.getBlocksByType("graph_data_" + forced[i]).forEach(function(block) {
        // executing loop, block exists and mark it as such
        exist = true;

        // set it as unmoveable and undeletable
        block.setMovable(false);
        block.setDeletable(false);

        // check it is contained, and force it into place
        if (block.getRootBlock() !== container) {
          block.unplug(true);
          block.setDragging(false);
          appendToEndOfStack(block);
        }
      });

      // add the block to the new xml we are creating if needs be
      if (!exist) {
        forcedXml = "<block type=\"graph_data_" + forced[i] + "\" moveable=\"false\">\n"
                    + (forcedXml !== "" ? "\n<next>\n" + forcedXml + "</next>\n" : "")
                    + "</block>";
      }
    }

    // create the new blocks if needed
    if (forcedXml !== "") {
      // create blocks from xml
      var forcedBlocks = Blockly.Xml.domToBlock(Blockly.Xml.textToDom(forcedXml), mutatorWS);

      // connect to main block
      appendToEndOfStack(forcedBlocks);
    }
  }
}

/**
 * Prevents multiple blocks existing in a graph data mutator.
 * @param mutatorWS the workspace of the mutator.
 * @param createEvent the create block event.
 **/
Blockly.Constants.Graph.DATA.Events.preventDuplicates_ = function(mutatorWS, createEvent) {
  for (var i in createEvent.ids) {
    // get the new block
    var block = mutatorWS.getBlockById(createEvent.ids[i]);
    if (block) {
      // get how many of this block exist (that are not being dragged)
      var count = mutatorWS.getBlocksByType(block.type).filter(function(b) { return !b.isInsertionMarker_; }).length;

      // delete this block if this is not the only block of this type to exist
      if (count > 1) {
        block.dispose();
      }
    }
  }
}
