/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Generating JavaScript for helper blocks.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

goog.provide("Blockly.JavaScript.helpers");

goog.require("Blockly.JavaScript");

Blockly.JavaScript["helpers_main"] = function(block) {
	return "";
}

Blockly.JavaScript["helpers_comment"] = function(block) {
	var comment = block.getFieldValue("COMMENT");
	return "// " + comment + "\n";
};

Blockly.JavaScript["helpers_variable_swap"] = function(block) {
  var one = Blockly.JavaScript.variableDB_.getName(block.getFieldValue("VAR1"), Blockly.Variables.NAME_TYPE);
  var two = Blockly.JavaScript.variableDB_.getName(block.getFieldValue("VAR2"), Blockly.Variables.NAME_TYPE);
  return "var holder = " + one + "; " + one + " = " + two + "; " + two + " = holder;\n";
};

Blockly.JavaScript["helpers_typeof"] = function(block) {
  var v = Blockly.JavaScript.variableDB_.getName(block.getFieldValue("VAR"), Blockly.Variables.NAME_TYPE);
  return ["typeof " + v, Blockly.JavaScript.ORDER_TYPEOF];
};

Blockly.JavaScript["helpers_lists_is_in"] = function(block) {
  var item = Blockly.JavaScript.valueToCode(block, "ITEM", Blockly.JavaScript.ORDER_NONE);
  var list = Blockly.JavaScript.valueToCode(block, "LIST", Blockly.JavaScript.ORDER_ATOMIC) || "[]";
  return [list + ".indexOf(" + item + ") != -1", Blockly.JavaScript.ORDER_EQUALITY];
};

Blockly.JavaScript["helpers_lists_set_funcs"] = function(block) {
  var one = Blockly.JavaScript.valueToCode(block, "LIST1", Blockly.JavaScript.ORDER_ATOMIC) || "[]";
  var two = Blockly.JavaScript.valueToCode(block, "LIST2", Blockly.JavaScript.ORDER_ATOMIC) || "[]";
  var op = block.getFieldValue("OPERATION");
  var code = "";
  switch (op) {
    case "UNION":
      code = "[..." + one + ", ..." + two + "]";
      break;
    case "INTERSECTION":
      code = one + ".filter(function(x) { return " + two + ".indexOf(x) != -1 })";
      break;
    case "DIFFERENCE":
      code = one + ".filter(function(x) { return " + two + ".indexOf(x) == -1 })";
      break;
    case "SYMETRIC_DIFFERENCE":
      code = one + ".filter(function(x) { return !" + two + ".indexOf(x) != -1 })" +
               ".concat(" + two + ".filter(function(x) { return !" + one + ".indexOf(x) != -1 }));";
      break;
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};
