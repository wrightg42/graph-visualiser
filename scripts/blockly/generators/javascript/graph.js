/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Generating JavaScript for graph blocks.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

goog.provide("Blockly.JavaScript.graph");

goog.require("Blockly.JavaScript");
goog.require("Blockly.JavaScript.object");
// goog.require("Blockly.Constants.Graph"); // does not like to compile as not in this compile block

// Repeatedly used code, defined as constant functions to make code consistant and reusable
Blockly.Constants.Graph.JavaScript = { }

// returns string of data set in DATA input
Blockly.Constants.Graph.JavaScript.DATA_CODE = function(block, input, noval) {
  // apply default values
  if (input === undefined) {
    input = "DATA";
  }
  if (noval === undefined) {
    noval = "{}"
  }
  var data = Blockly.JavaScript.valueToCode(block, input, Blockly.JavaScript.ORDER_NONE) || noval;

  // replace label with id or _label when using node/edge mode
  if (data[0] === "{" && data[data.length - 1] === "}") {
    var replacement = "\"id\":";
    if (block.getFieldValue("TYPE") && block.getFieldValue("TYPE") !== "NODE") {
      replacement = "\"_label\":";
    }
    data = data.replace("\"label\":", replacement);
  }
  return data;
}

/**
 * Generates a function call from some given input data.
 * @param fnname the function name to compile a call for.
 * @param args array of string based arguments to supply to the function call.
 * @param options options to apply post processing. Options include NEWLINE, GERPROPERTY (string, what property to return)
 * @return the code for the desired function call.
 **/
Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL = function(fnname, args, options) {
  // apply default values
  if (options === undefined) {
    options = { };
  }

  // compile the function call;
  var code = "GRAPH." + fnname + "(";
  for (var i in args) {
    code += (i == 0 ? "" : ", ") + args[i];
  }

  // remove all trailing "undefined" terms
  while (code.endsWith("undefined")) {
    code = code.slice(0, -9);
    if (code.endsWith(", ")) {
      code = code.slice(0, -2);
    }
  }

  code += ")"; // finish function call

  // process options
  if (options.NEWLINE) {
    code += ";\n";
  }
  return code;
}

Blockly.JavaScript["graph_select"] = function(block) {
  var dropdown_graph = block.getFieldValue("GRAPH");
  var code = "";
  if (dropdown_graph === "CURRENT") {
    code = "GRAPH.getCurrentName()";
  } else {
    code = "\"" + dropdown_graph + "\"";
  }
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript["graph_data"] = function(block) {
  var code = Blockly.JavaScript["object_new"](block);

  // replace stuff to work with vis js
  code[0] = code[0].replace("'colour':", "'color':");
  code[0] = code[0].replace("'weight':", "'value':");
  if (block.data_.type === "NODE") {
    code[0] = code[0].replace("'label':", "'id':");
  }

  return code;
}

Blockly.JavaScript["graph_options"] = function(block) {
  var eweight = block.getFieldValue("EVAL") == "TRUE";
  var nweight = block.getFieldValue("NVAL") == "TRUE";
  var dir = block.getFieldValue("DIR") == "TRUE";
  var neg = block.getFieldValue("NEG") == "TRUE";
  var multi = block.getFieldValue("MULTI") == "TRUE";

  // don't need to call graph.options with new, as this is
  // automatically done by graph options + wrapper function is not a constructor
  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("options",
    [dir, eweight, nweight, neg, multi]
  );
  return [code, Blockly.JavaScript.ORDER_NEW];
}

Blockly.JavaScript["graph_data_add"] = function(block) {
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("add",
      [data, node, graphid], { NEWLINE: true, }
    );
}

Blockly.JavaScript["graph_data_update"] = function(block) {
  var search = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var update = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "UPDATE");
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("setStyle",
      [search, update, node, graphid], { NEWLINE: true, }
    );
}

Blockly.JavaScript["graph_data_highlight"] = function(block) {
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("highlight",
      [data, node, graphid], { NEWLINE: true, }
    );
}

Blockly.JavaScript["graph_data_copy"] = function(block) {
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var style = block.getFieldValue("STYLE") === "TRUE";
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var targetid = Blockly.JavaScript.valueToCode(block, "TARGET", Blockly.JavaScript.ORDER_NONE) || "undefined";

  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("copyData",
      [data, style, node, graphid, targetid], { NEWLINE: true, }
    );
}

Blockly.JavaScript["graph_data_get_single"] = function(block) {
  // get data
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var key = block.getFieldValue("RETURN");
  var fn = key === "id" ? "getSingle" : "getData";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  // assemble params
  var params = [data, node, graphid];
  if (key !== "id") {
    params = [data, "\"" + key + "\"", node, graphid];
  }

  // compile code and return
  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL(fn, params);
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_data_get_multi"] = function(block) {
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var filter = block.getFieldValue("FILTER").toLowerCase(); filter = filter !== "undefined" ? "\"" + filter + "\"" : filter;

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("get",
      [data, node, graphid, filter]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_data_get_all"] = function(block) {
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var filter = block.getFieldValue("FILTER").toLowerCase(); filter = filter !== "undefined" ? "\"" + filter + "\"" : filter;

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("get",
      ["undefined", node, graphid, filter]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_data_get_by_degree"] = function(block) {
  var degree = Blockly.JavaScript.valueToCode(block, "DEGREE", Blockly.JavaScript.ORDER_NONE) || "0";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var filter = block.getFieldValue("FILTER").toLowerCase(); filter = filter !== "undefined" ? "\"" + filter + "\"" : filter;

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("get",
      ["{degree:" + degree + ",}", "true", graphid, filter]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_data_ids_to_data"] = function(block) {
  // get data
  var ids = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "IDS", "[]");
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var key = block.getFieldValue("RETURN");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  // assemble map function
  var mapCode = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("getData",
    ["d", "\"" + key + "\"", node, graphid]
  );

  // assemble full mapping and return
  var code = ids + ".map(function(d) { return " + mapCode + "; })";
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_data_remove"] = function(block) {
  var data = Blockly.Constants.Graph.JavaScript.DATA_CODE(block);
  var node = block.getFieldValue("TYPE") === "NODE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("remove",
      [data, node, graphid], { NEWLINE: true, }
    );
}

Blockly.JavaScript["graph_query_connected"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("connected",
      ["undefined", "undefined", graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_connected_node"] = function(block) {
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE1");
  var node2 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE2", "undefined");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("connected",
      [node1, node2, graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_is_node"] = function(block) {
  var node = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("isNode",
      [node, graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_is_edge"] = function(block) {
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE1");
  var node2 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE2");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("edgeTo",
      [node1, node2, graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_is_adjacent"] = function(block) {
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE1");
  var node2 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE2");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("isAdjacent",
      [node1, node2, graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_get_sourcesink"] = function(block) {
  var type = block.getFieldValue("TYPE") === "SOURCE" ? "true" : "false";
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var filter = block.getFieldValue("FILTER").toLowerCase(); filter = filter !== "undefined" ? "\"" + filter + "\"" : filter;

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("getSourceSink",
      [type, graphid, filter]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_get_neighbours"] = function(block) {
  var fn = block.getFieldValue("TYPE") === "NODE" ? "neighbours" : "incidents";
  var dir = "\"\"";
  switch (block.getFieldValue("DIR")) {
    case "IN":
      dir = "\"to\""; break;
    case "OUT":
      dir = "\"from\""; break;
  }
  var node = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var filter = block.getFieldValue("FILTER").toLowerCase(); filter = filter !== "undefined" ? "\"" + filter + "\"" : filter;

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL(fn,
      [node, dir, graphid, filter]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_get_shortest_paths"] = function(block) {
  var ret = block.getFieldValue("RETURN") === "COST" ? "\"cost\"" : "\"path\"";
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("shortestPath",
      [node1, "undefined", graphid, ret]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_get_shortest_path_to"] = function(block) {
  var ret = block.getFieldValue("RETURN") === "COST" ? "\"cost\"" : "\"path\"";
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE1");
  var node2 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE2");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("shortestPath",
      [node1, node2, graphid, ret]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_query_get_cost_to"] = function(block) {
  var node1 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE1");
  var node2 = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "NODE2");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("costTo",
      [node1, node2, graphid]
    );
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_manage_create"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  if (graphid === "\"\"" || graphid.trim().toLowerCase() === "current") {
    graphid = "undefined";
  }
  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("create", [graphid]);
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_manage_clear"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("clear", [graphid], { NEWLINE: true });
}

Blockly.JavaScript["graph_manage_set_options"] = function(block) {
  var options = Blockly.Constants.Graph.JavaScript.DATA_CODE(block, "OPTS");
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("setOptions", [options, graphid], { NEWLINE: true, });
}

Blockly.JavaScript["graph_manage_destroy"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("destroy", [graphid], { NEWLINE: true });
}

Blockly.JavaScript["graph_manage_get_names"] = function(block) {
  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("getGraphNames");
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_manage_set_view"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  return Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("setCurrent", [graphid], { NEWLINE: true, });
}

Blockly.JavaScript["graph_manage_copy"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var targetname = Blockly.JavaScript.valueToCode(block, "NAME", Blockly.JavaScript.ORDER_NONE) || "undefined";
  if (targetname === "\"\"" || targetname.trim().toLowerCase() === "current") {
    targetname = "undefined";
  }

  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("copy", [graphid, targetname]);
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}

Blockly.JavaScript["graph_manage_rename"] = function(block) {
  var graphid = Blockly.JavaScript.valueToCode(block, "GRAPH", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var name = Blockly.JavaScript.valueToCode(block, "NAME", Blockly.JavaScript.ORDER_NONE) || "undefined";
  var code = Blockly.Constants.Graph.JavaScript.COMPILE_FUNCTION_CALL("rename", [graphid, name]);
  return [code, Blockly.JavaScript.ORDER_FUNCTION_CALL];
}
