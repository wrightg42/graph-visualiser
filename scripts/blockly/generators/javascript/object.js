/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Generating JavaScript for object blocks.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 * @author wrightgeorge42@gmail.com (George Wright)
 */

goog.provide("Blockly.JavaScript.object");

goog.require("Blockly.JavaScript");

/**
 * Creates code from an object definition block.
 * @param block the block to generate code for.
 * @return the object definition as a string.
 **/
Blockly.JavaScript["object_new"] = function(block) {
  if (block.parentBlock_ === null) {
    // object definition not used anywhere will create issues
    return "";
  }

  var properties = "", i = 0, propInp;
  for (var i = 0; i < block.inputList.length; ++i) {
    // get name of the property
    var name = block.inputList[i].name;

    // reject unwanted input fields
    if (name[0] === "K") {
      var key = name.substring(1).replaceAll("'", "\\'");
      var value = Blockly.JavaScript.valueToCode(block, name, Blockly.JavaScript.ORDER_ATOMIC) || undefined;
      if (value === undefined && block.getField(name)) {
        value = JSON.stringify(block.getFieldValue(name));
      }
      properties += "'" + key + "':" + value + ",";
    }
  }
  return ["{" + properties + "}", Blockly.JavaScript.ORDER_ATOMIC];
}

Blockly.JavaScript["object_copy_data"] = function(block) {
  var v = Blockly.JavaScript.variableDB_.getName(block.getFieldValue("VAR"), Blockly.Variables.NAME_TYPE);
  var object = Blockly.JavaScript.valueToCode(block, "OBJECT", Blockly.JavaScript.ORDER_NONE) || "{}";
  return v + " = { ..." + v + ", ..." + object + " };\n";
}

Blockly.JavaScript["object_get"] = function(block) {
  var obj = Blockly.JavaScript.valueToCode(block, "VAR", Blockly.JavaScript.ORDER_NONE) || "{}";
  var field = Blockly.JavaScript.valueToCode(block, "FIELD", Blockly.JavaScript.ORDER_NONE) || "\"\"";
	return [obj + "[" + field + "]", Blockly.JavaScript.ORDER_NONE];
};

Blockly.JavaScript["object_set"] = function(block) {
  var obj = Blockly.JavaScript.valueToCode(block, "VAR", Blockly.JavaScript.ORDER_NONE) || "{}";
  var field =  Blockly.JavaScript.valueToCode(block, "FIELD", Blockly.JavaScript.ORDER_NONE) || "\"\"";
  var value =  Blockly.JavaScript.valueToCode(block, "VALUE", Blockly.JavaScript.ORDER_NONE) || undefined;
  return obj + "[" + field + "] = " + value + ";\n";
};
