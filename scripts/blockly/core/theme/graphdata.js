/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Blockly styling data for new block types.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

// add colours to the classic theme
Blockly.Themes.Classic.categoryStyles["object_category"] = {
  "colour": Blockly.Constants.Object.HUE,
};
Blockly.Themes.Classic.categoryStyles["graph_category"] = {
  "colour": Blockly.Constants.Graph.HUE,
};

Blockly.Themes.Classic.blockStyles["object_blocks"] = {
  "colourPrimary": Blockly.Constants.Object.HUE,
};
Blockly.Themes.Classic.blockStyles["graph_blocks"] = {
  "colourPrimary": Blockly.Constants.Graph.HUE,
};

// add to dark theme
Blockly.Themes.Dark.categoryStyles["object_category"] = Blockly.Themes.Classic.categoryStyles["object_category"];
Blockly.Themes.Dark.categoryStyles["graph_category"] = Blockly.Themes.Classic.categoryStyles["graph_category"];
Blockly.Themes.Dark.blockStyles["object_blocks"] = Blockly.Themes.Classic.blockStyles["object_blocks"];
Blockly.Themes.Dark.blockStyles["graph_blocks"] = Blockly.Themes.Classic.blockStyles["graph_blocks"];

// add to high contrast theme
Blockly.Themes.HighContrast.categoryStyles["object_category"] = Blockly.Themes.Classic.categoryStyles["object_category"];
Blockly.Themes.HighContrast.categoryStyles["graph_category"] = Blockly.Themes.Classic.categoryStyles["graph_category"];
Blockly.Themes.HighContrast.blockStyles["object_blocks"] = Blockly.Themes.Classic.blockStyles["object_blocks"];
Blockly.Themes.HighContrast.blockStyles["graph_blocks"] = Blockly.Themes.Classic.blockStyles["graph_blocks"];

// add to colour blind modes
Blockly.Themes.Deuteranopia.categoryStyles["object_category"] = {
  "colour": 210,
};
Blockly.Themes.Deuteranopia.categoryStyles["graph_category"] = {
  "colour": 302,
};
Blockly.Themes.Deuteranopia.blockStyles["object_blocks"] = {
  "colourPrimary": 210,
};
Blockly.Themes.Deuteranopia.blockStyles["graph_blocks"] = {
  "colourPrimary": 302,
};

Blockly.Themes.Tritanopia.categoryStyles["object_category"] = Blockly.Themes.Deuteranopia.categoryStyles["object_category"];
Blockly.Themes.Tritanopia.categoryStyles["graph_category"] = Blockly.Themes.Deuteranopia.categoryStyles["graph_category"];
Blockly.Themes.Tritanopia.blockStyles["object_blocks"] = Blockly.Themes.Deuteranopia.blockStyles["object_blocks"];
Blockly.Themes.Tritanopia.blockStyles["graph_blocks"] = Blockly.Themes.Deuteranopia.blockStyles["graph_blocks"];

// add start hats to all themes
for (var t in Blockly.Themes) {
  Blockly.Themes[t].setStartHats(true);
}
