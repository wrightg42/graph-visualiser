/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Mocha unit tests for internal blockly graph functionality.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

goog.require("Blockly.Blocks.graph");

suite("Graphs", function() {
  // inverts a type from node to edge
  function invertType(t) {
    return t !== "NODE" ? "NODE" : "EDGE";
  }
  // checks if 2 arrays are equal
  function checkArraysEqual(actual, expected) {
    chai.assert.lengthOf(actual, expected.length);
    for (var i in actual) {
      chai.assert.deepEqual(actual[i], expected[i]);
    }
  }

  setup(function() {
    sharedTestSetup.call(this);
    this.workspace = new Blockly.Workspace();
  });
  teardown(function() {
    sharedTestTeardown.call(this);
  });

  // Tests the extension methods
  suite("Extensions", function() {
    // no shared setup, individual for each test

    test("function tooltip", function() {
      // get initial tooltip
      var add = new Blockly.Block(this.workspace, "graph_data_add");
      var tooltipBefore = add.getTooltip();

      // change data, get new tooltip
      var options = add.getField("TYPE").getOptions(false).map(function(o) { return o[1] });
      var newoption = options[options.indexOf(add.getFieldValue("TYPE")) ? 0 : 1];
      add.setFieldValue(newoption, "TYPE");
      var tooltipAfter = add.getTooltip();

      chai.assert.notEqual(tooltipBefore, tooltipAfter);
    });

    test("graph select dropdown", function() {
      // setup required functions to generate names and ids
      var graphIDs = ["graph_1", "second", "foo_bar_whizz"];
      var graphNames = ["graph 1", "second", "foo bar whizz"];
      window.GRAPH = {
        getGraphIDs: function() { return graphIDs; },
        getGraphNames: function() { return graphNames; }
      };

      // get create block and get dropdown options
      var select = new Blockly.Block(this.workspace, "graph_select");
      var options = select.getField("GRAPH").getOptions();

      // form expected output array
      var expected = [ ["Current", "CURRENT" ] ];
      for (var i in graphIDs) {
        expected.push([ graphNames[i], graphNames[i] ]);
      }

      // check correct values
      checkArraysEqual(options, expected);

      // clean global namespace
      delete window.GRAPH;
    });

    suite("return altering extensions", function() {
      // gets the return type when using a given value in the dropdown
      function getReturn(block, dropdown, dropval) {
        block.setFieldValue(dropval, dropdown);
        if (block.getFieldValue(dropdown) !== dropval) {
          // throw error if we didn't make a change since we supplied an invalid argument
          throw new TypeError("Invalid dropdown value, does not exist in current options.");
        }
        return block.outputConnection.getCheck();
      }

      test("get data", function() {
        // create a get block
        this.getblock = new Blockly.Block(this.workspace, "graph_data_get_single")
        this.getblock.setFieldValue("NODE", "TYPE");

        // get some node return values
        var idReturn = getReturn(this.getblock, "RETURN", "id");
        var weightReturn = getReturn(this.getblock, "RETURN", "value");
        var degreeReturn = getReturn(this.getblock, "RETURN", "degree");

        // get some edge data
        this.getblock.setFieldValue("EDGE", "TYPE");
        var endpointReturn = getReturn(this.getblock, "RETURN", "endpoints");
        var toReturn = getReturn(this.getblock, "RETURN", "to");
        var fullReturn = getReturn(this.getblock, "RETURN", "FULL");

        // evaluate this all worked ok
        chai.assert.equal(idReturn, "String");
        chai.assert.equal(weightReturn, "Number");
        chai.assert.isArray(degreeReturn);
        chai.assert.deepEqual(degreeReturn, [ "Number", "Array" ]);
        chai.assert.equal(endpointReturn, "Array");
        chai.assert.equal(toReturn, "String");
        chai.assert.equal(fullReturn, Blockly.Constants.Object.OUTPUT);

        // try get invalid data (i.e. shape data now we are an edge)
        var errorFn = function() { getReturn(this.getblock, "RETURN", "shape"); };
        chai.assert.throws(errorFn);
      });

      test("get many filter", function() {
        // create a get many block
        this.getblock = new Blockly.Block(this.workspace, "graph_data_get_multi")
        this.getblock.setFieldValue("NODE", "TYPE");

        // get different output values
        var sortReturn = getReturn(this.getblock, "FILTER", "SORTASC");
        var minReturn = getReturn(this.getblock, "FILTER", "MIN");
        var undefReturn = getReturn(this.getblock, "FILTER", "UNDEFINED");

        // eval returns
        chai.assert.equal(sortReturn, "Array");
        chai.assert.equal(minReturn, "String");
        chai.assert.equal(undefReturn, "Array");
      });

      test("shortest path", function() {
        // create a shortest path block
        this.getblock = new Blockly.Block(this.workspace, "graph_query_get_shortest_path_to")

        // set different return values
        var pathReturn = getReturn(this.getblock, "RETURN", "PATH");
        var costReturn = getReturn(this.getblock, "RETURN", "COST");

        // eval returns
        chai.assert.equal(pathReturn, "Array");
        chai.assert.equal(costReturn, "Number");
      });
    });
  });

  // Tests the graph add blocks are correctly forced
  // in type when connecting children
  suite("Forced Types", function() {
    function updateParentBlockType(type, parentblock) {
      parentblock.setFieldValue(type, "TYPE");
    }
    function updateDataBlockType(type, datablock) {
      datablock.updateType(type);
    }

    function connectDataTo(parentblock, input, datablock) {
      parentblock.getInput(input).connection.connect(datablock.outputConnection);
    }
    function disconnectData(datablock) {
      datablock.outputConnection.disconnect();
    }

    function getTypes(parentblock, datablock) {
      return [ parentblock.getFieldValue("TYPE"), datablock.data_.type ];
    }

    setup(function() {
      // create a data block
      this.datablock = new Blockly.Block(this.workspace, "graph_data");

      // create a block that will be linked to the data
      this.addblock = new Blockly.Block(this.workspace, "graph_data_add");

      // use update block to check it forced the update data to the correct type
      this.updateblock = new Blockly.Block(this.workspace, "graph_data_update");

      // add in event handler required for this to work
      this.workspace.addChangeListener(Blockly.Constants.Graph.DATA.Events.blockConnectListener);
    });

    test("data updates parent dropdown", function() {
      // case 1: connect same type, update parent type (no change)
      updateDataBlockType(getTypes(this.addblock, this.datablock)[0], this.datablock);
      connectDataTo(this.addblock, "DATA", this.datablock);
      var sametypes = getTypes(this.addblock, this.datablock);

      // case 2: connect different type, should update parent type
      disconnectData(this.datablock);
      var targettype = invertType(this.addblock.getFieldValue("TYPE"));
      updateDataBlockType(targettype, this.datablock);
      connectDataTo(this.addblock, "DATA", this.datablock);
      var difftypes = getTypes(this.addblock, this.datablock);

      // case 3: connected, change the data type, should update parent type
      var targettype = invertType(targettype);
      updateDataBlockType(targettype, this.datablock);
      var changetypes = getTypes(this.addblock, this.datablock);

      // case 4: connected, change to same data type, should update parent type (no change)
      updateDataBlockType(targettype, this.datablock);
      var changetosametypes = getTypes(this.addblock, this.datablock);

      // assert same values
      chai.assert.equal(sametypes[0], sametypes[1]);
      chai.assert.equal(difftypes[0], difftypes[1]);
      chai.assert.equal(changetypes[0], changetypes[1]);
      chai.assert.equal(changetosametypes[0], changetosametypes[1]);
    });

    test("parent updates data type", function() {
      // connect data block
      connectDataTo(this.addblock, "DATA", this.datablock);
      var inittypes = getTypes(this.addblock, this.datablock);

      // case 1: update to same type, should update data block type (no change)
      var targettype = getTypes(this.addblock, this.datablock)[0];
      updateParentBlockType(targettype, this.addblock);
      var sametypes = getTypes(this.addblock, this.datablock);

      // case 2: update to different type, should update data block type
      var targettype = invertType(targettype);
      updateParentBlockType(targettype, this.addblock);
      var changedtypes = getTypes(this.addblock, this.datablock);

      // assert same values
      chai.assert.equal(inittypes[1], inittypes[0]);
      chai.assert.equal(sametypes[1], sametypes[0]);
      chai.assert.equal(changedtypes[1], changedtypes[0]);
    });

    test("parent force update data type", function() {
      // connect a data block to the update field, this.updateblock should update value
      var infoblock = new Blockly.Block(this.workspace, "graph_data");
      updateDataBlockType(invertType(this.updateblock.getField("TYPE")), infoblock);
      connectDataTo(this.updateblock, "DATA", infoblock);
      var originaltypes = getTypes(this.updateblock, infoblock);

      // case 1: add wrong type to update block, should update data block type
      var targettype = invertType(originaltypes[0]);
      updateDataBlockType(targettype, this.datablock);
      connectDataTo(this.updateblock, "UPDATE", this.datablock);
      var wrongtype = getTypes(this.updateblock, this.datablock);

      // case 2: update the update block's type, should force a change in data block type
      updateParentBlockType(targettype, this.updateblock);
      var parentupdate = getTypes(this.updateblock, this.datablock);

      // case 3: update the searching block type, should update parent type, then data block type
      updateDataBlockType(originaltypes[0], infoblock);
      var searchupdate = getTypes(this.updateblock, this.datablock);

      // assert same values
      chai.assert.equal(originaltypes[1], originaltypes[0]);
      chai.assert.equal(wrongtype[1], wrongtype[0]);
      chai.assert.equal(parentupdate[1], parentupdate[0]);
      chai.assert.equal(searchupdate[1], searchupdate[0]);
    });

    test("add block forces edge data", function() {
      // make sure no properties to begin with
      this.datablock.data_.properties = this.datablock.processProperties([]);
      this.datablock.updateShape_();

      // case 1: connect edge to parent, should force in data
      updateDataBlockType("EDGE", this.datablock);
      connectDataTo(this.addblock, "DATA", this.datablock);
      var connectProperties = this.datablock.data_.properties;

      // case 2: check not forced data on node
      disconnectData(this.datablock);
      updateDataBlockType("NODE", this.datablock);
      connectDataTo(this.addblock, "DATA", this.datablock);
      var nodeproperties = this.datablock.data_.properties;

      // case 3: connect node to parent, change parent to edge, should force in data
      updateParentBlockType("EDGE", this.addblock);
      var changeProperties = this.datablock.data_.properties;

      // case 4: not forced on an update block, should not force in any data
      disconnectData(this.datablock);
      this.datablock.data_.properties = this.datablock.processProperties([]);
      this.datablock.updateShape_();
      connectDataTo(this.updateblock, "UPDATE", this.datablock);
      var updateblockProperties = this.datablock.data_.properties;

      // assert all sets of properties have "to" and "from" data
      chai.assert.isTrue(connectProperties.includes("from"));
      chai.assert.isTrue(connectProperties.includes("to"));
      chai.assert.isEmpty(nodeproperties);
      chai.assert.isTrue(changeProperties.includes("from"));
      chai.assert.isTrue(changeProperties.includes("to"));
      chai.assert.isFalse(updateblockProperties.includes("from"));
      chai.assert.isFalse(updateblockProperties.includes("to"));
    });
  });

  // Tests the mutator works correctly
  suite("Mutator", function() {
    // delays the return of functions, allows for events to be handled without issues
    function waitForEvents() {
      Blockly.Events.fire(new Blockly.Events.Selected());
      while (Blockly.Events.FIRE_QUEUE_.length) { }
    }

    // functions to interact with the data block
    function dataProperties(datablock, properties) {
      if (properties) {
        datablock.data_.properties = datablock.processProperties(properties);
        waitForEvents
        return datablock.data_.properties;
      }
      return datablock.data_.properties;
    }
    function dataType(datablock, type) {
      if (type) {
        datablock.data_.type = type;
        datablock.processProperties();
        waitForEvents()
        return datablock.data_.type;
      }
      return datablock.data_.type;
    }

    // functions to interact with mutator. if parameter undefined will get the values
    function mutatorProperties(containerBlock, properties) {
      if (properties) {
        // create xml for properties
        var xml = "";
        properties.reverse();
        for (var i in properties) {
          xml = "<block type=\"graph_data_" + properties[i] + "\">\n"
                 + (xml !== "" ? "<next>\n" + xml + "\n</next>\n" : "")
                 + "</block>";
        }

        var pblock = null;
        if (xml !== "") {
          pblock = Blockly.Xml.domToBlock(Blockly.Xml.textToDom(xml), containerBlock.workspace);
        }

        // clear stack
        var stack = containerBlock.getInput("STACK").connection.targetBlock();
        while (stack) {
          var next = stack.getNextBlock();
          stack.unplug(true);
          stack.dispose();
          stack = next;
        }

        // set this block(s) as stack
        if (pblock) {
          containerBlock.getInput("STACK").connection.connect(pblock.previousConnection);
        }
        waitForEvents();
        return mutatorProperties(containerBlock);
      } else {
        // get all properties in stack
        var data = [];
        var stack = containerBlock.getInput("STACK").connection.targetBlock();
        while (stack) {
          data.push(stack.type.replace("graph_data_", ""));
          stack = stack.getNextBlock();
        }
        return data;
      }
    }
    function mutatorType(containerBlock, type) {
      var typeField = containerBlock.getField("TYPE");
      if (type) {
        typeField.setValue(type);
        waitForEvents();
        return mutatorType(containerBlock);
      } else {
        return typeField.getValue();
      }
    }

    setup(function() {
      // create a data block
      this.datablock = new Blockly.Block(this.workspace, "graph_data");
      this.datablock.data_.type = "NODE";
      this.datablock.data_.properties = this.datablock.processProperties([]);
      this.mutatorWS = new Blockly.Workspace(
          new Blockly.Options({
            parentWorkspace: this.workspace
          }));

      // add event listeners
      this.mutatorWS.addChangeListener(Blockly.Constants.Graph.DATA.Events.mutatorChangeListener_);

      // functions to open/close mutator with event handlers
      this.openMutator = function() {
        this.mutatorWS.mutatorOrigin = this.datablock;
        this.container = this.datablock.decompose(this.mutatorWS);
        this.datablock.saveConnections(this.container);
        this.datablock.mutator = { workspace_: this.mutatorWS };
        waitForEvents();
      }
      this.closeMutator = function(close) {
        if (close === undefined) {
          close = true;
        }

        this.datablock.compose(this.container);

        if (close) {
          this.datablock.mutator = undefined;
          this.container.dispose();
          delete this.container;
        }

        waitForEvents();
      }
    });

    suite("Composition", function() {
      test("properties", function() {
        // try add some properties
        var some = dataProperties(this.datablock, ["label", "weight"]);
        this.openMutator();
        var somecheck = mutatorProperties(this.container);
        this.closeMutator();

        // try no properties
        var empty = dataProperties(this.datablock, []);
        this.openMutator();
        var emptycheck = mutatorProperties(this.container);
        this.closeMutator();

        // try use all
        var all = dataProperties(this.datablock, ["label", "colour", "weight", "shape", "to", "from"]);
        this.openMutator();
        var allcheck = mutatorProperties(this.container);
        this.closeMutator();

        // assert values
        checkArraysEqual(some, somecheck);
        checkArraysEqual(empty, emptycheck);
        checkArraysEqual(all, allcheck);
      });

      test("type", function() {
        // check type same as block
        var original = dataType(this.datablock);
        this.openMutator();
        var originalcheck = mutatorType(this.container);
        this.closeMutator();

        // check it updates
        var inverted = dataType(this.datablock, invertType(original));
        this.openMutator();
        var invertedcheck = mutatorType(this.container);
        this.closeMutator();

        // assert consistant
        chai.assert.equal(originalcheck, original);
        chai.assert.equal(inverted, invertedcheck);
      });
    });

    suite("Decomposition", function() {
      test("properties", function() {
        // try add some properties
        this.openMutator();
        var some = mutatorProperties(this.container, ["label", "weight"]);
        this.closeMutator();
        var somecheck = dataProperties(this.datablock);

        // try no properties
        this.openMutator();
        var emptycheck = mutatorProperties(this.container, []);
        this.closeMutator();
        var empty = dataProperties(this.datablock);

        // try use all (some will be rejected, will be a long test as takes a
        // while to rejcet invalid blocks. Usually not allowed in mutator)
        this.openMutator();
        var all = mutatorProperties(this.container, ["label", "colour", "weight", "shape", "to", "from"]);
        this.closeMutator();
        var allcheck = dataProperties(this.datablock);

        // assert values
        checkArraysEqual(some, somecheck);
        checkArraysEqual(empty, emptycheck);
        checkArraysEqual(all, allcheck);
      });

      test("type", function() {
        // check type same as block
        this.openMutator();
        var original = mutatorType(this.container);
        this.closeMutator();
        var originalcheck = dataType(this.datablock);

        // check it updates
        this.openMutator();
        var inverted = mutatorType(this.container, invertType(original));
        this.closeMutator();
        var invertedcheck = dataType(this.datablock);

        // assert consistant
        chai.assert.equal(originalcheck, original);
        chai.assert.equal(inverted, invertedcheck);
      });
    });

    suite("Open", function() {
      setup(function() {
        this.openMutator();
      });

      test("unconnected blocks", function() {
        // set to no properties
        var props = mutatorProperties(this.container, []);
        var label = new Blockly.Block(this.mutatorWS, "graph_data_label");

        // check still no properties
        var propscheck = mutatorProperties(this.container);

        // verify results
        checkArraysEqual(props, propscheck);
      });

      test("add block - update parent type", function() {
        // add event handler
        this.workspace.addChangeListener(Blockly.Constants.Graph.DATA.Events.blockConnectListener);

        // create add block with wrong type set
        var addblock = new Blockly.Block(this.workspace, "graph_data_add");
        addblock.setFieldValue(invertType(dataType(this.datablock)), "TYPE");

        // connect, check it updated the parent
        addblock.getInput("DATA").connection.connect(this.datablock.outputConnection);
        var conntype = this.container.getFieldValue("TYPE");
        var conncheck = addblock.getFieldValue("TYPE");

        // update the dropdown in container, make sure it updates parent
        var sametype = conntype;
        this.container.setFieldValue(sametype, "TYPE");
        this.closeMutator(false); // compose, default mutator event handler ususally does this
        var samecheck = addblock.getFieldValue("TYPE");

        var difftype = invertType(sametype);
        this.container.setFieldValue(difftype, "TYPE");
        this.closeMutator(false); // compose, default mutator event handler ususally does this
        var diffcheck = addblock.getFieldValue("TYPE");

        // verify results
        chai.assert.equal(conncheck, conntype);
        chai.assert.equal(samecheck, sametype);
        chai.assert.equal(diffcheck, difftype);
      });

      test("update block - keep parent type", function() {
        this.workspace.addChangeListener(Blockly.Constants.Graph.DATA.Events.blockConnectListener);

        // create add block with different type set
        var updateblock = new Blockly.Block(this.workspace, "graph_data_update");
        var conntype = invertType(dataType(this.datablock));
        updateblock.setFieldValue(conntype, "TYPE");

        // connect, check datablock updated
        updateblock.getInput("UPDATE").connection.connect(this.datablock.outputConnection);
        this.closeMutator(false); // compose, default mutator event handler ususally does this
        var conncheck = this.container.getFieldValue("TYPE");

        // update the dropdown in container, make sure it didn't actually update
        this.container.setFieldValue(conntype, "TYPE");
        this.closeMutator(false); // compose, default mutator event handler ususally does this
        var samecheck = this.container.getFieldValue("TYPE");

        this.container.setFieldValue(invertType(conntype), "TYPE");
        this.closeMutator(false); // compose, default mutator event handler ususally does this
        var diffcheck = this.container.getFieldValue("TYPE");

        // verify results
        chai.assert.equal(conncheck, conntype);
        chai.assert.equal(samecheck, conntype);
        chai.assert.equal(diffcheck, conntype);
      });

      test("forced blocks", function() {
        // add event handler
        this.workspace.addChangeListener(Blockly.Constants.Graph.DATA.Events.blockConnectListener);

        // create add block with edge type
        var addblock = new Blockly.Block(this.workspace, "graph_data_add");
        this.container.setFieldValue("EDGE", "TYPE");
        this.closeMutator(false); // compose, default mutator event handler ususally does this

        // connect, check it updated with forced fields
        addblock.getInput("DATA").connection.connect(this.datablock.outputConnection);
        Blockly.Constants.Graph.DATA.Events.updateForcedBlocks_(this.mutatorWS);
        var properties = mutatorProperties(this.container);

        checkArraysEqual(properties, ["from", "to"]);
      });
    });

    suite("Serialization", function() {
      // creates xml from a type and list of properties
      function createXml(type, properties) {
        // create a string for the properties
        var propstr = "";
        if (properties) {
          for (var i in properties) {
            if (i != 0) {
              propstr += ",";
            } else {
            }
            propstr += "\\'" + properties[i] + "\\'";
          }
          propstr = "[" + propstr + "]";
        }

        // return xml string
        return "<block type=\"graph_data\">\n"
        + "<mutation data=\"{\\'type\\':\\'" + type + "\\'"
        + (properties ? ",\\'properties\\':" + propstr : "")
        + "}\"></mutation>\n"
        + "</block>";
      }

      // creates a block from xml
      function createBlock(xml, workspace) {
        return Blockly.Xml.domToBlock(Blockly.Xml.textToDom(xml), workspace)
      }

      // creates xml from a block
      function toXml(block) {
        return Blockly.Xml.blockToDom(block, true);
      }

      // verifies the type of the mutation
      function verifyType(dom, expected) {
        var mutation = dom.firstChild;
        var json = JSON.parse(dom.firstChild.getAttribute("data").replaceAll("\\'", "\""));
        chai.assert.equal(json.type, expected);
      }

      // verifies the properties in a mutator
      function verifyProperties(dom, expected) {
        var mutation = dom.firstChild;
        var json = JSON.parse(dom.firstChild.getAttribute("data").replaceAll("\\'", "\""));

        chai.assert.lengthOf(json.properties, expected.length);
        for (var i in expected) {
          chai.assert.equal(json.properties[i], expected[i]);
        }
      }

      test("minimal", function() {
        // serialize xml to mutator block
        var xml = "<block type=\"graph_data\" />";
        var block = createBlock(xml, this.workspace);

        // serialize back to xml and verify data
        var dom = toXml(block);
        verifyType(dom, "NODE");
        verifyProperties(dom, []);
      });

      test("no properties", function() {
        // serialize xml to mutator block
        var xml = createXml("EDGE", []);
        var block = createBlock(xml, this.workspace);

        // serialize back to xml and verify data
        var dom = toXml(block);
        verifyType(dom, "EDGE");
        verifyProperties(dom, []);

        // serialize xml to mutator block
        xml = createXml("NODE");
        var block = createBlock(xml, this.workspace);

        // serialize back to xml and verify data
        var dom = toXml(block);
        verifyType(dom, "NODE");
        verifyProperties(dom, []);
      });

      test("properties", function() {
        // serialize xml to mutator block
        var xml = createXml("NODE", ["label", "shape", "colour"]);
        var block = createBlock(xml, this.workspace);

        // serialize back to xml and verify data
        var dom = toXml(block);
        verifyType(dom, "NODE");
        verifyProperties(dom, ["label", "shape", "colour"]);
      });

      test("illegal properties", function() {
        // serialize xml to mutator block
        var xml = createXml("NODE", ["weight", "to", "from"]);
        var block = createBlock(xml, this.workspace);

        // serialize back to xml and verify data
        var dom = toXml(block);
        verifyType(dom, "NODE");
        verifyProperties(dom, ["weight"]);
      })
    });
  });
});
