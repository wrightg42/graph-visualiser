/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Additional script to add to generator testing page in order to test graphs.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

// add our JS scripts
document.write(`
  <!-- add our blocks and generators -->
  <script src="../../blocks/helpers.js"></script>
  <script src="../../blocks/object.js"></script>
  <script src="../../blocks/z_graph.js"></script>
  <script src="../../generators/javascript/helpers.js"></script>
  <script src="../../generators/javascript/object.js"></script>
  <script src="../../generators/javascript/z_graph.js"></script>
  <script src="../../core/theme/graphdata.js"></script>

  <!-- add our scripts to compile the code -->
  <script type="text/javascript" src="https://unpkg.com/vis-network/standalone/umd/vis-network.min.js"></script>
  <script src="../../../../interpreter/acorn_interpreter.js"></script>
  <script src="../../../../../scripts/components/graph.js"></script>
  <script src="../../../../../scripts/components/interpreter.js"></script>
`);

// add a check box to load the graphs xml
document.getElementById("checkboxes").innerHTML += "<input type=\"checkbox\" class=\"suite_checkbox\" value=\"graphs.xml\">Graphs</input><br/>";

// replace toolbox to be the toolbox used in main application
var xhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
xhttp.onload = function() {
  // get the unit test data from current toolbox
  var originalToolbox = document.getElementById("toolbox");
  var unittest = "";
  for (var i in originalToolbox.childNodes) {
    try {
      if (originalToolbox.childNodes[i].getAttribute("name") === "Unit test") {
        unittest = originalToolbox.childNodes[i];
        break;
      }
    } catch (e) { }
  }

  // get the contents of the toolbox we loaded
  var toolbox = xhttp.responseXML.documentElement;

  // add the unit test and new seperator to the toolbox
  toolbox.innerHTML = "\n<sep gap=\"8\"></sep>\n" + toolbox.innerHTML;
  toolbox.insertBefore(unittest, toolbox.childNodes[0]);

  // repalce the toolbox contents
  while (originalToolbox.childNodes.length) {
    originalToolbox.firstChild.remove();
  }
  while (toolbox.childNodes.length) {
    originalToolbox.appendChild(toolbox.firstChild);
    toolbox.firstChild.remove();
  }

  // replace toolbox in blockly
  try {
    Blockly.getMainWorkspace().updateToolbox(document.getElementById("toolbox"));
  } catch (e) {
    // error as main workspace hasn't been loaded, toolbox will load by default, no problem
  }
}
xhttp.open("GET", "./toolbox/toolbox.xml", true);
xhttp.responseType = "document";
xhttp.send();

// add our function to run code in browser
function runJavascriptCode() {
  // get code
  toJavaScript();

  // IO through winodw
  var code = outputCode.replaceAll("window", "_window");

  // use Array.isArray not instanceof Array
  var code = code.replace("a instanceof Array", "Array.isArray(a)");
  var code = code.replace("b instanceof Array", "Array.isArray(b)");

  // pass code to interpreter
  var interpreter = INTERPRETER.createInterpreter(code);

  // run the test code via interpreter
  try {
    interpreter.run();
  } catch (e) {
    console.log("ERROR EXECUTING TEST:");
    console.log(e);
  }
}

// add a button to run our tests
var jsinterpnode = document.getElementsByTagName("a")[1];
var runnode = document.createElement("u")
runnode.innerHTML = "<a style=\"margin-left:5px;\" target=\"_blank\" onclick=\"runJavascriptCode()\">run</a>";
jsinterpnode.parentNode.insertBefore(runnode, jsinterpnode.nextSibling);
