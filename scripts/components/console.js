/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions that control the embedded console
 * in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 * @author wrightg42@gmail.com (George Wright)
 **/

// Console namespace
var CONSOLE = (function() {
  // history of commands from the user to allow scrolling like a terminal
  var history = [ "" ];
  var historyIndex = 0;

  // data store for getting data
  var dGet = undefined;

  /**
   * Applies event listeners checking for key presses to console
   **/
  function listenConsole() {
    $("#console-tab", POP.getDocument("console")).on("click", function(e) {
      $("#user-input", POP.getDocument("console")).focus();
    });

    $("#user-input", POP.getDocument("console")).on("keydown", function(e) {
      processKey(e);
    });
  }

  /**
   * Focuses the console and scrolls to the bottom of input for the user.
   **/
  function focusConsole() {
    var consolebody = $("#console-tab", POP.getDocument("console"));
    consolebody.scrollTop(consolebody.prop("scrollHeight"), 500);

    $("#user-input", POP.getDocument("console")).focus();
  }

  /**
   * Checks what key has been pressed while the console is selected.
   * @param event the keydown event
   **/
  function processKey(event) {
    var input = $("#user-input", POP.getDocument("console"));
    if (event.which == 13) {
      // enter, submit result
      var inp = input.text();
      execute(inp);

      // add to history clear input
      if (inp !== history[0] && inp !== "") {
        history.unshift(inp);
      }
      historyIndex = -1;
      $("#user-input", POP.getDocument("console")).html("");
    }	else if (event.which == 38 || event.which == 40) {
      // arrow, load adjacent statemenet
      if (event.which == 40) {
        --historyIndex;
      } else {
        ++historyIndex;
      }

      // adjusted historyIndex / history
      if (historyIndex > history.length) {
        historyIndex = history.length;
      } else if (historyIndex < 0) {
        historyIndex = -1;
      }

      input.text(historyIndex >= 0 ? history[historyIndex] : "");
    } else if (event.which == 27) {
      // escape, stop running of code
      POP.pause();
    }
  }

  /**
   * Prints the given text into the console.
   * @param text the text/object to print to console. Will stringify JSON objects.
   * @param cssclass the css class to give this printed message. Will default to computer-text (white).
   * NOTE: you can supply arrays to give different parts of text different classes. Both must be arrays to function correctly.
   **/
  function print(text, cssclass) {
    // convert inputs to array to be iterated over
    if (!Array.isArray(cssclass)) {
      text = [ text ]
      cssclass = [ cssclass ];
    }

    var html = "";
    for (var i in text) {
      var str = text[i];
      if (typeof str === "object") {
        // if it is an object, we need to take the internal data stored and stringify for clean output
        str = JSON.stringify(str);
      }
      if (!cssclass[i]) {
        cssclass[i] = "computer-text";
      }

      // add this part to the printout
      html += `<span class="${ cssclass[i] }">${ str }</span> `;
    }

    // add to console history, then scroll to bottom
    $("#answer-box", POP.getDocument("console")).before(`<p class="console-text">${ html }</p>`);
    focusConsole();
  }

  /**
   * Prints a help message on how to use the console.
   **/
  function help() {
    print(`
      THE FOLLOWING COMMANDS CAN BE USED TO CONTROL THE PROGRAM EXECUTION:
      <br />
      runCode() - Will begin a run of the currently constructed blocks.
      <br />
      stepCode() - Will make the next step through the blocks.
      <br />
      help - Will display this message again.
      <br />
      <br />
      Other than this, the console is mainly used for input and output of data from the program constructed out of blocks, although the console will respond to most javascript commands
      <br />
      <br />
      Created by Luke Roberts, graph support added by George Wright.
    `);
  }

  /**
   * Considers the command given by the user
   * and performs the appropriate command.
   * @param input the input from the user
   **/
  function execute(input) {
    // print input
    if (dGet && dGet.message) {
      print([dGet.message, input], ["", "user-text"]);
    } else {
      print(`AV> ${ input }`, "user-text");
    }

    // check if we have a get request to fulfil
    if (dGet !== undefined) {
      // try searialize data
      var val = input;
      if (dGet.type === "number") {
        val = Number(val);
        if (isNaN(val)) {
          // invalid input, ask for it again
          print("Number required!", "error-text");
          focusConsole();
          return;
        }
      }

      dGet.callbacks.resolve(val);
      $("#answer-box", POP.getDocument("console")).html(`AV> ${ $("#user-input", POP.getDocument("console"))[0].outerHTML }`);
      $("#answer-box", POP.getDocument("console")).addClass("user-text");
      listenConsole();
      dGet = undefined;
    } else {
      input = input.trim(); // strip whitespace
    	if (input === "help") {
    		 help();
    	} else if (input === "runCode()") {
        POP.run();
      }	else if (input === "stepCode()") {
        POP.step();
      } else {
    		try {
    			var result = eval(input);
    			if (result != undefined) {
            print(result);
    			}
    		} catch (err) {
    			print("ERROR", "error-text");
    		}
    	}
    }
  }

  /**
   * Used by the prompt command to signal that user input is needed.
   * @param message the message printed in the console
   * @param type what type of variable is required. Etiher "string" or "number". Defaults to string.
   **/
  async function get(message, type) {
    if (dGet !== undefined) {
      throw "Cannot get two variables at once!";
    }

    // print prompt if there was one
    if (message) {
      $("#answer-box", POP.getDocument("console")).html(`${ message } ${ $("#user-input", POP.getDocument("console"))[0].outerHTML }`);
      $("#answer-box", POP.getDocument("console")).removeClass("user-text");
      listenConsole();
    }
    POP.focusConsole();

    // return the promise
    return new Promise((resolve, reject) => {
      // store data as required
      dGet = {
        message: message,
        type: type === undefined ? "string" : type.toLowerCase(),
        callbacks: { resolve: resolve, reject: reject },
      }
    });
  }

  /**
   * Cancels a request to get data from the console.
   **/
  function cancelGet() {
    if (dGet !== undefined) {
      dGet.callbacks.reject("cancelled");
      dGet = undefined;
    }
  }

  /**
   * Event listener that prints some default text to the
   * console and focuses the window to italics
   **/
  $(document).ready(() => {
    print(`
      ALGORITHM VISUALIZER VERSION 2.0
      <br />
      MODIFICATION PROTECTED BY THE APACHE 2 LICENCE <a>http://www.apache.org/licenses/LICENSE-2.0</a>
      <br/>
      <br/>
    `);
    help();
    listenConsole();
    focusConsole();
  });

  return {
    print: print,
    get: get,
    cancelGet: cancelGet,
    listenConsole: listenConsole,
    focusConsole: focusConsole,
  };
})();
