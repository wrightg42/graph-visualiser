/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

 /**
 * @fileoverview Functions that control Input and Output for the page.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

 // IO namespace
var IO = (function() {
  /**
   * Starts a client download of some data.
   * @param data The data to save in the file
   * @param filename The name of the file to download as. If left undefined will copy to clipboard.
   * @param type The content type of this save file
   **/
  function save(data, filename, type) {
    // create blob to save
    var file = new Blob([data], { type: type });

    if (filename !== undefined) {
      var temp = document.createElement("a");

      // link and download
      temp.href = URL.createObjectURL(file);
      temp.download = filename;
      temp.click();

      // clear download
      URL.revokeObjectURL(file);
    } else {
      var temp = [ new ClipboardItem({ [ file.type ]: file }) ];
      navigator.clipboard.write(temp);
    }
  }

  /**
   * Load a text file and implement it in the workspace.
   * @param inputID the ID of the file input to get data from
   * @return The text loaded from input field
   **/
  async function load(inputID) {
    return new Promise((resolve) => {
      var source = document.getElementById(inputID); // source for file

      // read file and resolve once loaded
      var reader = new FileReader();
      reader.addEventListener("load", function() {
        resolve(reader.result);
      });

      // read and return
      reader.readAsText(source.files[0]);
    });
  }

  return {
    save: save,
    load: load,
  };
})();
