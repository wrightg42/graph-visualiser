/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2020 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview script to load blockly in either compressed or
 * uncompressed mode from a url parameter
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

/**
 * Gets a parameter value from the page querystring.
 * https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
 * @param name the name of the parameter to get a value for.
 * @param url the url string to get the parameter from. Defaults to current page URL.
 * @param return the value of the paremeter from the URL's query string.
 **/
function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
  var results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

// load different scripts if compressed mode or not
const compressed = getParameterByName("compressed") === "false" ? false : true;
const tests = false;
const PAGEURL = (function() {
  var pageurl = window.location.href.split("?")[0];
  var lastslash = pageurl.lastIndexOf("/");
  return pageurl.substring(0, lastslash != -1 ? lastslash : pageurl.length);
})();
console.debug(`host: ${ PAGEURL }, compressed: ${compressed}`);

// load xml files into correct locations
function loadxml(location, targetdiv) {
  $.ajax({
    type: "GET",
    url: location,
    dataType: "text",
    isLocal: true,
    success: function(data) {
      $(targetdiv).append($(data));
    },
  });
}
loadxml(`${ PAGEURL }/xml/toolbox.xml`, "#toolbox-div");
loadxml(`${ PAGEURL }/xml/start-blocks.xml`, "#start-blocks-div");

// add correct scripts if in compressed or uncompressed modes
if (compressed) {
  document.getElementsByTagName("head")[0].innerHTML += `
    <!-- page css files -->
    <link rel="stylesheet" href="${ PAGEURL }/compressed/style.css">
  `;

  document.write(`
    <!-- js interpreter -->
    <script src="${ PAGEURL }/libraries/interpreter/acorn_interpreter.js"></script>

    <!-- blockly -->
    <script src="${ PAGEURL }/compressed/blockly/blockly_compressed.js"></script>
    <script src="${ PAGEURL }/compressed/blockly/blocks_compressed.js"></script>
    <script src="${ PAGEURL }/compressed/blockly/javascript_compressed.js"></script>
    <script src="${ PAGEURL }/compressed/blockly/msg/js/en.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/core/theme/graphdata.js"></script>

    <!-- js files -->
    <script src="${ PAGEURL }/compressed/scripts.min.js"></script>
  `);
} else {
  document.getElementsByTagName("head")[0].innerHTML += `
    <!-- page css files -->
    <link rel="stylesheet" href="${ PAGEURL }/css/page.css">
    <link rel="stylesheet" href="${ PAGEURL }/css/blockly.css">
    <link rel="stylesheet" href="${ PAGEURL }/css/console.css">
    <link rel="stylesheet" href="${ PAGEURL }/css/table.css">
    <link rel="stylesheet" href="${ PAGEURL }/css/graph.css">
    <link rel="stylesheet" href="${ PAGEURL }/css/array.css">
  `;

  document.write(`
    <!-- js interpreter -->
    <script type="text/javascript" src="${ PAGEURL }/libraries/interpreter/acorn.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/libraries/interpreter/interpreter.js"></script>

    <!-- blockly -->
    <script src="${ PAGEURL }/libraries/google/blockly/blockly_uncompressed.js"></script>

    <script src="${ PAGEURL }/libraries/google/blockly/blocks/logic.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/loops.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/math.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/text.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/lists.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/colour.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/variables.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/blocks/procedures.js"></script>

    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/logic.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/loops.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/math.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/text.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/lists.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/colour.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/variables.js"></script>
    <script src="${ PAGEURL }/libraries/google/blockly/generators/javascript/procedures.js"></script>

    <script src="${ PAGEURL }/libraries/google/blockly/msg/messages.js"></script>

    <!-- created blockly extensions -->
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/blocks/object.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/blocks/graph.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/blocks/helpers.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/core/theme/graphdata.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/generators/javascript/object.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/generators/javascript/graph.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/blockly/generators/javascript/helpers.js"></script>

    <!-- Import local functions and methods -->
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/io.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/console.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/graph.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/table.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/array.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/pop.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/blockly-inject.js"></script>
    <script type="text/javascript" src="${ PAGEURL }/scripts/components/interpreter.js"></script>
    ${ tests ?
    `
      <!-- Import test scripts -->
      <script type="text/javascript" src="${ PAGEURL }/scripts/components/tests/core.js"></script>
      <script type="text/javascript" src="${ PAGEURL }/scripts/components/tests/graph.js"></script>
    `
    : "" }
  `);
}

// set the page splitters up correctly
$(document).ready(() => {
  $("#body-row").jqxSplitter({
    width: "100%",
    height: "100%",
    panels: [ { size: "60%", collapsible: false } ]
  });

  $("#tabs-pane").jqxSplitter({
    width: "100%",
    height: "100%",
    orientation: "horizontal",
    panels: [ { size: "60%", collapsible: false } ]
  });
});
