/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions that control the table visualisation
 * in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

// Table namespace
var TABLE = (function() {
  /**
   * Removes the existing table and re-initilizes italitilizes it.
   * This is called by the parsing api
   * @param vars list of variables to put in the table
   **/
  function reset(vars) {
    // clear table, not popout button however
    var box = $("#table-tab", POP.getDocument("table"));
    while (box.children().length > 1) {
      box.children().last().remove();
    }

    // reinit table and dropdown menu
    initDropdown(vars);
    initTable(vars);
    setHidden($("#hide-all-vars").prop("checked"));
  }

  /**
   * Builds the table on page and sets some initial values.
   * @param vars list of the variables to put in the table
   **/
  function initTable(vars) {
    // get table headers for variables
    var var_headers = "";
    for (var i in vars) {
      var_headers += `<th class="var-col">${ vars[i] }</th>`;
    }

    // construct table header html
    var table = $("#table-tab", POP.getDocument("table"));
    var html = `
      <div id="table-container">
        <table>
          <thead id="table-header">
            <tr>
              <th class="svg-cell blue-cell">Block</th>
              <th class="iteration-cell blue-cell">Step</th>
              ${ var_headers }
            </tr>
          </thead>
          <tbody id="table-body">
          </tbody>
        </table>
      </div>
    `;
    var item = $.parseHTML(html);
    table.append(item);
    setDimensions();
  }

  /**
   * Creates other options in the table drop-down
   * menu that shows the different variables.
   * @param vars the variables to put in the dropdown
   **/
  function initDropdown(vars) {
    // empty dropdown list
    var menu = $("#table-dropdown");
    while (menu.children().length > 3) {
      menu.children().last().remove();
    }

    // add option for each var to the menu
    for (var i in vars) {
      var html = `
        <li class="var-menu">
          ${ vars[i] }
          <input class="var-check" type="checkbox" value="${ vars[i] }" onchange="TABLE.hideColumn(value)"></input>
        </li>
      `;
      var item = $.parseHTML(html);
      menu.append(item);
    }
  }

  /**
   * Modify the table on each step through the code.
   * Allows for a variation of row and cell width based on the initilized table
   * @param vars dictionary of variable values to put in the table
   **/
  function step(vars) {
    // get head and body
    var head = $("#table-header > tr", POP.getDocument("table"));
    var body = $("#table-body", POP.getDocument("table"));

    var cols = head.children().length;
    var rownum = $("#table-body", POP.getDocument("table")).children().length + 1;

    // construct html for the new row
    var html = ""
    for (var i = 0; i < cols; ++i) {
      switch (i) {
        case 0:
          html += `<td class="svg-cell blue-cell"><svg>${ POP.getCurrentSVG() }</svg></td>`
          break;
        case 1:
          html += `<td class="iteration-cell">${ rownum }</td>`;
          break;
        default:
          // variable, get value and set text
          var varname = head.children(`th:nth-child(${ i + 1 })`).text();
          var value = vars[varname];
          value = (value === undefined) ? "" : value;

          // get if this cell should be hidden
          var hidden = head.children(`th:nth-child(${ i + 1 })`).css("display") === "none";

          if (ARRAYS.isComplex(vars[varname])) {
            // complex, update the list view
            var changed = ARRAYS.update(varname, vars[varname], rownum);
            var iter = ARRAYS.countHistory(varname);

            // add link to array tab
            html += `<td class="var-col${ changed ? " blue-cell" : "" } complex-cell" ${ hidden ? "style=\"display: none;\"" : "" } onclick="TABLE.viewComplex('${ varname }', ${ iter })">Check Arrays</td>`;
          } else {
            // normal data, just print it here and check for changes
            var changed = body.children().length != 0 ? body.children().last().children().eq(i).text() !== "" + value : false;
            html += `<td class="var-col${ changed ? " blue-cell" : "" }" ${ hidden ? "style=\"display: none;\"" : "" }>${ value }</td>`;
          }

          break;
      }
    }

    // add row tag and append to table
    html = `<tr id="row-${ rownum }">${ html }</tr>`;
    var item = $.parseHTML(html);
    body.append(item);

    // if any changed, remove the blue-cell class
    setTimeout(function() {
      $("#row-" + rownum, POP.getDocument("table")).children().slice(2).removeClass("blue-cell");
    }, 750);

    // scroll to bottom of trace table
    var tablebody = $("#table-tab", POP.getDocument("table"));
    if (!$("#blockly-div"), POP.getDocument("table").length) {
      tablebody = tablebody.parent();
    }
    tablebody.prop("scrollTop", tablebody.prop("scrollHeight"));

    setDimensions();
  }

  /**
   * Hides a column of the trace table.
   * @param id the variable name of the column to hide.
   **/
  function hideColumn(id) {
    // get variable index
    var varIndex = null;
    $("#table-header > tr > .var-col", POP.getDocument("table")).each(function(index, tr) {
      if (tr.innerHTML === id) {
        varIndex = index;
      }
    });

    // hide columns based on index
    $(`#table-header > tr > *:nth-child(${ varIndex + 3 })`, POP.getDocument("table")).toggle();
    $(`#table-body > tr > *:nth-child(${ varIndex + 3 })`, POP.getDocument("table")).toggle();

    setDimensions();

    // uncheck the hide all button if we unhid a column
    if ($(`.var-check[value="${ id }"]`).prop("checked") === false) {
      $("#hide-all-vars").prop("checked", false);
    }
  }

  /**
   * Adjusts the layout of the table visualisation depending
   * on the size of the window.
   **/
  function setDimensions() {
    // set the width of columns
    var width = $("#table-container", POP.getDocument("table")).width();
    var vars = $("#table-header > tr > .var-col", POP.getDocument("table")).filter(function() {
      return $(this).css("display") !== "none";
    }).length;
    var blockit_width = parseInt($(".svg-cell", POP.getDocument("table")).css("width"))
          + parseInt($(".iteration-cell", POP.getDocument("table")).css("width"));
    var col_width = Math.max((width - blockit_width) / Math.max(1, vars), 50) - 1;
    $(".var-col", POP.getDocument("table")).css({
      "width": col_width, "min-width": col_width, "max-width": col_width
    });

    // change popout colour depending on background
    if (vars == 0) {
      $("#table-popout", POP.getDocument("table")).css("color", );
      $("#table-popout", POP.getDocument("table")).hover(function() {
        $(this).css("color", "");
      }, function() {
        $(this).css("color", "")
      });
    } else {
      $("#table-popout", POP.getDocument("table")).css("color", "rgb(242, 242, 242)");
      $("#table-popout", POP.getDocument("table")).hover(function() {
        $(this).css("color", "rgb(179, 179, 179)");
      }, function() {
        $(this).css("color", "rgb(242, 242, 242)")
      });
    }
  }

  /**
   * Sets all variables to hidden or not hidden in the trace table.
   * @param checked Whether to hide all items.
   **/
  function setHidden(hide) {
    $(".var-check").each((i, e) => {
      if ($(e).prop("checked") !== hide) {
        $(e).prop("checked", hide);
        hideColumn($(e).prop("value"));
      }
    });
  }

  /**
   * Views the array tab and scrolls to a specific variable.
   * @param varname the name of the variable to view in the array tab.
   * @param iter the iteration of the array to view.
   **/
  function viewComplex(varname, iter) {
    // view array tab
    $("#arrays-button", POP.getDocument("blockly")).click();
    var id = GRAPH.stripToID(varname);

    // check if we need to expand the history
    if (iter !== ARRAYS.countHistory(varname)) {
      $(`#history-list-${ id }`, POP.getDocument("arrays")).collapse("show");
    }

    // scroll to the target variable
    setTimeout(() => {
      $("#arrays-bin", POP.getDocument("arrays")).animate({
        scrollTop: $(`#${ id }-update-${ iter }`, POP.getDocument("arrays")).position().top
      }, 500);
    }, 300);
  }

  // Event handler to reset the table size
  $(document).ready(function() {
    new ResizeObserver(setDimensions).observe($("#table-tab")[0]);

    // hide all variables when requested
    $("#hide-all-vars").click(function() {
      setHidden(this.checked);
    })

    // print trace table button
    $("#print-table-button").click(function() {
      print();
    });
  });

  /**
   * Prepares a printable copy of the table in a new window.
   **/
  function print() {
    var printWindow = createWindow("table-tab");
    setTimeout(function() { printWindow.print(); printWindow.close(); }, 10);
  }

  // module exports
  return {
    reset: reset,
    step: step,
    setDimensions: setDimensions,
    hideColumn: hideColumn,
    viewComplex: viewComplex,
  };
})();
