/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions to inject Blockly to the correct div and handle blockly interactions.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

// blockly namespace to interact with blockly page
var BLOCKLY = (function() {
  // Inject Blockly into the correct div and create event handlers
  var workspace = Blockly.inject("blockly-div",
    { toolbox: $("#toolbox")[0], sounds: false, comments: true,
      grid : {
        spacing : 15,
        length : 1,
        colour : '#888',
        snap : true,
      },
      zoom : {
        controls : true,
        wheel : true,
        startScale : 1,
        maxScale : 3,
        minScale : 0.3,
        scaleSpeed : 1.2,
      },
    }
  );
  Blockly.Xml.domToWorkspace($("#start-blocks")[0], workspace);

  /**
   * Closes a mutator window when the UI is used elsewhere. Prevent's cluttered workspace
   * and calls previous functions to handle updates correctly.
   * @param event the blockly event to handle.
   **/
  function mutatorOpenClose(event) {
    if (event.type === Blockly.Events.BUBBLE_OPEN && event.bubbleType === "mutator") {
      // close old mutator if a new one has opened
      if (event.isOpen) {
        if (openMutatorID_ !== event.blockId) {
          closeMutator(openMutatorID_, event.workspaceId);
        }
        openMutatorID_ = event.blockId;

        // see if we need to create an event handler to keep custom mutators updated
        var mutBlock = Blockly.Workspace.getById(event.workspaceId).getBlockById(event.blockId);
        var mutWS = mutBlock.mutator.getWorkspace();
        mutWS.mutatorOrigin = mutBlock; // add reference to block that made the mutator
        if (mutBlock.type === "object_new") {
          mutWS.addChangeListener(Blockly.Constants.Object.NEW.Events.mutatorChangeListener_);
        } else if (mutBlock.type === "graph_data") {
          mutWS.addChangeListener(Blockly.Constants.Graph.DATA.Events.mutatorChangeListener_);
        }
      }
    } else if ([Blockly.Events.CLICK, Blockly.Events.VIEWPORT_CHANGE, Blockly.Events.SELECTED].includes(event.type)) {
      // close old mutator window when user performs an action
      // incluidng a click, viewport change, select another block
      var usefulIds = [event.blockId, event.newElementId, event.oldParentId];
      if (!usefulIds.includes(openMutatorID_)) {
        closeMutator(openMutatorID_, event.workspaceId);
        openMutatorID_ = undefined;
      }
    }
  }
  var openMutatorID_ = undefined; // current open mutator's ID

  /**
   * Makes sure only one main block exists and disables code outside of this.
   * @param event the blockly event to handle.
   **/
  function mainBlockChangeListener(event) {
    // recursive function to enable / disable block and children
    function enableBlock(block, e) {
      // enable block
      block.setEnabled(e);

      // enable inputs for block
      for (var i in block.inputList) {
        var conn = block.inputList[i].connection;
        if (conn && conn.targetConnection && conn.targetConnection.sourceBlock_) {
          enableAllInTree(conn.targetConnection.sourceBlock_, e)
        }
      }
    }

    // update all child blocks based on root block
    function enableAllInTree(block, e) {
      while (block) {
        enableBlock(block, e);
        block = block.getNextBlock();
      }
    }

    // check valid workspace
    var eventWS = Blockly.Workspace.getById(event.workspaceId);
    if (!eventWS.isMutator) {
      if (event.type === Blockly.Events.MOVE) {
        // get workspace and block that was moved
        var block = eventWS.getBlockById(event.blockId);
        if (block) {
          // check the root block is a hatted block
          var root = block.getRootBlock();
          var rootHatted = !root.previousConnection && !root.outputConnection;

          enableAllInTree(block, rootHatted);
        }
      } else if ([Blockly.Events.BLOCK_CREATE, Blockly.Events.BLOCK_DELETE].includes(event.type)) {
        // get block type
        var block = event.xml || event.oldXml;
        if (block.getAttribute("type") === "helpers_main") {
          // block was helpers_main, update toolbox
          var enabled = event.type === Blockly.Events.BLOCK_DELETE;

          // update the toolbox item to disable/enable
          var category = eventWS.toolbox_.getToolboxItems()[0];
          var items = category.getContents();
          items[0] = { kind: "block", "type": "helpers_main", "disabled": !enabled };
          category.updateFlyoutContents(items);
        }

        // disable the created block immediatly if not rooted
        if (event.type === Blockly.Events.BLOCK_CREATE) {
          var block = eventWS.getBlockById(event.ids[0]);
          enableAllInTree(block, !block.previousConnection && !block.outputConnection);
        }
      } else if (event.type === Blockly.Events.BLOCK_CHANGE && event.element ===  "disabled" && event.newValue === false) {
        // we enabled a block, check it is rooted, otherwise disable it
        var block = eventWS.getBlockById(event.blockId);
        if (block) {
          // check the root block is a hatted block
          var root = block.getRootBlock();
          var rootHatted = !root.previousConnection && !root.outputConnection;
          if (!rootHatted) {
            // force the block to be disabled if not hatted
            block.setEnabled(false);
          }
        }
      }
    }
  }

  // apply desired event handlers
  workspace.addChangeListener(Blockly.Constants.Graph.DATA.Events.blockConnectListener);
  workspace.addChangeListener(mutatorOpenClose);
  workspace.addChangeListener(mainBlockChangeListener);

  $(document).ready(function() {
    $(window).resize(function() {
      // get absolute coordinates and dimensions of the area to place blockly
      var element = $("#blockly-pane")[0];
      var x = 0;
      var y = 0;
      do {
        x += element.offsetLeft;
        y += element.offsetTop;
        element = element.offsetParent;
      } while (element);

      // resize the blockly elements
      $("#blockly-div").css("left", `${ x }px`);
      $("#blockly-div").css("top", `${ y }px`);
      $("#blockly-div").css("width", `${ $("#blockly-pane")[0].offsetWidth }px`);
      $("#blockly-div").css("height", `${ $("#blockly-pane")[0].offsetHeight }px`);
      Blockly.svgResize(workspace);
    });
    window.dispatchEvent(new Event("resize"));
  });

  /**
   * Saves the blockly workspace XMl to a file.
   **/
  function save() {
    var xml = Blockly.Xml.workspaceToDom(workspace);
    var xml_text = Blockly.Xml.domToText(xml);
    IO.save(xml_text, "blockly_save.xml", "application/xml");
  }

  /**
   * Load a text file and implement it in the workspace.
   **/
  function load() {
    // clear workspace and repalce with text read
    IO.load("xml-input").then((data) => {
      Blockly.mainWorkspace.clear();
      var xml = Blockly.Xml.textToDom(data);
      Blockly.Xml.domToWorkspace(xml, workspace);
    });
  }

  /**
   * injects code to highlight the blocks.
   * @param id the id of the block.
   **/
  function highlight(id) {
    $("g").removeClass("blocklySelected");
    if (id !== null && id !== undefined) {
      $(`g[data-id="${ id }"]`).addClass("blocklySelected");
    }
  }

  /**
   * Gets the SVG of a given block.
   * @param id the id of the block to be converted to an SVG
   * @return the generated SVG image
   **/
  function getSVG(id) {
    var l = workspace.getBlockById(id);
    thisBlockSvg = l.svgGroup_.outerHTML;
    if (l.getNextBlock() != null) {
      nxtBlockSvg = l.getNextBlock().svgGroup_.outerHTML;
      thisBlockSvg = thisBlockSvg.replace(nxtBlockSvg, "");
    }

    return thisBlockSvg;
  }

  /**
   * Closes a mutator window.
   * @param id the blockid the mutator belongs to.
   * @param wsId the workspace id to close mutator in.
   **/
  function closeMutator(id, wsId) {
    var ws = wsId ? Blockly.Workspace.getById(wsId) : workspace;
    if (id !== undefined) {
      var block = ws.getBlockById(id);
      if (block) {
        block.mutator.setVisible(false);
      }
    }
  }

  /**
   * Get the variables used in the program.
   * @return the list of variable being used.
   **/
  function getVars() {
    Blockly.JavaScript.variableDB_.setVariableMap(workspace.getVariableMap());
    var vars = Blockly.Variables.allUsedVarModels(workspace);
    if (vars) {
      return vars.map(
        // map to the actual variable name
        v => Blockly.JavaScript.variableDB_.getName(v.name, Blockly.VARIABLE_CATEGORY_NAME)
      )
    } else {
      return [];
    }
  }

  /**
   * Converts a JavaScript variable name to the UI version to avoid user confusion.
   * @param jsVarName the JavaScript variable name.
   * @return the original UI varient of the variable name
   **/
  function getUIVarName(jsVarName) {
    Blockly.JavaScript.variableDB_.setVariableMap(workspace.getVariableMap());
    var results = workspace.getVariableMap().variableMap_[""].filter(
      v => Blockly.JavaScript.variableDB_.getName(v.name, Blockly.VARIABLE_CATEGORY_NAME) === jsVarName
    );
    if (results.length) {
      return results[0].name;
    } else {
      return undefined;
    }
  }

  /**
   * Converts a UI variable name to the internal JavaScript one to make correct ID's from.
   * @param uiVarName the UI variable name.
   * @return the corrosponding JavaScript varient of the variable name
   **/
  function getInternalVarName(uiVarName) {
    Blockly.JavaScript.variableDB_.setVariableMap(workspace.getVariableMap());
    return Blockly.JavaScript.variableDB_.db_[uiVarName + "_VARIABLE"];
  }

  /**
   * Gets the javascript of the workspace.
   * @param prefix What to prefix statements with, i.e. highlight
   **/
  function getCode(prefix = "") {
    // set the prefix for blocks
    Blockly.JavaScript.STATEMENT_PREFIX = prefix;
    return Blockly.JavaScript.workspaceToCode(workspace)
  }

  /**
   * Updates all graph selector nodes with correct text values for a given graph rename.
   * @param oldgraphname the old graph name that has been renamed.
   * @param newgraphname the new name of the graph.
   */
  function updateGraphSelector(oldgraphname, newgraphname) {
    // get all graph_select blocks to update the values
    workspace.getAllBlocks().filter(b => b.type === "graph_select").forEach((item, i) => {
      // get dropdown and update values
      var dropdown = item.inputList[0].fieldRow[1];
      dropdown.generatedOptions_ = dropdown.menuGenerator_();

      // check if the renamed graph is currently selected and update the value
      if (dropdown.value_ === oldgraphname) {
        // update selected item and redraw block
        dropdown.selectedOption_ = dropdown.generatedOptions_.filter(o => o[1] === newgraphname)[0];
        dropdown.value_ = dropdown.selectedOption_[1];
        dropdown.forceRerender();
      }
    });
  }

  /**
   * Sets the currently used theme in blockly.
   * @param theme the name of the theme to use.
   **/
  function setTheme(theme) {
    if (Blockly.Themes[theme]) {
      // set theme if it exists
      workspace.setTheme(Blockly.Themes[theme]);

      // set the correct colour arrows
      if (["Dark"].includes(theme)) {
        $(".blocklyTreeIcon").css("background-position-y", "-16px");
      } else {
        $(".blocklyTreeIcon").css("background-position-y", "-1px");
      }
    }
  }

  /**
   * Saves the blockly javascript code for the user.
   **/
  function saveCode() {
    var code = getCode();
    IO.save(code, "blockly_code.js", "application/javascript");
  }

  // click event handlers
  $(document).ready(function() {
    // saving and loading blocks
    $("#save-button").click(function() {
      save();
    });

    $("#load-button").click(function() {
      $("#xml-input").val("");
      $("#xml-input").click();
    });

    $("#xml-input").change(function() {
      var value = $(this).val();
      if (value && value !== "") {
        load();
      }
    });

    // get code and an executable page to interact with
    $("#download-code-button").click(function() {
      saveCode();
    });

    // display mode settings
    var displayModeOpen = false;
    $("#display-mode-dropdown").click(function(e) {
      // the dropdown has been clicked, mark this incase the dropdown tries to hide itself
      displayModeOpen = true;
      setTimeout(() => displayModeOpen = false, 50);
    });

    $("#workspace-dropdown").on("hide.bs.dropdown", function(e) {
      // don't hide the dropdown if we are using the display mode menu
      if (displayModeOpen) {
        e.preventDefault();
      }
    });

    $("#display-mode-dropdown").change(function() {
      setTheme($(this).val());
    });

    // prevent highlighting navbar buttons, always click on any touch to them
    $("#navbar li").on("pointerdown", function(e) {
      if ($(this).parent().prop("tagName") === "ul") {
        e.preventDefault();
        $(this).click();
      }
    });
    $("#navbar li").attr('unselectable', 'on').css('user-select', 'none').on('selectstart dragstart', false);
  });

  return {
    highlight: highlight,
    getSVG: getSVG,
    getVars: getVars,
    getUIVarName: getUIVarName,
    getInternalVarName: getInternalVarName,
    getCode: getCode,
    updateGraphSelector: updateGraphSelector,
  }
})();
