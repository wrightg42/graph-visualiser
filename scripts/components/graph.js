/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions that interact with the graph visualization
 * in the algorithm visualizer. Also some list functions.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

// Graph namespace
var GRAPH = (function() {
  var graphs = { }; // dictionary of graphs for this namespace
  var current = undefined; // store the current graph id

  // range of font sizes allowed in graph
  const FONTSIZERANGE = [8, 24];
  var staticDropdownCount = 3;

  /**
   * A graph options prototype to store the supported graph options.
   * @param directed If the graph is directional.
   * @param weightedEdges If the graph was weighted edges.
   * @param weightedNodes If the graph was weighted edges.
   * @param negativeWeights If the graph has negative weights enabled.
   * @param multigraph If the graph allows parallel edges between nodes.
   * @param degrees If the graph has degree labels.
   * @param physics If the graph has physics simulations enabled.
   * @param fontSize The font size of text in the diagram.
   * @param labelAlign The align of labels on edges in the graph. One of 'horizontal','top','middle','bottom'.
   **/
  function GraphOptions(directed, weightedEdges, weightedNodes, negativeWeights, multigraph,
                        degrees, physics, fontSize, labelAlign) {
    if (!new.target) {
      // return new object if wasn't called with new
      return new GraphOptions(directed, weightedEdges, weightedNodes, negativeWeights, multigraph, degrees, physics, fontSize, labelAlign);
    }

    // functional settings
    this.directed = directed;
    this.weightedEdges = weightedEdges;
    this.weightedNodes = weightedNodes;
    this.negativeWeights = negativeWeights;
    this.multigraph = multigraph;

    // ui settings
    this.degrees = degrees;
    this.physics = physics;
    this.fontSize = fontSize;
    this.labelAlign = ["horizontal", "top", "middle", "bottom"].includes(labelAlign) ? labelAlign : "horizontal";

    // adjust font size if needed
    if (this.fontSize < FONTSIZERANGE[0]) {
      this.fontSize = FONTSIZERANGE[0];
    } else if (this.fontSize > FONTSIZERANGE[1]) {
      this.fontSize = FONTSIZERANGE[1];
    }
  }

  /**
   * A graph prototype. Handles methods to interact with a graph and stores its key data.
   * @param name the graph name.
   * @param ns the nodes to initialize the graph with. Can be left empty.
   * @param es the edges to initialize the graph with. Can be left empty.
   * @param options the nodes to initialize the graph with. Can be left empty to start with default options.
   * @return A graph object.
   **/
  function Graph(name, ns = [], es = [], options = undefined) {
    if (!new.target) {
      // return new object if wasn't called with new
      return new Graph(name, ns, es, options);
    }

    // data stores
    var title = name;
    var id = stripToID(name);
    var nodes = new vis.DataSet([]);
    var edges = new vis.DataSet([]);
    var options = options;
    var network = undefined;
    var edgemode = true; // a boolean value to if the network is in edge mode
    // Actual constructor code is at end of definition since it features
    // exported methods undeclared at this point

    /**
     * Uses private options to generate the options for the network.
     * @return the vis.js network options for the user options set
     **/
    function generateNetworkOptions() {
      return {
        // default data
        autoResize: true,
        height: "100%",
        width: "100%",
        clickToUse: false,
        interaction: {
          hover: true,
          navigationButtons: true,
          dragNodes: false,
        },
        manipulation: {
          enabled: false,
          addEdge: function(edgeData, callback) {
            GRAPH.add(edgeData, false); // add edge to diagram
          },
        },
        layout: {
          randomSeed: Math.random(),
          improvedLayout: true,
        },
        nodes: {
          scaling: { min: 10, max: 10, label: false, },
          font: { size: options.fontSize, }
        },
        edges: {
          scaling: { min: 1, max: 1, label: false, },
          arrows: {
            to: {
              enabled: options.directed, // arrows for directed graphs
            },
          },
          selectionWidth: 2,
          font: { size: options.fontSize, align: options.labelAlign, },
        },
      };
    }

    /**
     * Resets the grapht to edge mode.
     * @param timeout if the graph should do this on a timeout.
     **/
    function resetEdgeMode(timeout = false) {
      if (timeout) {
        setTimeout(() => { resetEdgeMode() }, 25);
      } else {
        try {
          network.addEdgeMode();
          edgemode = true;
        } catch (e) {
          if (e.type === "TypeError" && e.message === "this.manipulation is undefined") {
            // testing page or graph disposed, let's ignore the error
          } else {
            resetEdgeMode(true);
          }
        }
      }
    }
    this.resetEdgeMode = resetEdgeMode;

    /**
     * Subscribes to network events to react accordingly.
     **/
    function subscribeEvents() {
      // function to create a context menu
      function createContextMenu(position, mode) {
        // set contents of context menu
        var content = `
          <li id="context-menu-edit"><a><span class="glyphicon glyphicon-cog"></span> Edit ${ mode }</a></li>
          <li id="context-menu-delete"><a><span class="glyphicon glyphicon glyphicon-trash"></span> Delete ${ mode }</a></li>
          ${
            mode === "Graph" ? `
              <li role="separator" class="divider"></li>
              <li class="context-menu-save" id="context-menu-copy"><a><span class="glyphicon glyphicon glyphicon-copy"></span> Copy Image</a></li>
              <li class="context-menu-save" id="context-menu-save"><a><span class="glyphicon glyphicon-floppy-disk"></span> Save Image</a></li>
            ` : `
              <span style="display: none;" id="context-menu-id">${ mode === "Node" ? network.getNodeAt(position) : network.getEdgeAt(position) }</span>
            `
          }
          <span style="display: none;" id="context-menu-type">${ mode.toLowerCase() }</span>
        `;
        $("#context-menu", POP.getDocument("graph")).html(content);

        // set css of context menu and show
        $("#context-menu", POP.getDocument("graph")).css({
          left: position.x + "px",
          top: position.y + "px"
        });
        $("#context-menu", POP.getDocument("graph")).finish().show(100);
      }

      // drag / edit mode on pointer events
      var updateEdgemode = function(params) {
        // deselect page
        document.getSelection().removeAllRanges();

        // set if edit or move mode
        if (!edgemode && !params.ctrlKey) {
          resetEdgeMode();
          // disable node dragging to be sure this will not drag a node, was a previous bug
          network.setOptions({ interaction: { dragNodes: false } });

          // we just put in edge mode so need to repatch the event
          params.preventDefault();
          setTimeout(() => network.body.container.firstChild.firstChild.dispatchEvent(params), 25);
        } else if (params.ctrlKey) {
          edgemode = false;
          network.setOptions({ interaction: { dragNodes: true } });
          network.disableEditMode();
        }
      }
      network.body.container.firstChild.firstChild.addEventListener("pointerdown", updateEdgemode);

      var cancelSelfedge = function(params) {
        if (network.getNodeAt(params.pointer.DOM) !== undefined) {
          resetEdgeMode(); // prevent clicking making a self connecting edge
        }
      }
      network.on("click", cancelSelfedge);

      // double click, add/edit data. hold CTRL to remove data
      var doubleclick = function(params) {
        var cntl = params.event.srcEvent.ctrlKey; // get if control key pressed down
        if (params.nodes.length != 0) {
          if (cntl) {
            // remove on double click with control
            GRAPH.remove(params.nodes[0]);
          } else {
            // edit label on double click on node
            openSettings(params.nodes[0], true, id);
          }
        } else if (params.edges.length != 0) {
          if (cntl) {
            // remove on double click with control
            GRAPH.remove(params.edges[0], false);
          } else {
            // edit weight on double click on edge
            openSettings(params.edges[0], false, id);
          }
        } else {
          // add node on double click and no selected item
          GRAPH.add({ x: params.pointer.canvas.x, y: params.pointer.canvas.y });
        }

        edgemode = false;
      }
      network.on("doubleClick", doubleclick);

      // right click, generate menues to interact with nodes/edges
      var rightclick = function(params) {
        params.event.preventDefault(); // prevent default context menu

        // get if this is a context menu for a node, edge, or graph
        var mode = ""
        if (network.getNodeAt(params.pointer.DOM) !== undefined) {
          mode = "Node";
        } else if (network.getEdgeAt(params.pointer.DOM) !== undefined) {
          mode = "Edge";
        } else {
          mode = "Graph";
        }
        createContextMenu(params.pointer.DOM, mode);
      }
      network.on("oncontext", rightclick);

      // allow nodes to move when dragging
      var dragstart = function(params) {
        if (!edgemode && params.nodes.length) {
          nodes.update({ id: params.nodes[0], fixed: false });
        }
      }
      network.on("dragStart", dragstart);

      var dragend = function(params) {
        if (!edgemode && params.nodes.length) {
          // fix node if needs be and update the position
          nodes.update({ id: params.nodes[0], fixed: !options.physics,
            x: params.pointer.canvas.x, y: params.pointer.canvas.y });
        }
      }
      network.on("dragEnd", dragend);
    }

    /**
     * Gets a contrasting font colour to a given hex background colour.
     * https://stackoverflow.com/questions/3942878/how-to-decide-font-color-in-white-or-black-depending-on-background-color
     * @param hex the hexadecimal colour code.
     * @return the colour to use as a contrast.
     **/
    function getContrastFontColour(hex) {
      // convert to component form
      var col = hexToRgb(hex);

      // calculate luminence for components
      for (c in col) {
        col[c] = col[c] / 255.0;
        if (col[c] <= 0.03928) {
          col[c] = col[c] / 12.92;
        } else {
          col[c] = Math.pow((col[c] + 0.055) / 1.055, 2.4);
        }
      }
      L = 0.2126 * col.r + 0.7152 * col.g + 0.0722 * col.b;

      // check if black or white contrast should be used
      if (L > 0.179) {
        return "#000000";
      }  else {
        return "#ffffff";
      }
    }

    /**
     * Converts a hexadecimal string to component form.
     * https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
     * @param hex the hexadecimal colour code.
     * @return the colour in component RGB format.
     **/
    function hexToRgb(hex) {
      // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
      var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
      hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
      });

      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
      } : null;
    }

    /**
     * Gets a dataset for nodes or edges as required by a parameter.
     * @param node whether the desired dataset is a node. Defaults as true.
     * @return the requested dataset stored in the object.
     **/
    function getDataset(node = true) {
      if (node) {
        return nodes;
      } else {
        return edges;
      }
    }

    /**
     * Returns data that should be accessible to the user.
     * @param data the data to filter for the user.
     * @param node whether the data is for a node. Defaults as true.
     * @return the data that is useful to the user.
     **/
    function getReturnData(data, node = true) {
      // add id to data
      var usrdata = { id: data.id };

      if (node) {
        // add the degree to the node
        if (options.directed) {
          usrdata.degree = data.degree;
        } else {
          usrdata.degree = data.degree.in + data.degree.out;
        }

        // add weight
        usrdata.value = data.value;
      } else {
        // add endpoints
        usrdata.to = data.to;
        usrdata.from = data.from;

        // add weight
        usrdata.value = data.value;
      }

      return usrdata;
    }

    /**
     * Parses object data about a node or edge to fit well in the program.
     * @param data the data to prase for the program.
     * @param node if the data is for a node.
     * @param oldStyle the previous styling data stored in program (only set if updating)
     * @param full whether to do a full parse. If true, will generate missing properties such as value and font.
     * @return the filtered and parsed input data.
     **/
    function parseInputData(data, node = true, oldstyle, full = true) {
      if (oldstyle === undefined && full) {
        // convert oldData to an object to avoid errors, we are adding new data now
        oldstyle = {};
        if (data === undefined) {
          data = { };
        }

        // generate ID if data is a string and we are adding a node (oldData = undefined)
        if (node) {
          if (!["undefined", "object", "function"].includes(typeof data)) {
            // if string, use this as the target id
            data = { id: data };
          } else if (!data.id) {
            // no id supplied, generate one that won't collide
            var idnum = 1;
            while (nodes.get("" + idnum)) {
              ++idnum;
            }
            data.id = idnum;
          }
        }
      }

      // convert key data to strings
      if (data.id) {
        data.id = data.id + "";
      }
      if (data.to) {
        data.to = data.to + "";
      }
      if (data.from) {
        data.from = data.from + "";
      }

      if (full) {
        if (node) {
          // set the font colour to a contrast colour if text appears inside the node
          data.font = { color: "#000000" };
          var shape = data.shape !== undefined ? data.shape : oldstyle.shape;
          var colour = data.color !== undefined ? data.color : oldstyle.color;
          if (["ellipse", "circle", "box"].includes(shape)) {
            data.font.color = getContrastFontColour(colour);
          }
          delete data.degree; // remove degree, private property, can't be set
        } else {
          // set colour correctly if edge
          if (data.inheritcol === true || data.color === undefined) {
            if (oldstyle !== undefined) {
              data.inheritcol = true;
              data.color = { inherit: true };
            } else {
              data.color = oldstyle.color;
            }
          }

          // use private label for edges
          data._label = data.label;
        }
        delete data.label; // don't set private label data

        // verify not to use negative weights
        if (data.value === undefined || typeof data.value !== "number") {
          data.value = 0;
        } else if (!options.negativeWeights && data.value < 0) {
          data.value = 0;
        }
      }

      // delete all "undefined" properties, this will cause bugs
      for (key in data) {
        if (data[key] === undefined) {
          delete data[key];
        }
      }

      // return our parsed and edited data object
      return data;
    }

    /**
     * Converts a data object to a corrosponding ID using this.get.
     * @param data the data object to get data for.
     * @param node if the data is for a node.
     * @return the id of the data object requested.
     **/
    this.dataToId = function(data, node = true, allowArray = false) {
      if (typeof data === "object") {
        var search = this.get(data, node);
        if (Array.isArray(search)) {
          if (!allowArray) {
            search = search[0];
          } else {
            return search.map(d => d.id);
          }
        } else if (search === null) {
          search = { id: null };
        }
        return search.id;
      }

      // force id to string
      if (data !== undefined && data !== null) {
        data = data + "";
      }
      return data;
    }

    /**
     * Updates the container when a graph has been popped in/out.
     **/
    this.updateContainer = function() {
      // destroy old network
      if (network !== undefined) {
        network.destroy();
      }

      // get data and container, then init network
      var data = { nodes: nodes, edges: edges };
      var container = undefined;
      try {
        // POP exists, real page not testing, get correct container
        container = $(`#network-${ id }`, POP.getDocument("graph"))[0];
      } catch (e) {
        // testing without POP, make a new div and hide it
        container = document.createElement("div");
        container.style.visibility = "hidden";
      }
      network = new vis.Network(container, data, generateNetworkOptions());
      try {
        // only subscribe events if we have a page to view / interact with
        subscribeEvents();
      } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }

      // reset to add edge mode
      resetEdgeMode();
    }

    /**
     * Clears the current data in the graph.
     **/
    this.clear = function() {
      edges.clear();
      nodes.clear();
    }

    /**
     * Redraws the current data in the graph.
     **/
    this.draw = function() {
      network.redraw();
    }

    /**
     * Fits the current data in the graph to the screen.
     **/
    this.fit = function() {
      network.fit();
    }

    /**
     * Destroys the network and removes from DOM.
     **/
    this.destroy = function() {
      network.destroy();
    }

    /**
     * Sets the options for the network.
     * @param opts the options to set for the graph.
     **/
    this.setOptions = function(opts) {
      // update options
      for (o in opts) {
        if (opts[o] !== undefined) {
          options[o] = opts[o]
        }
      }
      network.setOptions(generateNetworkOptions());

      // relabel nodes and edges based on settings, and adjust physics and weights also
      nodes.forEach((item, i) => {
        this.setData(item.id, "fixed", !options.physics);
        if (!opts.negativeWeights && this.getData(item.id, "value") < 0) {
          this.setData(item.id, "value", 0);
        }
        this.updateLabel(item.id);
      });

      var repeats = []; // log repeated edges that need removing
      edges.forEach((item, i) => {
        if (!opts.negativeWeights && this.getData(item.id, "value", false) < 0) {
          this.setData(item.id, "value", 0, false);
        }
        this.updateLabel(item.id, false);

        // mark the edge for deletion if there is a parallel edge, and no weights or its weight is lower, and this not marked to delete)
        if (!opts.multigraph) {
          var parallel = this.edgeTo(item.from, item.to);
          if (Array.isArray(parallel)) {
            // array returned, multiple edges for this pair of nodes
            if (options.weightedEdges) {
              // keep only highest weighted edge, if we use a weighted graph
              parallel.sort((a, b) => b.value - a.value || a.id.localeCompare(b.id) );
            } else {
              parallel.sort((a, b) => a.id.localeCompare(b.id) );
            }

            // remove all but first/largest edge
            parallel.forEach((e, i) => {
              if (i !== 0 && !repeats.includes(e.id)) {
                repeats.push(e.id);
              }
            });
          }
        }
      });

      // remove parallel edges
      for (var i in repeats) {
        this.remove(repeats[i], false);
      }

      // reset to add edge mode
      resetEdgeMode();
    }

    /**
     * Creates an Object of the current data in the graph. Used for saving data.
     * @return the data in the current graph.
     **/
    this.data = function() {
      // get node set and remove unwanted data
      network.storePositions();
      var n = JSON.parse(JSON.stringify(nodes.get()));
      n.forEach((item, i) => {
        delete n[i].label;
        delete n[i].degree;
        delete n[i].fixed;
      });

      // get edge set and remove unwanted label
      var e = JSON.parse(JSON.stringify(edges.get()));
      e.forEach((item, i) => {
        if (e[i]._label !== undefined && e[i]._label !== "") {
          e[i].label = e[i]._label;
          delete e[i]._label;
        } else {
          delete e[i].label;
        }
      });

      return {
        name: title,
        nodes: n,
        edges: e,
        options: options
      };
    }

    /**
     * Gets the current graph as an image.
     * @return the base64 encoded canvas image string.
     **/
    this.getImage = function() {
      this.fit();
      return network.body.container.firstChild.firstChild.toDataURL();
    }

    /**
     * Sets the title for the graph.
     * @param t the new title for the grpah.
     **/
    this.setTitle = function(t) {
      // reset internal name
      title = t;
      id = stripToID(t);
    }

    /**
     * Adds a node or edge to the graph.
     * @param data the data to be added to the graph. If unsupplied and a node, an ID will be generated automatically.
     * @param node whether the data is for a node. Defaults as true.
     * @return false if already a node with this id, otherwise returns item.
     **/
    this.add = function(data, node = true) {
      if (Array.isArray(data)) {
        // add each element if data is an array
        for (var i in data) {
          data[i] = this.add(data[i], node);
        }
        return data;
      }

      // filter node add requests
      if (!node) {
        if (data === undefined || data.to === undefined || data.from === undefined ||
                    (!options.multigraph && this.edgeTo(data.from, data.to))) {
          // need endpoint data at least to init an edge, and no other edge in a non-multigraph
          return false;
        } else {
          // if colour not defined by default, we will inherit it
          if (data.color === undefined) {
            data.inheritcol = true;
          }

          // set a default empty label if none supplied
          data._label = data.label !== undefined ? data.label : "";
          delete data.label;
        }
      }

      // parse the input data for the program
      data = parseInputData(data, node);

      if (node) {
        // add degree
        data.degree = { in: 0, out: 0 };

        // set default shape and colour if not supplied
        data.shape = data.shape !== undefined ? data.shape : "circle";
        data.color = data.color !== undefined ? data.color : "#97C2FC";

        // allow physics when stabalised if no x/y coord supplied
        if (data.x === undefined || data.y === undefined) {
          network.once("stabilized", () => {
            this.setData(data.id, "fixed", !options.physics);
          });
        } else {
          data.fixed = !options.physics;
        }
      }

      // add to dataset, catching errors to detect id collision
      try {
        if (node) {
          // add node
          nodes.add(data);
        } else {
          // edge, add and correct degrees
          edges.add(data);
          this.adjustDegree(data.from, true, true);
          this.adjustDegree(data.to, false, true);

          // reset to add edge mode
          resetEdgeMode(true);
        }

        // update the label
        this.updateLabel(data.id, node);
        return getReturnData(data, node);
      } catch (e) {
        // error, return failed
        return false;
      }
    }

    /**
     * Gets a node or edge from the graph.
     * @param data the data to query the graph with. If a string, will use as ID. If undefined will get all of the nodes / edges.
     * @param node whether the data is for a node. Defaults as true.
     * @return a list of nodes/edges that meet the data search criteria.
     * NOTE: this function can be used to search by degrees also. If using degree as a search
     * it will check for total degree on undirected graphs, and a match for either in or out on directed graphs.
     * If supplying in and out degree data, this will be matched exactly.
     **/
    this.get = function(data = undefined, node = true) {
      // get node or edge dataset
      var dataset = getDataset(node);

      // set the data to an object if undefined or a string
      if (data === undefined) {
        data = { }; // no search terms, return all nodes
      } else if (typeof data !== "object") {
        data = { id: data }; // set string to the only search term
      }
      data = parseInputData(data, node, undefined, false);

      // convert degree to number if undirected
      if (!options.directed && typeof data.degree === "object") {
        data.degree = (data.degree.in ? data.degree.in : 0) + (data.degree.out ? data.degree.out : 0);
      }

      // search using the data
      var searcher = function(data) {
        return dataset.get({
          filter: function(item) {
            // iterate over keys in data lookup
            for (var key in data) {
              if (data[key] === undefined) {
                // skip, will cause all to fail the match
                continue;
              } else if (key === "degree") {
                // check special degree search cases
                var t = typeof data[key] === "number" ? data[key] : undefined;
                var i = t !== undefined ? undefined : data[key].in;
                var o = t !== undefined ? undefined : data[key].out;

                if (t !== undefined) {
                  if ((options.directed && (item[key].in !== t && item[key].out !== t)) ||
                      (!options.directed && t !== item[key].in + item[key].out)) {
                    // if total degree searched and in directed graph neither in or out = search
                    // or undirected and total != degree, no match on this item
                    return false;
                  }
                } else {
                  if ((i !== undefined && i !== item[key].in) || (o !== undefined && o !== item[key].out)) {
                    // search by in or out terms
                    return false;
                  }
                }
              } else if (data.hasOwnProperty(key) && item.hasOwnProperty(key) && item[key] !== data[key]) {
                // non-matching property, discard this data
                return false;
              }
            }
            return true;
          },
        }).map(d => getReturnData(d, node));
      }
      var ret = searcher(data);

      // find edges in other direction also if required
      if (!node && !options.directed && (data.to || data.from)) {
        // remove duplicate edges, i.e. those that loop to self
        ret = ret.filter((i) => i.to !== i.from);

        // run search in other direction
        var tmp = data.to; data.to = data.from; data.from = tmp;
        ret = ret.concat(searcher(data));
      }

      // return undefined or the single element if 0 or 1 matches
      if (ret.length === 0) {
        ret = null;
      } else if (ret.length === 1) {
        ret = ret[0];
      }
      return ret;
    }

    /**
     * Checks if a node exists in the graph.
     * @param id the id of the node to search for.
     * @return the node if it exists, otherwise false.
     **/
    this.isNode = function(id) {
      id = this.dataToId(id, true);
      if (!id) {
        return false;
      }
      var res = this.get(id);
      return res === null ? false : res;
    }

    /**
     * Gets all the source/sink nodes in the graph,
     * i.e. the nodes in a directed graph with no incoming/outgoing edges.
     * @param source Whether to search for source node or sink nodes. Defaults to true, source nodes.
     * @return a list of source or sink nodes
     **/
    this.getSourceSink = function(source = true) {
      if (options.directed) {
        var data = source ? { degree: { in: 0 } } : { degree: { out: 0 } };
        var res = this.get(data);
        if (Array.isArray(res)) {
          return res;
        } else if (res === null) {
          return [ ];
        } else {
          return [ res ];
        }
      } else {
        return [ ];
      }
    }

    /**
     * Checks if nodes are adjacent to each other.
     * @param id1 the first node id.
     * @param id2 the second node id.
     * @return the edge if it exists, otherwise false.
     **/
    this.isAdjacent = function(id1, id2) {
      id1 = this.dataToId(id1, true);
      id2 = this.dataToId(id2, true);

      var e = this.edgeTo(id1, id2);
      if (e === false && options.directed) {
        // check if there is a connection in the other direction
        e = this.edgeTo(id2, id1);
      }
      return e;
    }

    /**
     * Checks if there is an edge from one node to another.
     * If the graph is directed only this direction will return the edge.
     * @param id1 the first node ID with edge leaving it.
     * @param id2 the second node ID with edge incoming.
     * @return the edge if it exists, otherwise undefined.
     **/
    this.edgeTo = function(id1, id2) {
      id1 = this.dataToId(id1, true);
      id2 = this.dataToId(id2, true);
      if (!this.isNode(id1) || !this.isNode(id2)) {
        return null;
      }

      // get edges connecting id1 and id2
      var e = this.get({ from: id1, to: id2 }, false);
      return e === null ? false : e;
    }

    /**
     * Removes a node or edge from the graph.
     * @param data the data to query the graph with. If a string, will use as ID.
     * @param node whether the data is for a node. Defaults as true.
     * @return a list of nodes/edges that meet the data criteria and were deleted.
     **/
    this.remove = function(data, node = true) {
      // set the data to an object if a string
      if (typeof data === "string") {
        data = { id: data };
      }

      // get queried data as an array
      var search = this.get(data, node);
      if (Array.isArray(search)) {
        data = search;
      } else if (search === null) {
        return []; // no data with search, do nothing
      } else {
        data = [ search ];
      }

      // remove data and return removed list
      var dataset = getDataset(node);
      data.forEach((item, i) => {
        if (node) {
          // if node removed, remove all incidents edges
          edges.remove(this.incidents(item.id).map(d => d.id));
        } else {
          // if edge update the degrees
          this.adjustDegree(item.from, true, false);
          this.adjustDegree(item.to, false, false);
        }

        // remove item
        dataset.remove(item.id);
      });
      return data;
    }

    /**
     * A function to update a label for a given node / edge.
     * @param id the id of the node / edge to update.
     * @param node whether the data is for a node. Defaults as true.
     **/
    this.updateLabel = function(id, node = true) {
      id = this.dataToId(id, node);

      var data = getDataset(node).get(id); // get item in question
      var label = "";
      if (node) {
        // set label to id of node
        label = ` ${ data.id } `;

        // add weight/degree labels if set
        if (options.weightedNodes) {
          label += `\nW=${ data.value }`;
        }
        if (options.degrees) {
          var degree = this.getDegree(id);
          label += `\nD=${ options.directed ? `${ degree.in}, ${ degree.out }` : degree }`;
        }
      } else {
        if (data._label !== undefined && data._label !== "") {
          label = `${ data._label }\n`;
        }

        // label is either weight or nothing
        if (options.weightedEdges) {
          label += data.value + "";
        }
      }

      // make the label empty to force update if no label needed
      if (label === "") {
        label = " ";
      }

      // set the label
      this.setData(id, "label", label, node);

      // reID edges with a good edge ID
      if (!node) {
        // genrate an id from data
        var newID = `e-f${ data.from }-t${ data.to }`;

        // reid the edge if there was a change in to/from data
        if (data.id.indexOf(newID) === -1) {
          var suffix = 0;
          var targetID = newID;
          while (!this.setItemID(data.id, targetID, node)) {
            targetID = newID + "_" + (++suffix);
          }
        }
      }
    }

    /**
     * Gets the degree of a node.
     * @param id the node's id. If left undefined will get all the degrees of nodes in the graph.
     * @return the degree of the given node. If directed, will return a dictionary of in degree and out degree.
     **/
    this.getDegree = function(id) {
      id = this.dataToId(id, true);

      if (id === null) {
        // no node specified, generate list of nodes and degrees
        var degrees = [];
        nodes.forEach((item, i) => {
          degrees.push({ id: item.id, degree: this.getDegree(item.id) });
        });
        return degrees;
      } else {
        // node specified, get degree as either directed pair or combined
        var degree = this.getData(id, "degree", true);
        if (options.directed) {
          return degree;
        } else {
          return degree.in + degree.out;
        }
      }
    }

    /**
     * Adjusts the degree of a node by one. Either the in / out degree, and either +1 or -1.
     * @param id the id of the node to adjust.
     * @param in whether the in degree is being adjusted. Defualts to true.
     * @param inc whether to increment the degree. Defaults to true.
     **/
    this.adjustDegree = function(id, out = true, inc = true) {
      id = this.dataToId(id, true);

      var node = nodes.get(id);
      if (out) {
        node.degree.out += inc ? 1 : -1;
      } else {
        node.degree.in += inc ? 1 : -1;
      }
      this.updateLabel(id);
    }

    /**
     * Gets the value of an item in the graph.
     * @param id the item's id.
     * @param node if the item is a node.
     * @return the nodes value. If weightings are off this will return undefined.
     **/
    this.getValue = function(id, node = true) {
      id = this.dataToId(id, node);
      var val = this.getData(id, "value", node);
      return (node && options.weightedNodes) || (!node && options.weightedEdges) ? val : null;
    }

    /**
     * Sets the value of an item in the graph.
     * @param id the item's id.
     * @param value the new value to set.
     * @param node if the item is a node.
     * @return the value set.
     **/
    this.setValue = function(id, value, node = true) {
      id = this.dataToId(id, node);
      if (!options.negativeWeights && value < 0 || value === undefined) {
        value = 0;
      }
      return this.setData(id, "value", value, node);
    }

    /**
     * Gets the style data of an item.
     * @param id the item's id.
     * @param node if the item is a node.
     * @return the style information for this item.
     **/
    this.getStyle = function(id, node = true) {
      id = this.dataToId(id, node);
      var dataset = getDataset(node); // get node or edge dataset
      var data = dataset.get(id);
      if (data == null) {
        return null;
      }

      // get style info using lambda to return the data or a default value
      var dataOrDefault = (data, def) => { return data === undefined ? def : data; };
      var style = {
        color: dataOrDefault(data.color, "#97C2FC"),
      };

      if (node) {
        // add node extra data
        style.shape = dataOrDefault(data.shape, "circle");
      } else {
        // add edge extra data
        style.inheritcol = !(typeof style.color === "string");
        if (style.inheritcol) {
          style.color = nodes.get(data.from).color;
        }
        style.label = data._label;
      }

      return style;
    }

    /**
     * Sets the style / data of an item. NOTE:
     * this will be able to access all of
     * the data for the item, such as value.
     * @param id the item's id.
     * @param data the new data to put as the style. NOTE: this will be able to edit all of the item.
     * @param node if the item is a node.
     **/
    this.setStyle = function(id, data, node = true) {
      id = this.dataToId(id, node, true);
      if (Array.isArray(id)) {
        // perform for each item if an array was found
        id.forEach((item, i) => {
          this.setStyle(item, JSON.parse(JSON.stringify(data)), node);
        }); return;
      }

      // verify not to use negative weights
      var oldstyle = this.getStyle(id, node);
      var delvalue = data.value === undefined;
      data = parseInputData(data, node, oldstyle);
      if (delvalue) {
        delete data.value;
      }

      // rename the item if required by the update
      var updateLabel = data.value !== undefined || data._label !== undefined;
      if (!(data.id && this.setItemID(id, data.id, node))) {
        // add the id to data if not renamed
        data.id = id;
      } else {
        updateLabel = true;
      }

      // adjust degrees if we alter the edge endpoints
      if (!node && (data.to || data.from)) {
        var olddata = this.get(id, node);
        if (data.to) {
          this.adjustDegree(data.to, false);
          this.adjustDegree(data.to, false, false);
        }
        if (data.from) {
          this.adjustDegree(data.from, true);
          this.adjustDegree(data.from, true, false);
        }
      }

      // set style
      var dataset = getDataset(node); // get node or edge dataset
      dataset.update(data);

      // set label if value or degree has been updated
      if (updateLabel) {
        this.updateLabel(data.id, node);
      }
    }

    /**
     * Renames / reIDs a node / edge.
     * @param oldid the old item name / id.
     * @param newid the new item name / id.
     * @param node if the item is a node.
     * @return whether ther the item rename was permitted.
     */
    this.setItemID = function(oldid, newid, node = true) {
      if (oldid === newid) {
        return true;
      }
      oldid = this.dataToId(oldid, node);

      // alter id in item
      var dataset = getDataset(node); // get node or edge dataset
      var item = dataset.get(oldid);
      var newitem = JSON.parse(JSON.stringify(item));
      newitem.id = newid + "";

      // add new object to dataset, remove old
      try {
        dataset.add(newitem);
      } catch {
        // error adding, same id as another item, return failed
        return false;
      }
      dataset.remove(oldid); // remove the old copy
      this.updateLabel(newid, node); // update the label

      // if this was a node, correct edges to point to the new label
      if (node) {
        edges.forEach((item, i) => {
          if (item.to === oldid) {
            item.to = newid;
          }
          if (item.from === oldid) {
            item.from = newid;
          }
          edges.update(item);
        });
      }
      return true; // no errors
    }

    /**
     * Sets the colour of an item.
     * @param id the item's id.
     * @param colour the new data to put as the colour. Accepts colour object allowed for items.
     * @param node if the item is a node.
     **/
    this.setColour = function(id, colour, node = true) {
      id = this.dataToId(id, node);

      // update to inherit colour if on edge
      if (node == false && colour === undefined) {
        colour = { inherit: true };
      }

      return this.setData(id, "color", colour, node);
    }

    /**
     * Gets the value from an item in the graph by a certain key.
     * @param id the item's id.
     * @param key the key of data to get.
     * @param node if the item is a node.
     * @return the value for this key. If no item found or key doesn't exist, returns null.
     **/
    this.getData = function(id, key, node = true) {
      id = this.dataToId(id, node);
      var dataset = getDataset(node);
      var data = dataset.get(id);
      return data ? data[key] : null;
    }

    /**
     * Sets the value from an item in the graph by a certain key.
     * @param id the item's id.
     * @param key the key of data to get.
     * @param value the value to set data to.
     * @param node if the item is a node.
     * @return the value for this key. If no item found or key doesn't exist, returns null.
     **/
    this.setData = function(id, key, value, node = true) {
      id = this.dataToId(id, node);
      var dataset = getDataset(node);
      var data = dataset.get(id);
      if (data !== null && value !== undefined && key !== "degree") {
        // update dataset if ID exists, given data, not editing degree
        data[key] = value;
        dataset.update(data);
        if (key === "value") {
          // update the label if we updated the value
          this.updateLabel(id, node);
        }
        return value;
      } else {
        return null;
      }
    }

    /**
     * Highlights a node / edge by selecting it.
     * @param id the item's id.
     * @param node if the item is a node.
     **/
    this.highlight = function(id, node = true) {
      // get valid id
      id = this.dataToId(id, node, true);
      if (id) {
        // convert to array for vis js
        if (!Array.isArray(id)) {
          id = [ id ];
        }

        // select to highlight
        network.unselectAll();
        try {
          if (node) {
            network.selectNodes(id, false);
          } else {
            network.selectEdges(id);
          }
        } catch { }
      }
    }

    /**
     * Gets a list of neighbours to a given node.
     * @param nodeid the node to get neighbours for.
     * @param direction the direction to get neighbours for. Defaults to "" (both directions). Can also be "to" (inbound, parent nodes) or "from" (outbound, child nodes).
     * @return a list of neighbours. If directed will include the direction. Will also contain the cost to the neighbours.
     **/
    this.neighbours = function(nodeid, direction = "") {
      nodeid = this.dataToId(nodeid, true);
      // return all adjacent nodes if undirected
      if (!options.directed) {
        direction = "";
      }

      // correct the parameter to match vis.js. Their directions don't make sense in this context / function handler
      if (direction.toLowerCase() === "to") {
        direction = "from";
      } else if (direction.toLowerCase() === "from") {
        direction = "to";
      }

      // get neighbours and cost to them
      var neighbours = network.getConnectedNodes(nodeid, direction).map(d => this.get(d));
      neighbours.forEach((n) => {
        // get the edge between the nodes
        var edge = direction === "" ? this.isAdjacent(nodeid, n.id)
                  : direction === "from" ? this.edgeTo(n.id, nodeid)
                  : direction === "to" ? this.edgeTo(nodeid, n.id) : null;

        // mark as inbound or outbound if directed
        if (options.directed) {
          n.direction = edge.to === nodeid ? "inbound" : "outbound";
        }

        // evaluate cost
        n.cost = null;
        if (options.weightedNodes) {
          n.cost = n.value;
        }
        if (options.weightedEdges) {
          n.cost = edge.value + n.cost;
        }
      });

      return neighbours.sort(
        (a, b) => a.id.toLowerCase() > b.id.toLowerCase() ? 1
                : a.id.toLowerCase() < b.id.toLowerCase() ? -1
                : 0
      );
    }

    /**
     * Gets a list of incident edges to a given node.
     * @param nodeid the node to get incident edges for.
     * @param direction the direction to get neighbours for. Defaults to "" (both directions). Can also be "to" (inbound, parent nodes) or "from" (outbound, child nodes).
     * @return a list of incident edges.
     **/
    this.incidents = function(nodeid, direction = "") {
      nodeid = this.dataToId(nodeid, true);
      if (!options.directed) {
        // if undirected, single this.get will fetch all results
        direction = "to";
      }

      // store incident edges
      var incidents = [];

      // get the edges inbound if required
      if (direction !== "from") {
        var edges = this.get({ to: nodeid }, false);
        edges = Array.isArray(edges) ? edges : [ edges ]; // convet to array if needed
        if (edges[0] !== null) {
          // only add if were inbound edges
          incidents = incidents.concat(edges);
        }
      }

      // get the edges outbound if required
      if (direction !== "to") {
        var edges = this.get({ from: nodeid }, false);
        edges = Array.isArray(edges) ? edges : [ edges ]; // convet to array if needed
        if (edges[0] !== null) {
          // only add if were inbound edges
          incidents = incidents.concat(edges);
        }
      }
      return incidents;
    }

    /**
     * Performs a BFS or DFS algorithm from a given node.
     * @param startnodeid the id of the node to start the search with. If undefined will take the first defined node.
     * @param bfs What mode to search with. Defaults to "bfs". Also supports "dfs" and "dijkstra".
     * @return a list of nodes in the order they were explored, along with the cost to reach there.
     **/
    this.search = function(startnodeid, mode = "bfs") {
      startnodeid = this.dataToId(startnodeid, true);
      var visited = []; // visiting order with weights
      var queue = []; // the queue of nodes to visit
      mode = mode.toLowerCase();
      if (!mode in [ "bfs", "dfs", "dijkstra" ]) {
        mode = "bfs"; // default mode to bfs if unrecognised
      }

      // create dequeue method to call
      var dequeue = (() => {
        if (mode === "dijkstra") {
          // Dijkstra's algorithm, find the (first) minimum cost then dequeue
          var min = queue.reduce((prev, current) => prev.cost <= current.cost ? prev : current);
          var index = queue.findIndex((n) => n.id === min.id);
          return queue.splice(index, 1)[0];
        } else {
          // BFS or DFS, remove from beginning (BFS, queue), or end (DFS, stack) of queue
          return mode === "bfs" ? queue.shift() : queue.pop();
        }
      });

      // create enqueue method to call
      var enqueue = ((nodes, currentPath = []) => {
        // filter out nodes already visited, adjust cost, push to queue
        nodes.forEach(node => {
          // define cost as 1 if undefined (i.e. path length)
          if ((mode !== "dijkstra" || node.cost === null) && node.id !== startnodeid) {
            node.cost = 1;
          }

          // adjust the stored data as required
          node.cost += currentPath.length > 0 ? currentPath[currentPath.length - 1].cost : 0;
          node.path = currentPath.slice();
          delete node.direction;
          node.path.push({ ...node });
          delete node.path[node.path.length - 1].path;

          // check if the item needs queuing, first check if in visited list
          if (visited.findIndex((n) => n.id === node.id) === -1) {
            var index = queue.findIndex((n) => n.id === node.id); // get index of same ID item if in queue
            if (index != -1 && ((mode === "dfs") ||                         // item already exists in queue and DFS or...
                (mode === "dijkstra" && node.cost < queue[index].cost))) {  // in Dijkstra's mode and this is lower cost
              // remove the old value and mark new one as needing queuing, old item is outdated / unwanted
              queue.splice(index, 1);
              index = -1;
            }
            // NOTE: if in queue, Dijkstra's mode and higher cost will not be queued since index != -1

            // only queue if not in queue list also
            if (index == -1) {
              queue.push(node);
            }
          }
        });
      });

      // get a start node if none supplied
      if (!this.isNode(startnodeid)) {
        var newstartid = undefined;
        if (options.directed) {
          var sources = this.getSourceSink();
          if (sources.length) {
            newstartid = sources.reduce((prev, current) => prev.degree.out >= current.degree.out ? prev : current).id;
          }
        }

        // no start id given, set as first defined node
        if (newstartid === undefined) {
          try {
            newstartid = nodes.getIds()[0];
          } catch {
            // return nothing if we don't actually have nodes in the graph to search from
            return [ ];
          }
        }
        startnodeid = newstartid;
      }

      // perform the search using standard algorithm
      var startnode = this.get(startnodeid);
      startnode.cost = 0; // 0 cost to get to the start node
      enqueue([ startnode ]); // add start node to queue

      // while we can explore nodes
      while (queue.length) {
        // get node to add to visited
        var current = dequeue();
        visited.push(current);

        // get outgoing neighbours and queue them
        enqueue(this.neighbours(current.id, "from"), current.path);
      }

      return visited;
    }

    /**
     * Performs a BFS or DFS algorithm from a given node.
     * @param startnodeid the id of the node to start the search with. If undefined will take the first defined node.
     * @param targetnodeid the node to check is connected. If undefined will check the whole grpah is connected.
     * @return true if the two nodes are connected. If no targetnode and directed, will check for partial connectivity.
     * NOTE: if partially connected (directed, not connected, connected when using undirected edges) will return "directed".
     **/
    this.connected = function(startnodeid, targetnodeid = undefined) {
      startnodeid = this.dataToId(startnodeid, true);
      targetnodeid = this.dataToId(targetnodeid, true);

      // verify we have a start node
      if (startnodeid !== undefined && !this.isNode(startnodeid)) {
        return null;
      }

      // run dfs
      var dfs = this.search(startnodeid);
      if (!targetnodeid) {
        // check if all nodes were reached to check connected
        var conn = (dfs.length === nodes.get().length);
        if (!conn && options.directed) {
          // check for partial connectivity if directed graph
          options.directed = false;
          dfs = this.search(startnodeid);
          options.directed = true;
          conn = (dfs.length === nodes.get().length) ? "partial" : false;
        }
        return conn;
      } else {
        // verify correct targetnode
        if (!this.isNode(targetnodeid)) {
          return null;
        }

        // check if connected
        var conn = dfs.find(n => n.id === targetnodeid) !== undefined;
        if (!conn && options.directed) {
          // check for partial connectivity if directed graph
          options.directed = false;
          dfs = this.search(startnodeid);
          options.directed = true;
          conn = dfs.find(n => n.id === targetnodeid) !== undefined ? "partial" : false;
        }

        return conn;
      }
    }

    /**
     * Performs Dijkstra's algorithm to find the shortest path between points.
     * Can also find the shortest path from a point to all points in the graph.
     * @param startnodeid the id of the node to start the search with. If undefined will take the first defined node.
     * @param targetnodeid the node to find shortest path to. If undefined will find the shortest path to all nodes.
     * @return either the shortest path to the target node, or a list of nodes and their shortest path.
     **/
    this.shortestPath = function(startnodeid, targetnodeid = undefined) {
      startnodeid = this.dataToId(startnodeid, true);
      targetnodeid = this.dataToId(targetnodeid, true);

      // verify we have a start node
      if (startnodeid !== undefined && !this.isNode(startnodeid)) {
        return null;
      }

      // perform the search for shortest path between nodes
      var djikstra = this.search(startnodeid, "dijkstra");
      if (!targetnodeid) {
        // return reachable nodes
        return djikstra;
      } else {
        // target node specified, return this result
        var ret = djikstra.find(n => n.id === targetnodeid);
        return ret !== undefined ? ret : null;
      }
    }

    /**
     * Subscribes to an event of the network.
     * @param e the event name to subscribe to.
     * @param fn the event handler to apply.
     * @param mode the mode to subscribe with. Can either be "once", "on", or "off". Defaults to "once".
     **/
    this.handleEvent = function(e, fn, mode = "once") {
      mode = mode.toLowerCase();

      switch (mode) {
        case "on":
          network.on(e, fn);
          break;
        case "off":
          network.off(e, fn);
          break;
        default:
          network.once(e, fn);
      }
    }

    // CONSTRUCTOR CODE
    if (options === undefined) {
      options = new GraphOptions(false, true, false, false, false, false, false, 16, "horizontal");
    }
    ns.forEach((item, i) => {
      this.add(item);
    });
    es.forEach((item, i) => {
      this.add(item, false);
    });

    // create a data class and corrosponding options class
    this.updateContainer();
    this.fit();
  }

  /**
   * Wraps a graph interaction function to work for the list of graphs.
   * The wrapper function performs the same action on a graph named by graphid.
   * The graphid will state the graph to use. If unsupplied will use the current graph.
   * @param fnname the name of the graph prototype function to wrap.
   * @return the wrapper function to handle the graph interaction. Same parameters as original function, with additional graphid parameter.
   */
  function graphWrapper(fnname) {
    // convert the constructor to a string and get the index of function definition
    var gpstr = Graph.prototype.constructor.toString();
    var startIndex = gpstr.indexOf(`this.${ fnname } = function`);
    if (startIndex == -1) {
      // incase we are using a compressed version
      startIndex = gpstr.indexOf(`this.${ fnname }=function`);
    }

    // get string between first parentheses, this is named parameters
    var paramString = gpstr.substring(gpstr.indexOf('(', startIndex) + 1, gpstr.indexOf(')', startIndex));

    // get the names of the actual parameters to form the argument string
    var argString = "";
    var args = paramString.split(",");
    args.forEach((item, i) => {
      var arg = item.trim().split('=')[0]; // remove default value if there is one
      argString += `,${ arg }`; // add to arg string
    });
    argString = argString.substring(1); // remove initial ','

    // add 'graphid' as extra parameter for wrapper. use only graphid if no parameters for function
    paramString = argString === "" ? "graphid=undefined" : `${ paramString },graphid=undefined`;

    // generate wrapper function and return
    return eval(`(${ paramString }) => {\n\treturn getGraphByID(graphid).${ fnname }(${ argString });\n}`);
  }

  /**
   * Function to create a new graph. Sets this as the current graph.
   * If no new data is supplied this will be auto generated with defaults.
   * @param name the name to give the new graph. Will generate if not supplied.
   * @param nodes the nodes to give the new graph. Not needed during creation.
   * @param edges the edges to give the new graph. Not needed during creation.
   * @param options the options to give the new graph. Not needed during creation.
   * @return the id of the new graph.
   **/
  function create(name, nodes, edges, options) {
    // provide name if not supplied
    if (name === undefined || stripToID(name) === "current") {
      name = "untitled";
    }

    // create new network div in view
    var tmpname = stripToID(Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15));
    // add to page
    try {
      var html = `<div id="network-${ tmpname }" class="network"></div>`;
      var item = $.parseHTML(html);
      $("#graph-container", POP.getDocument("graph")).append(item);

      // add new graph to selection list and select it
      var lastDropdown = $("#graph-select", POP.getDocument("graph")).children().last();
      for (var i = 0; i < staticDropdownCount - 1; ++i) {
        lastDropdown = lastDropdown.prev();
      }
      lastDropdown.before(
        $("<option>", { value: "graph-" + tmpname, text: tmpname })
      );
      $("#graph-select", POP.getDocument("graph")).val("graph-" + tmpname);
    } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }

    // create graph
    graphs[tmpname] = new Graph(tmpname, nodes, edges, options);

    // try rename to the desired name
    var name = rename(tmpname, name);
    setCurrent(name);
    return name;
  }

  /**
   * Destroys a graph and deletes it from the graph dictionary.
   * @param grapid the id of the graph to destroy. If undefined will delete the current graph.
   **/
  function destroy(graphid) {
    // get correct graph id
    if (graphid === undefined) {
      graphid = current;
    }

    // check if the oldgraphid was actually a id, not name
    if (!graphs[graphid]) {
      graphid = getGraphIDFromName(graphid);
    }

    // destroy the graph
    var i = getGraphIDs().indexOf(graphid);
    getGraphByID(graphid).destroy();
    delete graphs[graphid];

    // remove the container and select option
    try {
      $(`#network-${ graphid }`, POP.getDocument("graph")).remove();
      $(`#graph-select option[value="graph-${ graphid }"]`, POP.getDocument("graph")).remove();
    } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }

    // update the current graph if this was previously current
    if (graphid === current) {
      var ids = getGraphIDs();
      if (ids.length) {
        // other graphs avalible, set the first to current
        i = i != 0 ? i - 1 : 0;
        setCurrent(ids[i]);
        try {
          // update the current container if not testing
          $("#graph-select", POP.getDocument("graph")).val("graph-" + ids[i]);
        } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }
      } else {
        // no other graphs, create a new one
        create();
      }
    }
  }

  /**
   * Sets a graph to the current graph.
   * @param id the name/id of the graph to set as current
   **/
  function setCurrent(id) {
    // try get graph and set as current
    var graph = getGraphByID(id); // will default to no change if unrecognised id
    current = stripToID(graph.data().name);

    try {
      // hide networks, show this one
      $(".network", POP.getDocument("graph")).hide();
      $(`#network-${ current }`, POP.getDocument("graph")).show();

      // update the dropdown
      $("#graph-select", POP.getDocument("graph")).val("graph-" + current);
    } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }
  }

  /**
   * Gets the current selected graph.
   * @return the current graph ID
   **/
  function getCurrent() {
    return current;
  }

  /**
   * Gets the current selected graph's UI name.
   * @return the current graph's UI name
   **/
  function getCurrentName() {
    return graphs[current].data().name;
  }

  /**
   * Gets a graph by it's ID.
   * @param graphid the id of the graph to get data for. If unsupplied will use the current graph in view.
   * @return the graph data for this graph
   **/
  function getGraphByID(graphid) {
    var graph = graphs[graphid];
    if (graph === undefined) {
      // failed, try get by name instead
      return getGraphByName(graphid);
    } else {
      return graph;
    }
  }

  /**
   * Gets a graph by it's UI name.
   * @param graphname the name of the graph to get data for. If unsupplied will use the current graph in view.
   * @return the graph data for this graph.
   **/
  function getGraphByName(graphname) {
    return graphs[getGraphIDFromName(graphname)];
  }

  /**
   * Gets a graph's ID from it's UI name.
   * @param graphname the name of the graph to get data for. If unsupplied will use the current graph in view.
   * @return the ID of the graph
   **/
  function getGraphIDFromName(graphname) {
    var index = getGraphNames().indexOf(graphname);
    if (index === -1) {
      return current;
    } else {
      return getGraphIDs()[index];
    }
  }

  /**
   * Gets a graph's UI name from it's ID.
   * @param graphid the id of the graph to get data for. If unsupplied will use the current graph in view.
   * @return the UI name of the graph
   **/
  function getGraphNameFromID(graphid) {
    return getGraphByID(graphid).data().name;
  }

  /**
   * Gets the list of differnet graph ID's stored in the program.
   * @return a list of graph id names.
   **/
  function getGraphIDs() {
    return Object.keys(graphs);
  }

  /**
   * Gets a list of differnt graph names as shown in the UI.
   * @return array of graph names as shown in the UI.
   **/
  function getGraphNames() {
    var ret = []
    var ids = getGraphIDs();
    for (var i in ids) {
      if (graphs[ids[i]]) {
        ret.push(graphs[ids[i]].data().name);
      } else {
        delete graphs[ids[i]];
      }
    }
    return ret;
  }

  /**
   * Copies a graph into a new graph view.
   * @param graphid the graph to copy into a new instance.
   * @return the new graph instance
   **/
  function copy(graphid, targetname) {
    var data = GRAPH.getGraphData(graphid);

    // remove _number from name, makes sure we create a good name for the copy graph
    if (data.name.lastIndexOf("_") != -1 && !isNaN(data.name.substring(data.name.lastIndexOf("_") + 1))) {
      data.name = data.name.substring(0, data.name.lastIndexOf("_"));
    }

    // create graph
    return create(
      targetname ? targetname : data.name,
      new vis.DataSet(data.nodes),
      new vis.DataSet(data.edges),
      new GraphOptions(
        data.options.directed, data.options.weightedEdges, data.options.weightedNodes,
        data.options.negativeWeights, data.options.mutligraph,
        data.options.degrees, data.options.physics,
        data.options.fontSize, data.options.labelAlign
      )
    );
  }

  /**
   * Renames a graph.
   * @param oldgraphid the graph to rename.
   * @param newgraphid the new graph name.
   * @param renameOnFail if to always force a rename. Defaults to false.
   * @return The new renamed graph. Returns false if the change couldn't take place.
   **/
  function rename(oldgraphid, newgraphid, renameOnFail = true) {
    // check if the oldgraphid was actually a id, not name
    if (!graphs[oldgraphid]) {
      oldgraphid = getGraphIDFromName(oldgraphid);
    }
    var oldgraphname = getGraphNameFromID(oldgraphid);

    // check rename is ok
    var nospaceid = stripToID(newgraphid);
    if (!nospaceid || nospaceid === "current") {
      // don't allow already used name, or reserved "current" used in Blockly
      return false;
    }

    // generate a non conflicting name if there is a conflict
    var suffix = 0;
    var uniquename = newgraphid;
    while (nospaceid in graphs && nospaceid !== oldgraphid) {
      if (!renameOnFail) {
        return false;
      }
      uniquename = newgraphid + "_" + (++suffix);
      nospaceid = stripToID(uniquename);
    }
    newgraphid = uniquename;

    // rename in dictionary and update if current graph
    if (nospaceid !== oldgraphid) {
      graphs[nospaceid] = graphs[oldgraphid];
      delete graphs[oldgraphid];
      if (current === oldgraphid) {
        current = nospaceid;
      }
    }

    try {
      // rename the graph in the select box
      $(`#graph-select option[value="graph-${ oldgraphid }"]`, POP.getDocument("graph")).text(newgraphid);
      $(`#graph-select option[value="graph-${ oldgraphid }"]`, POP.getDocument("graph")).attr("value", "graph-" + nospaceid);

      // rename the container id
      $(`#network-${ oldgraphid }`, POP.getDocument("graph")).attr("id", `network-${ nospaceid }`);
    } catch (e) { /* empty catch, testing mode, we can ignore error from lack of page */ }

    // tell graph to update its name
    getGraphByID(nospaceid).setTitle(newgraphid);

    // tell blockly to update graph selectors, if blockly is loaded on this page
    try {
      BLOCKLY.updateGraphSelector(oldgraphname, newgraphid);
    } catch { }
    return newgraphid;
  }

  /**
   * Updates the containers when the graphs are popped in/out of the page.
   **/
  function updateContainers() {
    for (var g in graphs) {
      graphs[g].updateContainer();
      setTimeout(function() { graphs[g].fit(); }, 50);
    }
    subscribeEvents();
  }

  /**
   * Generates a default compare key to use when sorting or min/maxing a list of graph objects.
   * @param data the data to generate a compare key for.
   * @return the key to compare on
   **/
  function getCompareKey(data) {
    if (data[0].cost !== undefined) {
      return "cost";
    } else if (data[0].value !== undefined) {
      return "value";
    } else {
      return "id";
    }
  }

  /**
   * Gets an extreme value from a list of object, i.e. the max or min.
   * @param data the data, generated from the graph, to get an extreme value for.
   * @param min whether the minimum should be retreived. Defualts to true. Will get max on false.
   * @param key the key to perform min/max lookups on. If undefined will try pick the weight or cost.
   * @return the max/min value from the list supplied.
   **/
  function getExtreme(data, min = true, key = undefined) {
    // generate what key to search
    if (key === undefined) {
      key = getCompareKey(data);
    }

    // get the max / min value from the data
    if (min) {
      return data.reduce((prev, current) => prev[key] <= current[key] ? prev : current);
    } else {
      return data.reduce((prev, current) => prev[key] >= current[key] ? prev : current);
    }
  }

  /**
   * Sorts a list of object on a given key.
   * @param data the data, generated from the graph, to sort.
   * @param asc whether the sort should be ascending. Defualts to true.
   * @param key the key to perform sort on. If undefined will try pick the weight or cost.
   * @return the sorted list.
   **/
  function sort(data, asc = true, key = undefined) {
    // generate what key to sort by
    if (key === undefined) {
      key = getCompareKey(data);
    }

    // sort asc to be used in sort correctly
    if (asc) {
      asc = 1;
    } else {
      asc = -1;
    }

    return data.sort((a, b) => (a[key] > b[key]) ? asc : (a[key] < b[key]) ? -asc : 0);
  }

  /**
   * Saves a graph
   * @param graphid the id of the graph to save
   **/
  function save(graphid) {
    var data = GRAPH.getGraphData(graphid);
    IO.save(JSON.stringify(data), `graph-${ data.name }.json`, "data:text/json");
  }

  /**
   * Loads a graph and sets it to the current graph in view.
   * @return the newly loaded graph
   **/
  function load() {
    IO.load("graph-input").then((result) => {
      var graphJSON = JSON.parse(result);
      create(graphJSON.name, graphJSON.nodes, graphJSON.edges, graphJSON.options);
    });
  }

  /**
   * Opens a modal of settings for a specified item.
   * @param id the id of the item to get data for. If left unsupplied will load graph settings.
   * @param node whether the data is a node or not. Defaults to true. If false will load edge settings.
   * @param graphid the id of the graph to get settings for. If left undefined will use the current graph.
   **/
  function openSettings(id = undefined, node = undefined, graphid = undefined) {
    var title = "";
    var content = "";
    if (id === undefined) {
      // check if opening a graph data modal
      title = "Graph";
      content = generateGraphSettings(graphid);
    } else {
      // node / edge model
      title = node ? "Node" : "Edge";
      content = generateItemSettings(id, node, graphid);
    }

    // set modal content
    $("#graph-setting-title", POP.getDocument("graph")).text(`${ title } Settings`);
    $("#graph-setting-body", POP.getDocument("graph")).html(content);

    // open, with backdrop only on graph tab, and allowing interaction on rest of page
    $("#graph-settings", POP.getDocument("graph")).modal();
    if (POP.getDocument("graph") === POP.getDocument("blockly")) {
      // same page, restrict backdrop to page
      $(".modal-backdrop", POP.getDocument("graph")).appendTo($("#graph-tab", POP.getDocument("graph")));
      $("body", POP.getDocument("graph")).removeClass("modal-open");
    } else {
      // different page, remove from main page
      $("body", POP.getDocument("graph")).append($(".modal-backdrop"));
      $(".modal-backdrop").remove();
    }

    // focus the weight input, or label if no weight input exists
    setTimeout(function() {
      var inp = $("#item-weight", POP.getDocument("graph"));
      if (!inp.length) {
        inp = $("#item-name", POP.getDocument("graph"));
      }
      inp.focus();

      // move cursor to end
      var d = inp.val();
      inp.val("");
      inp.val(d);
    }, 500);
  }

  /**
   * Generates the content of a graph settings modal.
   * @param graphid the graph id to generate settings for. If left undefined, will use current graph.
   * @return the html string for the body of the modal settings popup.
   */
  function generateGraphSettings(graphid) {
    graphid = stripToID(GRAPH.getGraphData(graphid).name);
    var graphname = $(`#graph-select option[value="graph-${ graphid }"]`, POP.getDocument("graph")).text();
    var options = GRAPH.getGraphData(graphid).options;

    return `
      <form onSubmit="return false">
        <div class="form-group row">
          <label for="item-name" class="col-xs-2 col-form-label">Name</label>
          <div class="col-xs-10">
            <input type="text" class="form-control" id="item-name" value="${ graphname }">
            <span class="help-block" id="item-name-help"></span>
            <span id="item-name-original" style="display: none;">${ graphname.replaceAll("<", "&lt;") }</span>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-xs-2">Settings</div>
          <div class="col-xs-10">
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-directional-check"${ options.directed ? " checked" : "" }>
              <label class="form-check-label" for="graph-directional-check">Directed</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-weighted-edge-check"${ options.weightedEdges ? " checked" : "" }>
              <label class="form-check-label" for="graph-weighted-edge-check">Edge Weights</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-weighted-node-check"${ options.weightedNodes ? " checked" : "" }>
              <label class="form-check-label" for="graph-weighted-node-check">Node Weights</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-negative-weights"${ options.negativeWeights ? " checked" : "" }>
              <label class="form-check-label" for="graph-negative-weights">Negative Weights</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-multigraph"${ options.multigraph ? " checked" : "" }>
              <label class="form-check-label" for="graph-multigraph">Multigraph</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-degrees-check"${ options.degrees ? " checked" : "" }>
              <label class="form-check-label" for="graph-degrees-check">Show degrees</label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="checkbox" id="graph-physics-check"${ options.physics ? " checked" : "" }>
              <label class="form-check-label" for="graph-physics-check">Physics simulation</label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <label for="graph-font-size" class="col-xs-2 col-form-label">Font Size</label>
          <div class="col-xs-10">
            <input type="number" class="form-control" id="graph-font-size" min="${ FONTSIZERANGE[0] }" max="${ FONTSIZERANGE[1] }" value="${ options.fontSize }">
          </div>
        </div>
        <div class="form-group row">
          <label for="graph-label-align" class="col-xs-2 col-form-label">Edge Labels</label>
          <div class="col-xs-10">
            <select class="form-control" id="graph-label-align">
              <option value="horizontal"${ options.labelAlign === "horizontal" ? " selected" : "" }>Horizontal</option>
              <option value="top"${ options.labelAlign === "top" ? " selected" : "" }>Above Edge</option>
              <option value="middle"${ options.labelAlign === "middle" ? " selected" : "" }>Middle of Edge</option>
              <option value="bottom"${ options.labelAlign === "bottom" ? " selected" : "" }>Below edge</option>
            </select>
          </div>
        </div>
      </form>
    `;
  }

  /**
   * Generates the content of a node / edge settings modal.
   * @param id the id to generate settings for.
   * @param node If the id is for a node. If left undefined defualts to true.
   * @param graphid the graph id to generate settings for. If left undefined, will use current graph.
   * @return the html string for the body of the modal settings popup.
   */
  function generateItemSettings(id, node = true, graphid) {
    graphid = stripToID(GRAPH.getGraphData(graphid).name);
    var options = GRAPH.getGraphData(graphid).options;
    var item = GRAPH.get(id, node, graphid);
    var style = GRAPH.getStyle(id, node, graphid);

    return `
      <form onSubmit="return false">
        <span id="graph-name" style="display: none;">${ graphid }</span>
        <div class="form-group row">
          <label for="item-name" class="col-xs-2 col-form-label">Label</label>
          <div class="col-xs-10">
            <input type="text" class="form-control" id="item-name" value="${ node ? item.id : style.label !== undefined ? style.label : "" }">
            <span class="help-block" id="item-name-help"></span>
            <span id="item-name-original" style="display: none;">${ item.id.replaceAll("<", "&lt;") }</span>
          </div>
        </div>
        ${ (node && options.weightedNodes) || (!node && options.weightedEdges) ? `
          <div class="form-group row">
            <label for="item-weight" class="col-xs-2 col-form-label">Weight</label>
            <div class="col-xs-10">
              <input type="number" class="form-control" id="item-weight"${ options.negativeWeights ? "" : " min=\"0\"" } value="${ item.value === undefined ? 0 : item.value }">
            </div>
          </div>
          ` : ""
        }
        <div class="form-group row">
          <label for="item-colour" class="col-xs-2 col-form-label">Colour</label>
          <div class="col-xs-10">
            <input type="color" class="form-control" id="item-colour" value="${ style.color }"${ !node && style.inheritcol ? " disabled" : "" }>
          </div>
        </div>
        ${ node ? `
          <div class="form-group row">
            <label for="item-shape" class="col-xs-2 col-form-label">Shape</label>
            <div class="col-xs-10">
              <select class="form-control" id="item-shape">
                <option value="circle"${ style.shape === "circle" ? " selected" : "" }>Circle</option>
                <option value="ellipse"${ style.shape === "ellipse" ? " selected" : "" }>Ellipse</option>
                <option value="square"${ style.shape === "box" ? " selected" : "" }>Square</option>
                <option value="triangle"${ style.shape === "triangle" ? " selected" : "" }>Triangle</option>
                <option value="triangleDown"${ style.shape === "triangleDown" ? " selected" : "" }>Triangle Downwards</option>
                <option value="hexagon"${ style.shape === "hexagon" ? " selected" : "" }>Hexagon</option>
                <option value="diamond"${ style.shape === "diamond" ? " selected" : "" }>Diamond</option>
                <option value="star"${ style.shape === "star" ? " selected" : "" }>Star</option>
              </select>
            </div>
          </div>
          ` : `
          <div class="form-group row">
            <div class="col-xs-10 col-xs-offset-2">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" id="edge-inherit-colour"${ style.inheritcol ? " checked" : "" }>
                <label class="form-check-label" for="edge-inherit-colour">Inherit colour</label>
              </div>
            </div>
          </div>
          `
        }
      </form>
    `;
  }

  /**
   * Saves the settings from a model to the graph.
   **/
  function saveSettings() {
    var ok = true;
    var mode = $("#graph-setting-title", POP.getDocument("graph")).text().split(" ")[0].toLowerCase();

    // run checks / implement changes for respective setting page
    if (mode === "graph") {
      ok = saveGraphSettings();
    } else {
      ok = saveItemSettings(mode === "node");
    }

    // close the modal if changes did not cause issues
    if (ok) {
      $("#graph-settings", POP.getDocument("graph")).modal("hide");
    }
  }

  /**
   * Validates a graph settings page and saves the data.
   * @return true if the change was valid
   **/
  function saveGraphSettings() {
    // get any rename data
    var name = getInputtedName();

    // check rename is valid
    if (name.new !== name.old && !rename(name.old, name.new, false)) {
      // ID already in use, set error and return invalid
      if (stripToID(name.new) === "current") {
        setItemIDError("Current is a reserved work in Blockly, try another name");
      } else {
        setItemIDError("This is already a named graph, try another name");
      }
      return false;
    }

    // get valid font size
    var fontSize = parseInt($("#graph-font-size", POP.getDocument("graph")).val())
    if (fontSize < FONTSIZERANGE[0]) {
      fontSize = FONTSIZERANGE[0];
    } else if (fontSize > FONTSIZERANGE[1]) {
      fontSize = FONTSIZERANGE[1];
    }

    // set correct settings for graph
    options = new GraphOptions(
      $("#graph-directional-check", POP.getDocument("graph")).prop("checked"),
      $("#graph-weighted-edge-check", POP.getDocument("graph")).prop("checked"),
      $("#graph-weighted-node-check", POP.getDocument("graph")).prop("checked"),
      $("#graph-negative-weights", POP.getDocument("graph")).prop("checked"),
      $("#graph-multigraph", POP.getDocument("graph")).prop("checked"),
      $("#graph-degrees-check", POP.getDocument("graph")).prop("checked"),
      $("#graph-physics-check", POP.getDocument("graph")).prop("checked"),
      fontSize,
      $("#graph-label-align", POP.getDocument("graph")).val()
    );
    GRAPH.setOptions(options, stripToID(name.new));

    // nothing went wrong, return ok
    return true;
  }

  /**
   * Validates a node / edge settings page and saves the data.
   * @param node If the id is for a node. If left undefined defualts to true.
   * @return true if the change was valid
   **/
  function saveItemSettings(node = true) {
    // get any rename data
    var name = getInputtedName();
    var graphid = $("#graph-name", POP.getDocument("graph")).text();
    var options = GRAPH.getGraphData().options;

    // check if name change valid
    if (node && name.old !== name.new) {
      if (name.new === "") {
        setItemIDError("Nodes require a label.");
        return false;
      } else if (GRAPH.get(name.new, node, graphid) !== null) {
        // error renaming, display error
        setItemIDError(`This is already an existing ${ node ? "Node" : "Edge" }, try another name`);
        return false;
      }
    }

    // get settings
    settings = { };
    settings.color = $("#item-colour", POP.getDocument("graph")).val();
    if ($("#item-weight", POP.getDocument("graph")).length) {
      settings.value = parseFloat($("#item-weight", POP.getDocument("graph")).val());
    }

    if (node) {
      // extra node settings
      settings.shape = $("#item-shape", POP.getDocument("graph")).val();
    } else {
      // extra node settings, set colour option to inherit or colour
      settings.inheritcol = $("#edge-inherit-colour", POP.getDocument("graph")).prop("checked");
      settings.label = name.new; // get label specified
    }

    // set style
    GRAPH.setStyle(name.old, settings, node, graphid);

    // rename item if needed
    if (node && name.old !== name.new) {
      GRAPH.setItemID(name.old, name.new, node, graphid);
    }

    // nothing went wrong, return ok
    return true;
  }

  /**
   * Sets the name inputted and the original value.
   * @param whitespace whether whitespace should be included in the get request.
   **/
  function getInputtedName(whitespace = true) {
    var oldname = $("#item-name-original", POP.getDocument("graph")).text().trim();
    var newname = $("#item-name", POP.getDocument("graph")).val().trim();
    return { old: oldname, new: newname };
  }

  /**
   * Sets the error on the name input box.
   * @param error the error to display under the naming box.
   **/
  function setItemIDError(error) {
    $("#item-name-help", POP.getDocument("graph")).text(error);
    $("#item-name-help", POP.getDocument("graph")).parent().addClass("has-error");
  }

  /**
   * Strips whitespace and other illegal ID characters from a string, replacing it with _.
   * Useful for creating ID's from strings with spaces.
   * https://stackoverflow.com/questions/9635625/javascript-regex-to-remove-illegal-characters-from-dom-id
   * @param str the string to strip whitespace from.
   * @return the string with '_' instead of whitespace characters.
   **/
  function stripToID(str) {
    var lstr = str.trim().toLowerCase();
    if (/^[a-z]/.test(lstr)) {
      return lstr.replace(/^[^a-z]+|[^\w.-]+/gi, "_");
    } else {
      return stripToID("id-" + lstr);
    }
  }

  /**
   * Subscribes to event for buttons in the graph tab.
   **/
  function subscribeEvents() {
    // graph select, settings, IO
    $("#graph-select", POP.getDocument("graph")).on("change", function() {
      var val = this.value;
      if (val === "new") {
        create();
      } else if (val === "copy") {
        copy();
      } else if (val === "delete") {
        destroy();
      } else {
        setCurrent(this.value.replace("graph-", ""));
      }
    });

    // graph setting button
    $("#graph-settings-button", POP.getDocument("graph")).on("click", function() {
      openSettings();
    });

    // save settings
    $("#graph-settings-save", POP.getDocument("graph")).on("click", function() {
      saveSettings();
      setTimeout(() => { graphs[getCurrent()].resetEdgeMode(); }, 50);
    });

    // make sure edge mode after we close the modal
    $("#graph-settings-cancel", POP.getDocument("graph")).on("click", function() {
      setTimeout(() => { graphs[getCurrent()].resetEdgeMode(); }, 50);
    });

    // disable/enable colour box on inherit option
    $("#graph-setting-body", POP.getDocument("graph")).on("click", "#edge-inherit-colour", function() {
      $("#item-colour", POP.getDocument("graph")).prop("disabled", this.checked);
    });

    // stop page refreshing on enter in edit modal
    $("form", POP.getDocument("graph")).submit(function() { return false; });

    // graph IO
    $("#save-graph-button", POP.getDocument("graph")).on("click", function() {
      save();
    });

    $("#load-graph-button", POP.getDocument("graph")).on("click", function() {
      $("#graph-input", POP.getDocument("graph")).val("");
      $("#graph-input", POP.getDocument("graph")).click();
    });

    $("#graph-input", POP.getDocument("graph")).on("change", function() {
      var value = $(this).val();
      if (value && value !== "") {
        load();
      }
    });

    // clear graph button
    $("#clear-graph-button", POP.getDocument("graph")).on("click", function() {
      GRAPH.clear();
    });

    // context menu click handlers
    $("#context-menu", POP.getDocument("graph")).on("click", "#context-menu-edit", function() {
      // open correct settings modal
      var mode = $("#context-menu-type", POP.getDocument("graph")).text();
      if (mode === "graph") {
        openSettings();
      } else {
        openSettings($("#context-menu-id", POP.getDocument("graph")).text(), mode === "node");
      }
    });

    $("#context-menu", POP.getDocument("graph")).on("click", "#context-menu-delete", function() {
      // delete the data
      var mode = $("#context-menu-type", POP.getDocument("graph")).text();
      if (mode === "graph") {
        destroy();
      } else {
        GRAPH.remove($("#context-menu-id", POP.getDocument("graph")).text(), mode === "node");
      }
    });

    $("#context-menu", POP.getDocument("graph")).on("click", ".context-menu-save", function(event) {
      // export image
      var img = graphs[getCurrent()].getImage();

      // https://gist.github.com/davoclavo/4424731
      // get binary data and mime type
      var byteString = atob(img.split(',')[1]);
      var mimeString = img.split(',')[0].split(':')[1].split(';')[0]

      // write the bytes of the string to an ArrayBuffer
      var arrayBuffer = new ArrayBuffer(byteString.length);
      var ia = new Uint8Array(arrayBuffer);
      for (var i = 0; i < byteString.length; ++i) {
          ia[i] = byteString.charCodeAt(i);
      }

      // generate a data view and save the image
      var dataView = new DataView(arrayBuffer);
      var element = event.target;
      while (element.id === "") {
        element = element.parentElement;
      }
      var filename = element.id === "context-menu-copy" ? undefined : `${ current }.png`;
      IO.save(dataView, filename, mimeString);
    });

    $(POP.getDocument("graph")).on("pointerdown", function() {
      // close the context menu after click
      if ($("#context-menu", POP.getDocument("graph")).css("display") !== "none") {
        setTimeout(() => $("#context-menu").finish().hide(100), 100);
      }
    });
  }

  // Graph related event handlers
  try {
    $(document).ready(function() {
      staticDropdownCount = $("#graph-select").children().length;
      create();
      subscribeEvents();
    });
  } catch (e) {
    create();
  }

  return {
    options: GraphOptions,
    stripToID: stripToID,

    create: create,
    destroy: destroy,
    setCurrent: setCurrent,
    getCurrent: getCurrent,
    getCurrentName: getCurrentName,
    getGraphByID: getGraphByID,
    getGraphByName: getGraphByName,
    getGraphIDFromName: getGraphIDFromName,
    getGraphNameFromID: getGraphNameFromID,
    getGraphIDs: getGraphIDs,
    getGraphNames: getGraphNames,
    copy: copy,
    rename: rename,
    updateContainers: updateContainers,
    setOptions: graphWrapper("setOptions"),
    getGraphData: graphWrapper("data"),

    clear: graphWrapper("clear"),
    draw: graphWrapper("draw"),
    fit: graphWrapper("fit"),

    add: graphWrapper("add"),
    get: graphWrapper("get"),
    isNode: graphWrapper("isNode"),
    getSourceSink: graphWrapper("getSourceSink"),
    isAdjacent: graphWrapper("isAdjacent"),
    edgeTo: graphWrapper("edgeTo"),
    remove: graphWrapper("remove"),

    getDegree: graphWrapper("getDegree"),
    getValue: graphWrapper("getValue"),
    setValue: graphWrapper("setValue"),
    getStyle: graphWrapper("getStyle"),
    setStyle: graphWrapper("setStyle"),
    setItemID: graphWrapper("setItemID"),
    setColour: graphWrapper("setColour"),
    getData: graphWrapper("getData"),
    highlight: graphWrapper("highlight"),

    neighbours: graphWrapper("neighbours"),
    incidents: graphWrapper("incidents"),

    search: graphWrapper("search"),
    connected: graphWrapper("connected"),
    shortestPath: graphWrapper("shortestPath"),
    getExtreme: getExtreme,
    sortGraphData: sort,
    handleEvent: graphWrapper("handleEvent"),
  }
})();
