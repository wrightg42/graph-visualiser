/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

 /**
 * @fileoverview Functions that control the list visualization
 * in the algorithm visualizer.
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

// Array namespace
var ARRAYS = (function() {
  // private store of lists used to check for changes
  var lists = { };

  /**
  * A function to clear currently stored lists
  **/
  function clear() {
    // claer the current lists from the view
    var menu = $("#arrays-bin", POP.getDocument("arrays"));
    while (menu.children().length) {
      menu.children().last().remove();
    }

    lists = {};
  }

  /**
   * A function to update lists/objects with content.
   * If a new list is supplied this will automatically be added.
   * If no update has occured this is taken into account and nothing will happen.
   * @param name the name of the list/object
   * @param list the list/object data to update
   * @return true if the value was updated, false if no change.
   **/
  function update(name, list, step) {
    var id = BLOCKLY.getInternalVarName(name);
    if (!(name in lists)) {
      // add list if needed
      addList(name);
    } else if (_.isEqual(lists[name].value, list)) {
      // no change in list, do not update anything
      return false;;
    } else {
      // move the outdated data to the history tab
      var old = $(`#${ id }-update-${ lists[name].history.length }`, POP.getDocument("arrays"));
      $(`#history-list-${ id }`, POP.getDocument("arrays")).prepend(old);
      old.removeClass("current-array", POP.getDocument("arrays"));

      // save current value to history with values being updated
      lists[name].history.unshift(lists[name].value);
    }

    // turn into a full table and add to panel
    html = `
      <table class="array-table current-list" id="${ id }-update-${ lists[name].history.length }">
        <tr>
          <td class="table-number${ Object.keys(list).length === 0 ? "-empty" : "" }">
            <center><span>STEP\r\n${ step }</span><center>
          </td>
          ${ generateCells(name, list)   // replace complex data class hrefs with one that is named and numbered
              .replace(/complexdata-/g, `complexdata-${ id }-update-${ lists[name].history.length }-`) }
        </tr>
      </table>
    `;
    var item = $.parseHTML(html);
    $(`#list-${ id }`, POP.getDocument("arrays")).prepend(item);

    // update current held value
    lists[name].value = list;
    return true;
  }

  /**
   * A function to generate all cells for a list.
   * @param name the name of the list/object.
   * @param list the list/object data to update.
   * @return the html string of the cells for a table.
   **/
  function generateCells(name, list) {
    // variables to keep track of changes and assign classes for colouring
    var inserted = true;
    var removed = false;
    var object = !Array.isArray(list);
    var removedOffset = 0; // the delay for removed so we can perform accurate difference comparisonss

    // perform updates to the tracking
    if (object) {
      inserted = false;
      removed = false;
    } else if (lists[name].history.length) {
      // use Object.keys since previously could be an object
      inserted = Object.keys(list).length > Object.keys(lists[name].history[0]).length;
      removed = Object.keys(list).length < Object.keys(lists[name].history[0]).length;
    }

    // add the data to the view
    var html = "";
    for (var i in list) {
      // check for change in value
      var different = false;
      if (!lists[name].history.length || (!_.isEqual(list[i], lists[name].history[0][removed ? parseInt(i) + removedOffset : i]))) {
        // no history so all added
        // or there was a change to this item in the array (ternary to use the offset for when removing)
        different = true;

        // if cells were removed, increment the offset and add correct amount of cells
        if (removed) {
          // get how many items were removed
          var removedCount = removedOffset + 1;
          while (removedCount <= Object.keys(lists[name].history[0]).length &&
                  !_.isEqual(list[i], lists[name].history[0][parseInt(i) + removedCount])) {
            removedCount += 1;
          }
          removedCount -= removedOffset;

          // add the removed counter to the offset
          removedOffset += removedCount;
          if (removedCount - 1 === Object.keys(lists[name].history[0]).length) {
            // all items were removed, mark as we are addding all items instead
            removed = false;
            inserted = true;
            html += generateRemovedCell(undefined);
          } else {
            // mark the deleted cells
            different = false;
            html += generateRemovedCell(removedCount);
          }
        }
      }

      // get class of item to signify the change
      var cls = "array-no-change";
      if (different) {
        if (inserted) {
          cls = "array-insert";
        } else {
          cls = "array-change";
        }
      }

      // add cell to table
      html += `<td>${ generateCell(i, list, cls) }</td>`;
    }

    // add any missing removed items to the end
    if (removed) {
      // add unmarked removed to end
      var unmarkedRemoved = Object.keys(lists[name].history[0]).length - Object.keys(list).length - removedOffset;
      if (unmarkedRemoved > 0) {
        html += generateRemovedCell(unmarkedRemoved);
      }

      // if multiple removed was found, strip them all and mark a reset
      var index = 0;
      var count = 0;
      var search = "<table class=\"array-remove";
      while ((index = html.indexOf(search, index + 1)) !== -1) {
        count += 1;
      }

      if (count > 1) {
        // more than 1 delete blocks, replace all removes and add reset to beginning
        while ((index = html.indexOf(search)) !== -1) {
          var start = index - 13;
          var end = html.indexOf("</td>", html.indexOf("</td>", start) + 1) + 5;
          html = html.slice(0, start) + html.slice(end);
        }
        html = generateRemovedCell(undefined) + html;
      }
    }

    // mark all cells as added if reset
    if (html.indexOf(generateRemovedCell(undefined)) !== -1) {
      html = html.replaceAll("array-no-change", "array-insert");
      html = html.replaceAll("array-change", "array-insert");
    }

    return html;
  }

  /**
   * A function to generate a cell from a value and a given class
   * @param key The key to use to generate the cell
   * @param data The data to use to generate the cell
   * @param cls The class of the cell
   * @return the html string of the cell for a table.
   **/
  function generateCell(key, data, cls) {
    // add cell data
    var header = "";
    var body = "";

    // adjust the key if needed
    var adjustedKey = isNaN(parseInt(key)) ? key : parseInt(key) + Array.isArray(data);

    if (isComplex(data[key])) {
      // complex data, set header accordingly
      header = `
        <tr class="complexdropdown">
          <th colspan="2" class="array-key">
            <a class="collapse-btn" data-toggle="collapse" data-target=".complexdata-${ key }">${ adjustedKey }</a>
          </th>
        </tr>
      `;

      // make rows for each item. include a row with '...' to hide data, allowing user to extend if they wish
      body = `
        <tr class="collapse in complexdata-${ key }">
          <th colspan="2">...</th>
        </tr>
      `;
      for (var i in data[key]) {
        var adjustedI = isNaN(parseInt(i)) ? i : parseInt(i) + Array.isArray(data[key]);
        body += `
          <tr class="collapse complexdata-${ key }">
            <th class="array-key">${ adjustedI }</th>
            <td>${ isComplex(data[key][i]) ? "..." : data[key][i] }</td>
          </tr>
        `;
      }
    } else {
      // standard data, add normally to table cell
      header = `
        <tr>
          <th class="array-key">${ adjustedKey }</th>
        </tr>
      `;
      body = `
        <tr>
          <td>${ data[key] }</td>
        </tr>
      `;
    }

    // reconstruct cell in a table and return
    return `
      <table class="${ cls } nested-array-table">
        ${ header }
        ${ body }
      </table>
    `;
  }

  /**
  * A function to generate a removed data cell.
   * @param n the number of items removed. If n is undefined will mark all data has been reset.
   * @return the html data for the removed data cell.
   **/
  function generateRemovedCell(n) {
    return `
      <td>
        <table class="array-remove nested-array-table">
          <tr>
            <th class="array-key">${ n ? "DELETED" : "RESET" }</th>
          </tr>
          <tr>
            <td>${ n ? n + " item" : "All data removed" }</td>
          </tr>
        </table>
      </td>`;
  }

  /**
   * A function to add a list to the visualization.
   * @param name the new list's name
   **/
  function addList(name) {
    // add panel
    var id = BLOCKLY.getInternalVarName(name);
    var html = `
      <div class="panel panel-default">
        <div class="panel-heading">
        <button id="history-${ id }-btn" type="button" class="btn btn-primary history-button list-${ id }-content collapse in" data-toggle="collapse" data-target="#history-list-${ id }">History</button>
          <h4 class="panel-title">
            <a class="collapse-btn" data-toggle="collapse" data-target=".list-${ id }-content" aria-expanded="true">${ name }</a>
          </h3>
        </div>
        <div id="list-${ id }-div" class="center panel-collapse collapse in clearfix list-${ id }-content">
          <div id="list-${ id }" class="array-container">
            <button type="button" class="btn btn-primary history-button" data-toggle="collapse" data-target="#history-list-${ id }" style="display: none;">History</button>
            <div id="history-list-${ id }" class="collapse"></div>
          </div>
        </div>
      </div>
    `;
    var item = $.parseHTML(html);
    $(`#arrays-bin`, POP.getDocument("arrays")).append(item);

    // save list ready to be used
    lists[name] = { value: undefined, history: [ ] };
  }

  /**
   * Checks if a data object is complex (i.e. an object or array)
   * @param data the data to check if it is a non-primative
   * @return True if the data is an array or object
   **/
  function isComplex(data) {
    // checks for null and undefined
    if (data === null || data === undefined) {
      return undefined;
    }

    // return true if object or array (both have type 'object')
    return typeof data === "object";
  }

  /**
   * A function to count how much history a list/object has.
   * @param name the name of the list/object
   * @return the number of history items in the list/object. Return undefined if the list doesn't exist.
   **/
  function countHistory(name) {
    if (!(name in lists)) {
      return undefined;
    } else {
      return lists[name].history.length;
    }
  }

  return {
    clear: clear,
    update: update,
    countHistory: countHistory,
    isComplex: isComplex,
  }
})();
