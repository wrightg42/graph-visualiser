/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions that control the detachment of
 * the various visualizers in the algorithm visualizer.
 * @author lukeadamroberts@gmail.com (Luke Roberts)
 * @author wrightgeorge42@gmail.com (George Wright)
 **/

var POP = (function() {
   // pop out directory
  var pops = {
    table: { popped: false, dom: window },
    graph: { popped: false, dom: window },
    arrays: { popped: false, dom: window },
    console: { popped: false, dom: window },
    instructions: { popped: false, dom: window },
    blockly: { popped: false, dom: window }
  };

  /**
   * A function to get the document for a poppable element.
   * Used by jQuery to access correct page DOM.
   * @param name the poppable item's name
   **/
  function getDocument(name) {
    return pops[name].dom.document;
  }

  /**
   * Creates event handlers to respond to popout buttons being clicked
   **/
  function listenPopOut() {
    // pops an element out
    $(".popout").on("click", function() {
      var parentID = $(this).parent().attr("id");
      popOut(parentID);
    });
  }

  /**
   * Function for changing the active tab on the right side of screen
   * @param href the tab to be changed too. If null, removes active tab
   **/
  function changeTab(href) {
    // if null given remove all active classes
    if (href == null) {
      $("#instructions-nav").removeClass("active");
      $("#table-nav").removeClass("active");
      $("#graph-nav").removeClass("active");
      $("#arrays-nav").removeClass("active");
      return;
    }

    // get popname and check if it is poppped out
    var popname = href.substring(1).split('-')[0];
    if (pops[popname].popped) {
      // popped item not in this window, focus it
      pops[popname].dom.focus();
    } else {
      // set correct view
      window.focus();
      $(`a[data-toggle="tab"][href="${ href }"]`).trigger("click");

      $("#instructions-nav").removeClass("active");
      $("#table-nav").removeClass("active");
      $("#graph-nav").removeClass("active");
      $("#arrays-nav").removeClass("active");

      // set correct active tab in toolbar
      $(`#${popname}-nav`).addClass("active");

      if (popname === "graph") {
        // recenter if we showed the graph tab
        GRAPH.fit();
      }

      // force the splitter open
      forceSplitterOpen("#body-row");
    }
  }

  /**
   * A function to focus on the console input.
   **/
  function focusConsole() {
    // focus console window
    pops.console.dom.focus();

    // force the splitter open if needs be
    if (!pops.console.popped) {
      forceSplitterOpen("#tabs-pane");
      forceSplitterOpen("#body-row");
    }

    // focus input box
    pops.console.dom.CONSOLE.focusConsole();
  }

  /**
   * Checks/forces a splitter open.
   * @param splitterid the id of the splitter to force open.
   **/
  function forceSplitterOpen(splitterid) {
    $(splitterid).jqxSplitter("expand");
  }

  /**
   * Takes a popout tab name and create a new window with the data.
   * @param id The id of the element to create a new using
   * @param settings the settings to open the window with
   **/
  function createWindow(id, settings) {
    // get div of popped item and create window
    var popname = id.split('-')[0];
    var title = popname.charAt(0).toUpperCase() + popname.slice(1); // capitalise first letter for title
    var popObj = $(`#${ id }`, getDocument(popname));

    // add to div so we can get the 'outer' html
    popObj = $("<div/>").append(popObj.clone());

    var win = window.open("", id, settings);
    win.document.write(`
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <link rel="shortcut icon" href="favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>${ title }</title>

          <!-- Bootstrap -->
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

          ${ id === "graph-tab" ? "<link rel=\"stylesheet\" type=\"text/css\" href=\"https://unpkg.com/vis-network/styles/vis-network.min.css\" />" : "" }

          <!-- CSS -->
          <link rel="stylesheet" href="${ PAGEURL }/css/console.css">
          <link rel="stylesheet" href="${ PAGEURL }/css/table.css">
          <link rel="stylesheet" href="${ PAGEURL }/css/graph.css">
          <link rel="stylesheet" href="${ PAGEURL }/css/array.css">
          <link rel="stylesheet" href="${ PAGEURL }/css/page.css">
        </head>
        <body>
          ${ popObj.html().replace(/height: (([0-9]*\.[0-9]*%)|([0-9]*px))/, "height: 100%;") }
        </body>
      </html>
    `);
    return win;
  }

  /**
   * Removes a page element and places it in a seperate browser window.
   * @param id The id of the element to pop out
   **/
  function popOut(id) {
    // get div of popped item and create window
    var popname = id.split('-')[0];
    $(`#${popname}-popout`).hide();
    pops[popname].dom = createWindow(id, "location=no, width=500, height=500");
    pops[popname].popped = true;
    pops[popname].dom.document.POP = POP;

    // specfic pop out functions
    switch(id) {
      case 'instructions-tab':
        instructionsPopOut();
        break;
      case 'table-tab':
        tablePopOut();
        break;
      case 'console-tab':
        consolePopOut();
        break;
      case 'graph-tab':
        graphPopOut();
        break;
      case 'arrays-tab':
        arraysPopOut();
        break;
    }

    pops[popname].dom.addEventListener("beforeunload", function() { popIn(id); }); // restore the tab when we close the page

    // set the current active tab to the first non popped item
    var first = !pops["table"].popped ? "table" :
                !pops["graph"].popped ? "graph" :
                !pops["arrays"].popped ? "arrays" :
                !pops["instructions"].popped ? "instructions" : null;
    first = first == null ? first : `#${first}-tab`;
    changeTab(first);

    checkEmptyPanes();
  }

  /**
   * Places a detacted element and places it back in the main window.
   * @param id The id of the element to pop out
   **/
  function popIn(id) {
    // set the pop list object to store correct data
    var popname = id.split('-')[0];
    pops[popname].popped = false;
    if (id === "graph-tab") {
      // hide graph modal before popping in
      $("#graph-settings", getDocument("graph")).modal("hide");
      $("#graph-settings", getDocument("blockly")).css("display", "none");
    }
    $(`#${ id }`).html($(`#${ id }`, getDocument(popname)).html());
    $(`#${popname}-popout`).show();

    // specific pop in functions
    pops[popname].dom = window;
    switch(id) {
      case 'instructions-tab':
        instructionsPopIn();
        break;
    	case 'table-tab':
    		tablePopIn();
    		break;
    	case 'console-tab':
    		consolePopIn();
    		break;
    	case 'graph-tab':
    		graphPopIn();
    		break;
    	case 'arrays-tab':
    		arraysPopIn();
    		break;
    }

    // focus tab if put back in window
    if (id !== "console-tab") {
      changeTab(`#${id}`);
    } else {
      CONSOLE.listenConsole();
    }

    // restore functionality to click event
    listenPopOut()
    checkEmptyPanes();
  }

  /**
   * Collapses and blocks panes based on which panes are empty
   **/
  function checkEmptyPanes() {
    // get what is attached
    var has_console = !pops.console.popped;
    var has_tabs = false;
    Object.keys(pops).forEach(function(key) {
      if (key != "console" && !pops[key].popped) {
        has_tabs = true;
      }
    });

    // set size to be same as before
    var console_height = $("#console-tab").css("height");
    var tab_height = undefined;
    if (has_tabs && $("#tabs-pane").hasClass("jqx-fill-state-disabled")) {
      tab_height = has_console ? "60%" : "100%";
    } else if (console_height.slice(-1) === "%") {
      tab_height = `${ 100 - parseFloat(console_height.slice(0, console_height.length - 1)) }%`;
    } else {
      tab_height = `${ $("#tabs-pane").height() - parseInt(console_height.slice(0, console_height.length - 2)) }px`;
    }

    $('#tabs-pane').jqxSplitter({
      panels: [ { size: tab_height, collapsible: false }, { size: console_height, collapsible: true } ]
    });

    // hide console space if it is gone
    if (has_console) {
      // hide tab space if they are all gone
      if (!has_tabs) {
        $('#tabs-pane').jqxSplitter({
          panels: [ { collapsible: true }, { collapsible: false } ]
        });
        $('#tabs-pane').jqxSplitter("collapse");
      }

      $('#tabs-pane').jqxSplitter("enable");
    } else {
      $('#tabs-pane').jqxSplitter("collapse");
      $('#tabs-pane').jqxSplitter("disable");
    }

    // hide right side if everything is gone
    if (!has_console && !has_tabs) {
      $('#body-row').jqxSplitter("collapse");
      $('#body-row').jqxSplitter("disable");
    } else {
      var expand = $("#body-row").hasClass("jqx-fill-state-disabled");
      $('#body-row').jqxSplitter("enable");
      if (expand) {
        $('#body-row').jqxSplitter("expand");
      }
    }

    $('#window-container').css("visibility", "inherit");
    $('#console-tab').css("visibility", "inherit");
  }

  /**
   * Handles the transition to popping out trace table pane
   **/
  function tablePopOut() {
    // scale the table on resize
    pops.table.dom.TABLE = TABLE;
    pops.table.dom.onresize = TABLE.setDimensions;
    setTimeout(function() { TABLE.setDimensions(); }, 100);
  }

  /**
   * Handles the transition to popping in trace table pane
   **/
  function tablePopIn() {
  }

  /**
   * Handles the transition to popping out graph pane
   **/
  function graphPopOut() {
    pops.graph.dom.GRAPH = GRAPH;
    setTimeout(function() {
      pops.graph.dom.GRAPH.updateContainers();
    }, 100);
  }

  /**
   * Handles the transition to popping in graph pane
   **/
  function graphPopIn() {
    setTimeout(function() {
      pops.graph.dom.GRAPH.updateContainers();
    }, 100);
  }

  /**
   * Handles the transition to popping out arrays pane
   **/
  function arraysPopOut() {
  	pops.arrays.dom.ARRAYS = ARRAYS;
  }

  /**
   * Handles the transition to popping in arrays pane
   **/
  function arraysPopIn() {
  }

  /**
   * Handles the transition to popping out console pane
   **/
  function consolePopOut() {
  	pops.console.dom.CONSOLE = CONSOLE;
    CONSOLE.listenConsole();
  }

  /**
   * Handles the transition to popping in console pane
   **/
  function consolePopIn() {
    $('#console-pane').css("height", "40%");
    $('#console-pane').css("visibility", "inherit");
  }

  /**
   * Handles the transition to popping out instructions pane
   **/
  function instructionsPopOut() {
  }

  /**
   * Handles the transition to popping in instructions pane
   **/
  function instructionsPopIn() {
  }

  /**
   * Popout button and tab button handlers
   **/
  $(document).ready(function() {
    listenPopOut();

    // loads a window into the active tab
    $(".window-button").click(function() {
      // get text (in lowercase, swapping spaces for -)
      var text_parts = $(this).text().toLowerCase().split(' ');
      var text = text_parts[text_parts.length - 1];
      changeTab(`#${text}-tab`);
    });
  });

  return {
    changeTab: changeTab,
    focusConsole: focusConsole,
    getDocument: getDocument,
    createWindow: createWindow,

    // wrapper function to interact with page while popped out
    getCurrentSVG: function() {
      return INTERPRETER.getPreviousSVG();
    },
    run: function() {
      $("#run-button").click();
    },
    step: function() {
      $("#step-button").click();
    },
    stop: function() {
      $("#stop-button").click();
    },
    pause: function() {
      $("#pause-button").click();
    },
  };
})();
