/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview Functions to create and control the JS-interpreter.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

// interpreter namespace with functions to step/run/pause execution
var INTERPRETER = (function() {
  // interpreter data
  var interpreter = undefined;
  var locked = false;
  var paused = undefined;
  var parsed = false;
  var previousID = undefined;
  var currentID = undefined;
  var values = { };

  // error message to mark async functions and to stop the interpreter from running
  const ASYNCERRORBREAK = "async-call-stop";

  // dictionary of wrapper functions
  const WRAPDICT = (function() {
    // copy blockly and replace highlight function to update the current ID, used for trace table
    var BLOCKLY_wrapped = undefined;
    try {
      // only wrap blockly if it exists, the interpreter may be used in testing without blockly component
      var BLOCKLY_wrapped = Object.assign({}, BLOCKLY);
      BLOCKLY_wrapped.highlight = ((id) => {
        previousID = currentID;
        currentID = id;
        BLOCKLY.highlight(previousID);
      });
    } catch (e) { }

    // helper functions to help generate easier results in graph functions
    // convert a data object to a single point. If using an array will return the first item
    function toSingle(data) {
      if (Array.isArray(data)) {
        // convert to single
        data = data[0];
      } else if (data === null) {
        data = null;
      }
      return data;
    }

    // convert a data object to an array
    function toArray(data) {
      if (data === null) {
        // return empty array on fail
        return [];
      } else if (!Array.isArray(data)) {
        // convert to array
        return [data];
      }
      return data;
    }

    // get a data item from the object by key
    function getKey(data, key = "id") {
      if (data !== undefined && data !== null) {
        if (typeof data === "object") {
          if (Array.isArray(data)) {
            return data.map(d => convertDegree(d, false)[key]);
          } else {
            return convertDegree(data, false)[key];
          }
        } else {
          return data;
        }
      } else {
        return null;
      }
    }

    // sorts or gets an extreme value from a data result
    function filterData(data, filter) {
      if (filter) {
        if (filter === "min" || filter === "max") {
          return GRAPH.getExtreme(data, filter === "min");
        } else {
          return GRAPH.sortGraphData(data, filter === "sortasc");
        }
      } else {
        return data;
      }
    }

    // convert a degree in or out of array format
    function convertDegree(data, toObject = true) {
      if (Array.isArray(data)) {
        return data.map(d => convertDegree(d, toObject));
      } else if (typeof data === "object" && data !== null) {
        // convert JUST DEGREE object to a data object, to be parsed here
        var justDegree = false;
        if (data.in !== undefined && data.out !== undefined) {
          data = { degree: data };
          justDegree = true;
        }

        if (toObject) {
          if (data && data.degree && typeof data.degree == "object") {
            if (data.degree[0] !== undefined && data.degree[1] !== undefined) {
              data.degree = { in: data.degree[0], out: data.degree[1] };
            } else if (data.degree[0] !== undefined) {
              data.degree = data.degree[0];
            } else {
              delete data.degree;
            }
          }
        } else {
          if (data && data.degree && typeof data.degree === "object" && !Array.isArray(data.degree)) {
            data.degree = [ data.degree.in, data.degree.out ];
          }
        }

        // back to JUST DEGREE if needed
        if (justDegree) {
          data = data.degree;
        }
      }
      return data;
    }

    // strips a search down to a path or a key
    function stripSearch(data, key = "cost") {
      var stripped = getKey(data, key);
      if (stripped && key === "path") {
        for (var i in stripped) {
          stripped[i] = getKey(stripped[i], "id");
        }
      }
      return stripped;
    }

    // copy the graph namespace and replace key functions to return ID's for user simplicity
    var GRAPH_wrapped = Object.assign({}, GRAPH);
    // access methods
    GRAPH_wrapped.add = (data, node, graphid) => { return getKey(GRAPH.add(data, node, graphid)); }
    GRAPH_wrapped.getSingle = (data, node, graphid) => { return getKey(toSingle(GRAPH.get(convertDegree(data), node, graphid))); }
    GRAPH_wrapped.isNode = (node, graphid) => { return getKey(toSingle(GRAPH.isNode(convertDegree(node), graphid))); }
    GRAPH_wrapped.isAdjacent = (id1, id2, graphid) => { return getKey(toSingle(GRAPH.isAdjacent(convertDegree(id1), convertDegree(id2), graphid))); }
    GRAPH_wrapped.edgeTo = (id1, id2, graphid) => { return getKey(toSingle(GRAPH.edgeTo(convertDegree(id1), convertDegree(id2), graphid))); }
    GRAPH_wrapped.remove = (data, node, graphid) => { return getKey(GRAPH.remove(convertDegree(data), node, graphid)); }

    // get specific data
    GRAPH_wrapped.getDegree = (id, graphid) => { return convertDegree(getKey(GRAPH.getDegree(convertDegree(id), graphid), "degree"), false); }
    GRAPH_wrapped.getData = (id, key, node, graphid) => {
      if (key.toUpperCase() === key) {
        // requested a full data object
        var ret = { };
        if (key === "DATA" || key === "FULL") {
          // get full data as needed
          Object.assign(ret, convertDegree(toSingle(GRAPH.get(convertDegree(id), node, graphid)), false));
          if (ret.weight === 0) {
            ret.weight = 0;
          }
        }
        if (key === "STYLE" || key === "FULL") {
          // add style as needed
          Object.assign(ret, GRAPH.getStyle(convertDegree(id), node, graphid));
        }

        // return undefined if no data, otherwise return all we got
        if (Object.keys(ret).length === 0) {
          return undefined;
        } else {
          return ret;
        }
      } else if (key === "endpoints") {
        // convert to array of endpoints if asking for edge endpoints
        id = convertDegree(id);
        return [GRAPH.getData(id, "to", node, graphid), GRAPH.getData(id, "from", node, graphid)];
      } else if (key === "degree") {
        // return degree correctly if requested
        return GRAPH_wrapped.getDegree(id, graphid);
      } else if (key === "label") {
        // update correct key if accessing label
        key = "_label";
      }
      return convertDegree(GRAPH.getData(convertDegree(id), key, node, graphid), false);
    }
    GRAPH_wrapped.costTo = (id1, id2, graphid) => {
      if (GRAPH.edgeTo(convertDegree(id1), convertDegree(id2), graphid)) {
        return stripSearch(GRAPH.shortestPath(convertDegree(id1), convertDegree(id2), graphid), "cost");
      } else {
        return null;
      }
    }

    // data with many return points
    GRAPH_wrapped.get = (data, node, graphid, filter) => { return getKey(filterData(toArray(GRAPH.get(convertDegree(data), node, graphid)), filter)); }
    GRAPH_wrapped.getSourceSink = (source, graphid, filter) => { return getKey(filterData(toArray(GRAPH.getSourceSink(source, graphid)), filter)); }

    GRAPH_wrapped.neighbours = (nodeid, direction, graphid, filter) => { return getKey(filterData(GRAPH.neighbours(convertDegree(nodeid), direction, graphid), filter)); }
    GRAPH_wrapped.incidents = (nodeid, direction, graphid, filter) => { return getKey(filterData(GRAPH.incidents(convertDegree(nodeid), direction, graphid), filter)); }

    GRAPH_wrapped.search = (startnodeid, mode, graphid, key) => { return stripSearch(GRAPH.shortestPath(convertDegree(startnodeid), mode, graphid), key); }
    GRAPH_wrapped.shortestPath = (startnodeid, targetnodeid, graphid, key) => { return stripSearch(GRAPH.shortestPath(convertDegree(startnodeid), convertDegree(targetnodeid), graphid), key); }

    // a new wrapper function to copy data to a target graph
    GRAPH_wrapped.copyData = (data, incStyle, node, graphid, targetgraphid) => {
      // don't copy everything if no data
      if (data === undefined) {
        return;
      }

      // get full data to copy
      var data = toArray(GRAPH.get(convertDegree(data), node, graphid));

      // add each item to the target graph
      for (var i in data) {
        // get the style if requested
        if (incStyle) {
          Object.assign(data[i], GRAPH.getStyle(data[i].id, node, graphid));
        }

        // add data to target graph
        if (!GRAPH.add(data[i], node, targetgraphid)) {
          // failed to add, conflicting with target graph. update the data in target graph
          var id = data[i].id;
          if (!node) {
            // if edge, don't use ID as this will differ between graphs. use to/from
            id = { to: data[i].to, from: data[i].from };
            delete data[i].id;
          }
          GRAPH.setStyle(id, data[i], node, targetgraphid);
        }
      }
    }

    // create wrappings dictionary
    var wrappings = { "GRAPH": GRAPH_wrapped };
    if (BLOCKLY_wrapped !== undefined) {
      wrappings["BLOCKLY"] = BLOCKLY_wrapped;
    }

    // wrap console if it exists
    var consolename = "CONSOLE";
    var CONSOLE_wrapped = undefined;
    try {
      wrappings["CONSOLE"] = {
        "alert": CONSOLE.print,
        "prompt": async function(message, type) {
          BLOCKLY.highlight(currentID);
          return CONSOLE.get(message, type);
        }
      }
    } catch (e) {
      // if we don't have a console use the window alert and prompt methods
      wrappings["_window"] = { "alert": window.alert, "prompt": window.prompt };
      wrappings["console"] = { "log": console.log, "trace": console.trace }; // add console as this will be used in generator unit tests
    }

    return wrappings;
  })();

  /**
   * Creates an instance of the interpreter and specifies
   * all the wrappers used to call custom functions from the code.
   * @param interpreter the interpreter to init the API for.
   * @param scope the scope to init the API at.
   **/
  function initAPI(interpreter, scope) {
    /**
     * A function to generate a wrapper function ready for the interpreter to use.
     * @param fn the function to wrap.
     * @param async if the function is asynchronous. Defaults to false.
     * @return a designated wrapper function to use in the interpreter.
     **/
    function generateWrapper(fn, async = false) {
      // generate wrapper function for the function with correct parameter count
      var wrapperfunc = (function() {
        // parse all arguments, interpreter values are not useful
        for (var i in arguments) {
          if (i != arguments.length - async) {
            arguments[i] = fromInterpreterVar(arguments[i]);
          }
        }

        if (async) {
          // lock interpreter while waiting for async return value
          lock();

          // run the function
          fn.apply(this, arguments).then((result) => {
            unlock();
            arguments[arguments.length - 1](toInterpreterVar(result)); // call interpreter callback
            step(); // continue step, probably didn't finish line
            if (isRunning()) {
              run(); // continue execution if in running mode
            }
          }).catch(() => { stop(); });

          // throw error to stop the current call stack from blocking other commands
          throw new Error(ASYNCERRORBREAK);
        } else {
          // sync function, return the function value
          return toInterpreterVar(fn.apply(this, arguments));
        }
      }).bind(this);

      // count the arguments for the function correctly, including callback and default params
      var fnstr = fn.toString();
      var args = fnstr.substring(fnstr.indexOf("("), fnstr.indexOf(")")).split(",").length + async;

      // set correct amount of arguments and return wrapper
      Object.defineProperty(wrapperfunc, "length", { value: args });
      return wrapperfunc;
    }

    /**
     * Recursivly wrap a dictionary of objects for the interpreter.
     * @param n the name of the object.
     * @param o the object to wrap in the interpreter.
     * @param s the scope to wrap the object in.
     * @param globallist Wheather the dictionary to wrap is the global namespace of objects to wrap. Used for recursion, LEAVE AS TRUE.
     **/
    function wrap(n, o, s, globallist = true) {
      // determin what we are wrapping
      var type = typeof o;

      // wrap based on type
      if (type === "function") {
        // wrap function, check if async then wrap
        var async = o.constructor.name === "AsyncFunction";
        var wrapper = generateWrapper(o, async);
        interpreter.setProperty(s, n,
          async ? interpreter.createAsyncFunction(wrapper) : interpreter.createNativeFunction(wrapper));
      } else if (type === "object") {
        // generate scope for this object if required
        var scope = s;
        if (!globallist) {
          scope = interpreter.nativeToPseudo({});
          interpreter.setProperty(s, n, scope);
        }

        // wrap each object
        for (var name in o) {
          wrap(name, o[name], scope, false);
        }
      } else {
        interpreter.setProperty(s, n, o);
      }
    }

    // wrap objects
    wrap("", WRAPDICT, scope);
  }

  /**
   * Creates a new interpreter object to run a given code sample.
   * @param code the code to give the interpreter to run.
   * @return the new interpreter object to run the given code.
   **/
  function createInterpreter(code) {
    return new Interpreter(code, initAPI);
  }

  /**
   * Parses a variable gotten from the interpreter.
   * @param value The variable value gotten from interpreter, using getValueFromScope(id)
   **/
  function fromInterpreterVar(value) {
    if (typeof value === "function") {
      return value;
    } else {
      if (!interpreter) {
        // create an interpreter if we are using an external one
        interpreter = createInterpreter("");
      }

      // function to fix the outputted native arrays
      function pseudoObjectArrayFix(parsedObject) {
        if (typeof parsedObject === "object") {
          // check if keys are numbers
          if (Object.keys(parsedObject).map((n) => parseInt(n, 10)).every((n) => !isNaN(n))) {
            // try parse, make sure the numbers are 0, 1, 2...
            var nativearr = [];
            var valid = true;
            for (var i = 0; i < Object.keys(parsedObject).length; ++i) {
              if (i + "" in parsedObject) {
                nativearr.push(parsedObject[i + ""]);
              } else {
                valid = false;
                break;
              }
            }

            // make array if valid
            if (valid) {
              return nativearr;
            }
          }

          // just a normal object, parse all children incase there are array child elements
          for (var i in parsedObject) {
            parsedObject[i] = pseudoObjectArrayFix(parsedObject[i]);
          }
        }

        return parsedObject;
      }

      // parse the value then fix any array objects stored in the object
      return pseudoObjectArrayFix(interpreter.pseudoToNative(value));
    }
  }

  /**
   * Converts a normal JS datatype to an interpreter pseudo value.
   * @param value the value to convert.
   * @return the pseudovalue.
   **/
  function toInterpreterVar(value) {
    if (!interpreter) {
      // create an interpreter if we are using an external one
      interpreter = createInterpreter("");
    }
    return interpreter.nativeToPseudo(value);
  }

  /**
   * Updates the record of the variables being used in the
   * program. This is used to generate the trace table.
   * @return a dictionary of current variables and their values
   **/
  function getVars() {
    // get variable names
    var varValues = { };
    var vars = BLOCKLY.getVars();

    // iterate over values
    for (var i in vars) {
      // get variable value and store in results dictionary
      try {
        var v = vars[i];
        var interpreter_val = interpreter.getValueFromScope(v);
        var actual_val = fromInterpreterVar(interpreter_val);
        varValues[BLOCKLY.getUIVarName(v)] = actual_val;
      } catch {
        // any errors means unused variables, skip this item
        continue;
      }
    }

    return varValues;
  }

  /**
   * Locks the interpreter, used during async calls.
   **/
  function lock() {
    locked = true;
  }

  /**
   * Locks the interpreter, used to end async calls.
   **/
  function unlock() {
    locked = false;
  }

  /**
   * A function to check if the interpreter is running.
   **/
  function isRunning() {
    return paused === false;
  }

  /**
   * Gets the code to execute in the interpreter from Blockly.
   * @return the code with corrected CONSOLE namespace and highlight block commands.
   **/
  function getCode() {
    // get code
    var code = BLOCKLY.getCode("BLOCKLY.highlight(%1);\n");

    // replace window.prompt and alert with CONSOLE namespace
    code = code.replace(/window\./g, "CONSOLE.");

    // replace calls to CONSOLE.prompt with correct format
    var i = undefined; // index of CONSOLE.prompt call
    const PROMPT = "CONSOLE.prompt", NUM = "Number(";
    while ((i = code.indexOf(PROMPT, i + 1)) != -1) {
      // get position of start/end of params by matching brackets
      var pos = i + PROMPT.length; // the last position of brackets
      var brackets = [];
      do {
        if (code[pos] === "(") {
          brackets.push("(");
        } else if (code[pos] === ")") {
          brackets.pop();
        }
        ++pos;
      } while (brackets.length);

      // get contents of prompt call
      var prompt = code.substring(i + PROMPT.length + 1, pos - 1);

      // get type of prompt call
      var type = "string";
      if (code.substr(i - NUM.length, NUM.length) === NUM) {
        // converted to number in JS, type cast here and fix positions to match
        type = "number";
        i -= NUM.length;
        ++pos;
      }

      // replace prompt call with correct function call
      code = `${ code.substring(0, i) }${ PROMPT }(${ prompt }, "${ type }")${ code.substring(pos) }`;
    }

    console.log(code);
    return code;
  }

  /**
   * Prepare the code for stepping
   **/
  function parse() {
    // get code and create interpreter
    if (interpreter) {
      delete interpreter;
    }
    interpreter = createInterpreter(getCode());

    // add a message that we starting execution
    CONSOLE.print("STARTING EXECUTION...", "user-text");

    // remove highlights and get variables to be used in program
    BLOCKLY.highlight(null);
    values = getVars();
    previousID = undefined;
    currentID = undefined;

    // reset trace table, lists and graph
    TABLE.reset(Object.keys(values));
    ARRAYS.clear();
    parsed = true;
  }

  /**
   * Runs code at an interval specified by the run slider
   **/
  function run() {
    // only run if not locked
    if (locked) {
      alertConsoleInput();
    } if (paused === false) {
      // don't run again, will go too fast
      return;
    }
    paused = false; // running, not paused

    // callback function to call the run command again until interpreter.paused
    var callback = function() {
      if (!locked && paused === false) {
        step();
      }

      // get time till next call to step the code
      var delay = $("#run-slider")[0].value;
      delay = -delay;

      // create timer to call this function again
      if (parsed && !paused) {
        setTimeout(callback, delay)
      }
    };
    callback();
  }

  /**
   * Pauses execution of code.
   **/
  function pause() {
    paused = true;
  }

  /**
   * Stops code from running and forces a restart on next run.
   * @param reason the reason we stopped to print an appropriate message.
   **/
  function stop(reason) {
    paused = undefined
    parsed = false; // force restart next step/run

    // print message to console.
    if (reason) {
      CONSOLE.print(reason, reason.indexOf("ERROR") !== -1 ? "error-text" : "user-text");
    }

    unlock();
    currentID = undefined;
    BLOCKLY.highlight(null);
    CONSOLE.cancelGet();
  }

  /**
   * Performs a step of the code and updates internal data.
   **/
  function step() {
    if (!locked) {
      // mark the interpreter as not running if required to
      if (paused !== false) {
        paused = undefined;
      }

      // parse code if we need to
      if (!parsed) {
        parse();
        step(); // runs first line of code which is var definitions
      }

      // try execute a step
      var beginID = currentID;
      var _ok = true;
      while (beginID === currentID && _ok) {
        _ok = stepInterpreter();
        if (typeof _ok !== "boolean") {
          if (_ok.message === ASYNCERRORBREAK) {
            // intentional stop for async call, break
            break;
          } else {
            // unknown error, fail and mark for restarting
            console.log(_ok);
            if (!_ok.message) {
              _ok.message = "";
            }
            if (_ok.message === "undefined is not a function") {
              _ok.message = "Type error."
            }
            if (_ok.message.indexOf("undefined") !== -1) {
              _ok.message = "Undefined data."
            }

            alert("Error occured executing code!\n" + _ok.message);

            // TODO: give info on error!
            stop("ERROR EXECUTING CODE<br/>STOPPING EXECUTION.");

            break;
          }
        } else if (_ok === false) {
          stop("PROGRAM FINISHED.")
        }
      }

      // check to see if variables have updated
      var varValues = getVars();
      if (!_.isEqual(varValues, values)) {
        values = varValues;
        TABLE.step(varValues); // step table in order to update values
      }
    } else {
      // lock active, input required from user
      alertConsoleInput();
    }
  }

  /**
   * A wrapper function to step an interpreter and
   * update the trace table if values are updated.
   * @return true of there is no pause.
   **/
  function stepInterpreter() {
    // perform a step
    var _ok;
    try {
      _ok = interpreter.step();
    } catch (err) {
      return err;
    }

    return _ok;
  }

  /**
   * Gets the SVG of the previously executing block.
   * @return the generated SVG image of the previous block
   **/
  function getPreviousSVG() {
    return BLOCKLY.getSVG(previousID);
  }

  /**
   * Gets the SVG of the currently executing block.
   * @return the generated SVG image of the current block
   **/
  function getCurrentSVG() {
    return BLOCKLY.getSVG(currentID);
  }

  /**
   * A function to let the user know they need to input to the console.
   * @param msg Whether to create a popup to tell the user to supply input
   **/
  function alertConsoleInput(msg = true) {
    if (msg) {
      alert("Console input required!");
    }

    POP.focusConsole();
  }

  // click event handlers
  try {
    $(document).ready(function() {
      // update speed in ms
      $("#run-slider").change(function() {
        $("#speed").text(`${ -$(this).val() }ms`)
      });

      // running buttons
      $("#run-button").click(function() {
        run();
      });

      $("#pause-button").click(function() {
        pause();
      });

      $("#stop-button").click(function() {
        stop("STOPPING EXECUTION.");
      });

      $("#step-button").click(function() {
        step();
      });
    });
  } catch (e) { }

  return {
    run: run,
    pause: pause,
    stop: stop,
    step: step,
    getCurrentSVG: getCurrentSVG,
    getPreviousSVG: getPreviousSVG,
    createInterpreter: createInterpreter,
  }
})();
