/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview A module of useful functions to testing and analysis.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

// testing core namespace
const TEST_CORE = (function() {
  /**
   * Times the execution of a block of code.
   * @param fn the block of code to time.
   * @return time taken to execute as well as return value of code block.
   **/
  function timer(fn) {
    var time = performance.now();
    var res = fn();
    time = performance.now() - time;
    return { time: time, result: res };
  }

  // random number generator namespace
  var SEEDEDRANDOM = (function() {
    var seed = 0;

    /**
     * Sets the seed of the random number generator.
     * @param i the seed to set the random number generator to use.
     **/
    function setSeed(i) {
      seed = i;
    }

    /**
     * Generates a random number from the current seed.
     * https://gist.github.com/tommyettinger/46a874533244883189143505d203312c
     * @return random float between 0 and 1
     **/
    function mulberry32() {
      var t = (seed += 0x6D2B79F5);
      t = Math.imul(t ^ t >>> 15, t | 1);
      t ^= t + Math.imul(t ^ t >>> 7, t | 61);
      return ((t ^ t >>> 14) >>> 0) / 4294967296;
    }

    /**
     * Generates a random float between two numbers.
     * @param min the minimum float value to generate from.
     * @param max the maximum float value to generate to.
     * @return the "random" number generated by the PRNG.
     **/
    function next(min = 0, max = 1) {
      return min + (max - min) * mulberry32();
    }

    return {
      seed: setSeed,
      next: next,
    }
  })();

  /**
   * Plots data using plotly.
   * @param data the data object to use to plot a graph.
   * @param layout the layout object to use to plot the graph
   * @param width the width of the new graph window.
   * @param height the height of the new graph window.
   * @return the window the plot is displayed in.
   **/
  function plot(data, layout, width = 1000, height = 700) {
    // create a popup window with the plot of data
    var id = `testplot-${ Math.round(seededrandom.next(0, 1000)) }`;
    var win = window.open("", id,  "location=no, width=1000, height=700");
    win.document.write(`
      <!DOCTYPE html>
      <html>
        <head>
          <meta charset="utf-8">
          <link rel="shortcut icon" href="favicon.ico" />
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>${ layout !== undefined ? layout.title : id }</title>
          <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
        </head>
        <body>
          <div id="plot" style="width: 100%; height: 100vh;"></div>
          <script type="text/javascript">
            Plotly.newPlot(document.getElementById("plot"), ${ JSON.stringify(data) }, ${ JSON.stringify(layout) });
          </script>
        </body>
      </html>
    `);

    return win;
  }

  return {
    timer: timer,
    SEEDEDRANDOM: SEEDEDRANDOM,
    plot: plot,
  }
})();
