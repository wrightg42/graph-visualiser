/**
 * @license
 * Algorithm visualizer
 *
 * Copyright 2016 University of Leeds.
 * https://www.leeds.ac.uk/
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

/**
 * @fileoverview A simple test module for the graph component.
 * @author wrightgeorge42@gmail.com (George Wright)
 */

// graph test namespace
var TEST_GRAPH = (function() {
  // the options to use for various test runs
  var tests = [
    { name: "basic (n=20,e=35)", nodes: 20, edges: 35, options: new GRAPH.options(false, true, false, false, false, false, false), seed: 1 },
    { name: "directed basic (n=20,e=50)", nodes: 20, edges: 50, options: new GRAPH.options(true, true, false, false, false, false, false), seed: 2 },
    { name: "weighted nodes (n=20,e=35)", nodes: 20, edges: 35, options: new GRAPH.options(false, true, true, false, false, false, false), seed: 3 },
    { name: "directed weighted nodes (n=20,e=50)", nodes: 20, edges: 50, options: new GRAPH.options(true, true, true, false, false, false, false), seed: 4 },
    { name: "large dataset (n=100,e=500)", nodes: 100, edges: 500, options: new GRAPH.options(false, true, false, false, false, false, false), seed: 5 },
    { name: "directed large dataset (n=100,e=500)", nodes: 100, edges: 500, options: new GRAPH.options(true, true, false, false, false, false, false), seed: 6 },
  ];
  var runs = 1; // how many times the tests should be run to get averages

  $(document).ready(async function() {
    if (runs > 0) {
      console.debug("Graph tests started");
      data = {};
      for (var i = 0; i < runs; ++i) {
        console.debug(`Starting test iteration: ${ i + 1 }/${ runs }`);
        for (var t in tests) {
          // run test
          var result = await runTests(tests[t]);
          // create result store
          if (i == 0) {
            data[tests[t].name] = { results: {}, times: {} };
          }

          // store different result sets
          for (var n in result.results) {
            if (i == 0) {
              data[tests[t].name].results[n] = [ result.results[n] ];
            } else if (t < 4 && !_.isEqual(result.results[n], data[tests[t].name].results[n][0])) {
              // check if the result set is different and not the large dataset, deep equal does not perform well with this
              data[tests[t].name].results[n].push(result.results[n]);
            }
          }

          // save timing data
          for (var n in result.timings) {
            if (i == 0) {
              data[tests[t].name].times[n] = [ result.timings[n] ];
            } else {
              data[tests[t].name].times[n].push(result.timings[n]);
            }
          }
        }
      }
      console.debug("Graph test results");
      console.debug(data);

      // transform and plot data
      console.debug("Creating result plot");
      plot = [];
      for (var t in data) {
        // trace for this test
        trace = {
          x: [], y: [],
          name: t,
          type: "box",
        };

        // add x and y values for each time result
        for (var n in data[t].times) {
          if (n === "Failed Edges") {
            // not something we want to plot
            continue;
          } else {
            // add data points
            trace.x = trace.x.concat(Array(runs).fill(n));
            trace.y = trace.y.concat(data[t].times[n]);
          }
        }
        plot.push(trace);
      }

      // set layuout and plot
      layout = {
        title: "Execution Times Box Plot",
        yaxis: {
          title: "time (ms)",
        },
        boxmode: "group",
      };
      TEST_CORE.plot(plot, layout, 1800, 1000);
      console.debug("Graph tests ended");
    }
  });

  /**
   * Sets up a graph for testing using a set of config data.
   * @param nodes the number of nodes to create for the test.
   * @param edges the number of edges to create for the test.
   * @param settings the graph options object to use for the test graph.
   * @param seed the seed to use for generating edges.
   * @return the name of the newly created test graph.
   **/
  function setupGraph(testname, nodes, edges, settings, seed) {
    // set max edges to avoid infinate loop and seed PRNG
    console.debug(`Setting up graph for test: ${ testname }`);
    edges = Math.min(edges, (nodes * (nodes - 1) / (settings.directed ? 1 : 2))); // max edges using friendship therom
    TEST_CORE.SEEDEDRANDOM.seed(seed);

    // log time to add nodes
    var ndata = TEST_CORE.timer(() => {
      // create graph
      var name = GRAPH.create(`Test: ${ testname }`, undefined, undefined, settings).data().name;

      // add nodes
      for (var n = 0; n < nodes; ++n) {
        var d = { id: n.toString(), value: Math.round(TEST_CORE.SEEDEDRANDOM.next(0, 10)) }; // generate random weight for node
        GRAPH.add(d, true, name); // add node with int as id, makes edge generation easier
      }
      return name;
    });
    var name = ndata.result;

    // wait for network to stabalize physics before adding edges, saves endless mess of physics loop
    const edgePromise = new Promise((resolve) => {
      GRAPH.handleEvent("stabilized", () => {
        // log time to add edges
        var edata = TEST_CORE.timer(() => {
          // add edges
          var e = 0;
          var fails = 0;
          while (e < edges) {
            // generate data for the edge
            var from = Math.round(TEST_CORE.SEEDEDRANDOM.next(0, nodes)).toString();
            var to = Math.round(TEST_CORE.SEEDEDRANDOM.next(0, nodes)).toString();
            var value = Math.round(TEST_CORE.SEEDEDRANDOM.next(0, 10));

            // add edge, only inc counter if no error creating edge
            if (GRAPH.add({ from: from, to: to, value: value }, false, name) !== false) {
              ++e;
            } else {
              ++fails;
            }
          }
          return fails; // log amount of fails using to generate the timing
        });
        resolve({ "Node (Average)": ndata.time / nodes, "Edge {Average)": edata.time / edges, "Failed Edges": edata.result });
      }, "once", name);
    });
    return { name: name, promise: edgePromise };
  }

  /**
   * Sets up a graph then runs tests given a dictionary of options.
   * @param options the options to run tests with.
   **/
  function runTests(options) {
    console.debug(`Starting test: ${ options.name }`);
    var r = setupGraph(options.name, options.nodes, options.edges, options.options, options.seed)
    var g = r.name;
    return r.promise.then((timings) => {
      console.debug("Graph setup, running tests");
      // run search tests
      var bfs = TEST_CORE.timer(() => GRAPH.search(undefined, "bfs", g));
      var dfs = TEST_CORE.timer(() => GRAPH.search(undefined, "dfs", g));

      // run connectivity and shortest path tests
      var connected = TEST_CORE.timer(() => GRAPH.connected(undefined, undefined, g));
      var dijkstra = TEST_CORE.timer(() => GRAPH.search(undefined, "dijkstra", g));

      // store times in timing object
      timings["BFS"] = bfs.time;
      timings["DFS"] = dfs.time;
      timings["Connected"] = connected.time;
      timings["Djikstra"] = dijkstra.time;

      // create list of results from searches
      var outputs = {
        BFS: bfs.result,
        DFS: dfs.result,
        Connected: connected.result,
        Djikstra: dijkstra.result,
      }

      // tear down test graph and return results
      console.debug("Tests complete, tearing down graph");
      GRAPH.destroy(g);
      return { timings: timings, results: outputs };
    });
  }
})();
